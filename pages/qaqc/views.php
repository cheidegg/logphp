<?php

require "library/page.php";


// TwodimPlot
// ============================================
class TwodimPlot {
	
	// members
	// ---------------------------------------- 
	public $name = NULL;
	public $ctxX = NULL;
	public $ctxY = NULL;

	// __construct
	// ---------------------------------------- 
	public function __construct($name, $ctxX, $ctxY){
		$this->name = $name;
		$this->ctxX = $ctxX;
		$this->ctxY = $ctxY;
	}
}

// MeasPoint
// ============================================
class MeasPoint {
	
	// members
	// ---------------------------------------- 
	public $eqId;
	public $measValue;
	public $position;
	public $layer;
	public $board;

	// __construct
	// ---------------------------------------- 
	public function __construct($eqId, $measValue, $position, $layer, $board){
		$this->eqId      = $eqId;
		$this->measValue = $measValue;
		$this->position  = $position;
		$this->layer     = $layer;
		$this->board     = $board;
	}

	// equals
	// ---------------------------------------- 
	public function equals($other){
		return $this->eqId==$other->eqId && $this->position==$other->position && $this->layer==$other->layer && $this->board==$other->board;
	}
}

// loadTwodimPlots
// --------------------------------------------
function loadTwodimPlots(){
	$plots = array();
	array_push($plots, new TwodimPlot("Module Efficiency vs Gain Homogeneity", "MOD_Efficiency_Nom_AVG_SAC_FRA_MUN_DUB", "MOD_Gain_Homogen_Nom_AVG_SAC_FRA_MUN_DUB"));
	return $plots;
}

// fillInvisiTable
// --------------------------------------------
function fillInvisiTable($page, $json){

	// build invisible table header
	$fields = array();
	foreach($json["cols"] as $idx=>$col){
		$name = $col["label"];
		if($name=="BORDATE") $name = "Date";
		array_push($fields, sprintf("<th>%s</th>", $name)); // FIXME: mere process name here, but how put 'Expected #' or 'Items #' in case of single process?
	}
	$page->html->set("colspan", count($fields));
	$page->html->set("invhead", implode("", $fields));

	// build invisible table body
	$invbody = array();
	foreach($json["rows"] as $row){
		$fields = array();
		foreach($row as $val)
			array_push($fields, sprintf("<td>%s</td>", strval($val["0"]["v"])));
		array_push($invbody, sprintf("<tr>%s</tr>", implode("", $fields)));
	}
	$page->html->set("invbody", implode("", $invbody));
}

// Views Page
// ============================================
class ViewsPage extends Page {


	// public members and methods
	// ======================================== 
	
	// members
	// ---------------------------------------- 
	public $cdescs = array();
	public $cnames = array();
	public $twodim = array();


	// __construct
	// ---------------------------------------- 
	public function __construct($master, $name) {
		parent::__construct($master, $name);
		$this->db->contexts         ->owner = $this->master->qaqcOwnerContexts;
		$this->db->centralmeas      ->owner = $this->master->qaqcOwnerValues;
		//$this->db->prodsitegroupcont->owner = $this->master->qaqcOwnerContexts; // still from LOG, no?
	}

	// load
	// ---------------------------------------- 
	public function load() {
		/* Returns the content HTML when page is invoked via the menu */
		return $this->loadPage();
	}

	// submit
	// ---------------------------------------- 
	public function submit() {
		/* Returns the content HTML when the form in the page is submitted (i.e. a button is pressed) */

		// leave for now, maybe needed later
		return $this->loadPage();
	}


	// private members and methods
	// ======================================== 

	// loadBody
	// ---------------------------------------- 
	private function loadBody() {
		/* Builds and returns the body */

		if(!isset($this->post["do"])) return;

		// twodim plots
		if($this->post["do"]=="twodim") {
			$this->loadPlotsTwodim();
			return;
		}

		// normal plots
		$this->loadPlotsContext();

	}

	// loadHeader
	// ---------------------------------------- 
	private function loadHeader() {
		/* Builds and returns the header */

		// reset form in case something has been submitted
		if($this->post["do"]=="twodim") {
			unset($this->post["context"]); 
			unset($this->post["site"]);
		}
		if(in_array($this->post["do"], array("context", "site"))) {
			unset($this->post["twodim"]);
		}

		// sites
		$options = array(0 => "all");
		foreach($this->master->getOptionsSites() as $k=>$v) $options[$k] = $v;
		$site    = isset($this->post["site"]) ? $this->post["site"] : 0;
		$this->html->set("select_site", $this->html->makeSelect("site", $options, $site, array(), array(0=>"class=\"hidden\""), false, "submitIt('site')", "", "class=\"buttonlike flex\"", "class=\"buttonlike flex emphasized\"", array(0)));

		// contexts
		$site  = $site>0 ? array("psgc.sitename" => $options[$site]) : array();
		$ctxts = $this->db->readView("ctxts", array("c.prodsitegroup = psgc.prodsitegroup"),
		                                      array("c.id_contexts", "c.contextname", "c.contextdesc"), 
		                                      $site, array(), array("c.contextdesc" => "asc"));
		$this->cnames = array();
		$this->cdescs = array();
		foreach($ctxts as $ctx) {
			$this->cnames[$ctx->c_id_contexts] = $ctx->c_contextname;
			$this->cdescs[$ctx->c_id_contexts] = $ctx->c_contextdesc;
		}
		$options = array(0 => "select context");
		foreach($this->cdescs as $k=>$v) $options[$k] = $v;
		$add     = array(0 => "class='hidden'");
		if(count($this->cdescs)==0) 
			$options[0] = "nothing in here!"; 

		$thectx = isset($this->post["context"]) ? $this->post["context"] : NULL;
		$this->html->set("select_context", $this->html->makeSelect("context", $options, $thectx, array(), $add, false, "submitIt('context')", "", "class=\"buttonlike flex\"", "class=\"buttonlike flex emphasized\"", array(0)));

		// 2d plots
		$this->twodim = loadTwodimPlots();
		$options      = array(0 => "select plot");
		foreach($this->twodim as $td) array_push($options, $td->name); // careful, it's +1 here w.r.t. $this->twodim!
		$twodim = isset($this->post["twodim"]) ? $this->post["twodim"] : 0;
		$this->html->set("select_twodim", $this->html->makeSelect("twodim", $options, $twodim, array(), array(0=>"class=\"hidden\""), false, "submitIt('twodim')", "", "class=\"buttonlike flex\"", "class=\"buttonlike flex emphasized\"", array(0)));

		// template
		$this->html->set("header", $this->html->template("views_header"));
	}

	// loadPage
	// ---------------------------------------- 
	private function loadPage() {
		/* Builds and returns the page */

		$this->loadHeader();
		$this->loadBody  ();
		return $this->html->template("views");
	}

	// loadPlotsContext
	// ---------------------------------------- 
	private function loadPlotsContext() {
		/* Builds and returns the body in the contexts case */

		// context must have been selected
		if(!isset($this->post["context"]) || empty($this->post["context"])) return;

		// colleting data for both plots
		$thectx = $this->cnames[$this->post["context"]];
		$meases = $this->db->readTable("centralmeas", array("measvalue", "eqentryid", "meastime"), 
		                                              array("contextname"=>$thectx, "isvalidflag"=>"T"));

		// assembling jsons
		$json1 = array();
		$json2 = array();
		$json3 = array();
		$json2["cols"] = array(array('id' => '','label' => 'EQENTRYID', 'type' => 'string'),
		                       array('id' => '','label' => 'MEASVALUE', 'type' => 'number'),
		                       );
		$json3["cols"] = array(array('id' => '','label' => 'MEASTIME' , 'type' => 'datetime'),
		                       array('id' => '','label' => 'MEASVALUE', 'type' => 'number'  ),
		                       );
		$json2["rows"] = array();
		$json3["rows"] = array();
		foreach($meases as $row){
			$r   = array();
			$r[] = array('v' => (string) $row->eqentryid);
			$r[] = array('v' => (float)  $row->measvalue);
			array_push($json1        , (float) $row->measvalue);
			array_push($json2["rows"], array('c' => $r));

			$r = array();
			$r[] = array('v' => sprintf("Date(%s)", strtotime($row->meastime)."000"));
			$r[] = array('v' => (float) $row->measvalue);
			array_push($json3["rows"], array('c' => $r));
		}

		// no data
		if(count($json1)==0){
			$this->html->set("body", $this->html->template("views_body_empty"));
			return;
		}

		// has data -> continue here

		// general parameters
		$this->html->set("plotTitle", $thectx                   );
		$this->html->set("plotDate" , $this->master->eventDateHr);

		// first plot
		$this->html->set("json"     , json_encode($json1));
		$this->html->set("json_min" , min($json1)        );
		$this->html->set("json_max" , max($json1)        );
		$this->html->set("plot1"    , $this->html->template("views_body_context_plot1"));

		// second plot
		fillInvisiTable($this, $json2);
		$this->html->set("json"        , json_encode($json2));
		$this->html->set("canvasId"    , "canvasHist");
		$this->html->set("png"         , "pngHist"   );
		$this->html->set("linkCsv"     , "csvHist"   );
		$this->html->set("linkXls"     , "xlsHist"   );
		$this->html->set("tableId"     , "tableHist" );
		$this->html->set("expFileName" , sprintf("plot_hist_%d_%s", $this->post["context"], $this->master->eventDateFs));
		$this->html->set("scriptData"  , $this->html->template("plots_onedim_hist", array(), NULL, "all"));
		$this->html->set("scriptExport", $this->html->template("plots_export"     , array(), NULL, "all"));
		$this->html->set("plot2"       , $this->html->template("views_body_context_plot23"));

		// third plot
		fillInvisiTable($this, $json3);
		$this->html->set("json"        , json_encode($json3));
		$this->html->set("canvasId"    , "canvasScat");
		$this->html->set("png"         , "pngScat"   );
		$this->html->set("linkCsv"     , "csvScat"   );
		$this->html->set("linkXls"     , "xlsScat"   );
		$this->html->set("tableId"     , "tableScat" );
		$this->html->set("expFileName" , sprintf("plot_hist_%d_%s", $this->post["context"], $this->master->eventDateFs));
		$this->html->set("scriptData"  , $this->html->template("plots_onedim_scat", array(), NULL, "all"));
		$this->html->set("scriptExport", $this->html->template("plots_export"     , array(), NULL, "all"));
		$this->html->set("plot3"       , $this->html->template("views_body_context_plot23"));

		// the template
		$this->html->set("body", $this->html->template("views_body_context"));
	}

	// loadPlotsTwodim
	// ---------------------------------------- 
	private function loadPlotsTwodim() {
		/* Builds and returns the body in the twodim case */

		// twodim must have been selected
		if(!isset($this->post["twodim"]) || empty($this->post["twodim"])) return;

		// retrieve the TwodimPlot object and contexnames
		$td     = $this->twodim[$this->post["twodim"]-1];
		$ctxX   = $td->ctxX;
		$ctxY   = $td->ctxY;

		// x dimension
		$valsX  = array();
		$measX  = $this->db->readTable("centralmeas", array("measvalue", "eqentryid", "layer", "board", "position"), 
		                                              array("contextname"=>$ctxX, "isvalidflag"=>"T"));
		foreach($measX as $row)
			array_push($valsX, new MeasPoint($row->eqentryid, $row->measvalue, $row->position, $row->layer, $row->board));

		// y dimension
		$valsY  = array();
		$measY  = $this->db->readTable("centralmeas", array("measvalue", "eqentryid", "layer", "board", "position"), 
		                                              array("contextname"=>$ctxY, "isvalidflag"=>"T"));
		foreach($measY as $row)
			array_push($valsY, new MeasPoint($row->eqentryid, $row->measvalue, $row->position, $row->layer, $row->board));


		// no data
		if(count($valsX)==0 || count($valsY)==0){
			$this->html->set("body", $this->html->template("views_body_empty"));
			return;
		}

		// has data -> continue here with assembling the plot
		$measX = array();
		$measY = array();

		// FIXME: inefficient! let's keep it like that for now..
		foreach($valsX as $x){
			foreach($valsY as $y){
				if($x->equals($y)){
					array_push($measX, (float) $x->measValue);
					array_push($measY, (float) $y->measValue);
					break;
				}
			}
		}         


		// the template
		$this->html->set("name"    , $td->name           );
		$this->html->set("ctxX"    , $td->ctxX           );
		$this->html->set("ctxY"    , $td->ctxY           );
		$this->html->set("jsonx"    , json_encode($measX));
		$this->html->set("jsonx_min", min($measX)        );
		$this->html->set("jsonx_max", max($measX)        );
		$this->html->set("jsony"    , json_encode($measY));
		$this->html->set("jsony_min", min($measY)        );
		$this->html->set("jsony_max", max($measY)        );
		$this->html->set("body"     , $this->html->template("views_body_twodim"));

	}

}

$page = new ViewsPage($this, "views");

?>
