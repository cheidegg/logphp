<?php

require_once "library/page.php";
require_once "pages/all/all.php";
require_once "pages/all/ranges.php";




// Attribute
// ============================================
class Attribute {

	// members
	// ---------------------------------------- 
	public $label     = NULL;
	public $attribute = NULL;
	public $table     = NULL;
	public $options   = array();
	public $isDate    = false;
	public $isUrl     = false;

	// __construct
	// ---------------------------------------- 
	public function __construct($label, $attribute, $table, $options=array()){
		$this->label     = $label;
		$this->attribute = $attribute;
		$this->table     = $table;
		$this->options   = $options;
		$this->isDate    = (substr($attribute, -4)=="date");
		$this->isUrl     = (substr($attribute, -9)=="reference");
	}
}




// EditItemsPage
// ============================================
class EditItemsPage extends Page {


	// public members and methods
	// ======================================== 

	// members
	// ---------------------------------------- 
	public $items     = array();
	public $faulty    = 0;
	public $firstSite = array();

	// __construct
	// ---------------------------------------- 
	public function __construct($master, $name) {
		parent::__construct($master, $name);
		$this->attlist = array();
		array_push($this->attlist, new Attribute("non-conform-flag"              , "isnonconfflag"    , "equipment"       , $this->master->ncfs));
		array_push($this->attlist, new Attribute("current status"                , "statusid"         , "statuslocation"  , $this->master->getOptionsStatus("duringeditmiflag")));
		array_push($this->attlist, new Attribute("current location / site / room", "majorlocid"       , "statuslocation"  , $this->master->getOptionsSites ("duringeditmiflag")));
		array_push($this->attlist, new Attribute("document (max. 2MB)"           , "docid"            , "doclink"         , $this->master->doctypes));
		array_push($this->attlist, new Attribute("comment"                       , "eqcomment"        , "eqcomments"      , $this->master->doctypes));
		array_push($this->attlist, new Attribute("supplier"                      , "supplier"         , "equipmentdetails"));
		array_push($this->attlist, new Attribute("cable length (meters)"         , "cablelength"      , "equipmentdetails"));
		array_push($this->attlist, new Attribute("delivery date"                 , "deliverydate"     , "equipmentdetails"));
		array_push($this->attlist, new Attribute("manufacturing date"            , "manufacturingdate", "equipmentdetails"));
		array_push($this->attlist, new Attribute("ordered by"                    , "orderedby"        , "equipmentdetails"));
		array_push($this->attlist, new Attribute("order date"                    , "ordereddate"      , "equipmentdetails"));
		array_push($this->attlist, new Attribute("order reference (url)"         , "orderreference"   , "equipmentdetails"));
	}

	// load
	// ---------------------------------------- 
	public function load(){
		/* Returns the content HTML when page is invoked via the menu */
		$this->items = array();
		return $this->loadPage();
	}

	// submit
	// ---------------------------------------- 
	public function submit(){
		/* Returns the content HTML when the form in the page is submitted (i.e. a button is pressed) */

		rangesLoadItems($this);
		$res = false;
		if     ($this->post["do"]=="add" ) rangesSubmitAddMode0($this, true);
		else if($this->post["do"]=="go"  ) $res = rangesSubmitGo ($this);
		else if($this->post["do"]=="attr") $res = $this->submitAttr();
		else if($this->post["do"]=="clr" ) $this->submitClear();
		else if($this->post["do"]=="save") $res = $this->submitSave();

		if($res) $this->db->commit();
		else     $this->db->undo  ();

		return $this->loadPage();
	}


	// private members and methods
	// ======================================== 

	// loadPage
	// ---------------------------------------- 
	private function loadPage(){
		/* Building the form */

		// the entire ranges part
		rangesLoadPage($this, true, true, "Edit Multiple Items <span style='font-size:70%; color:#666666;'>(not for groups, batches and subbatches!)</span>", false, true);

		// attributes
		$opts = array(0=>"Choose attribute");
		$dis  = array();
		$add  = array(0=>"class='hidden'");
		foreach($this->attlist as $idx=>$att){
			$opts[$idx+1] = $att->label;
			if($this->faulty>0 && $att->table=="equipmentdetails") array_push($dis, $idx);
		}

		$sel = isset($this->post["do"]) && $this->post["do"]!="save" && isset($this->post["attId"]) ? $this->post["attId"] : 0;
		$this->html->set("options_choose"   , $sel!=0 ? "emphasized" : "");
		$this->html->set("options_selectatt", $this->html->makeOptions($opts, $sel, $dis, $add));
		return $this->html->template("edititems");
	}

	// submitAttr
	// ---------------------------------------- 
	private function submitAttr(){
		/* Choice for attribute */

		if(!isset($this->post["attId"]) || !array_key_exists($this->post["attId"]-1, $this->attlist)) return;
		$att = $this->attlist[$this->post["attId"]-1];
		if(empty($att)) return;

		$furtherInput = "";
		$furtherAdd   = "";
		$tpl = "<input type='%s' class='invisiblelike' id='further' name='further' onchange='changecolor(\"further\")' %s placeholder='%s' >";
		if     (count($att->options)>0){
			$opts = array("choose"=>"Choose option");
			foreach($att->options as $key=>$val)
				$opts[strval($key)] = $val;
			$furtherInput = $this->html->makeSelect("further", $opts, "choose", array("choose"), array(), false, "", "class='buttonlike flex'");
		}
		else if($att->isDate)
			$furtherInput = sprintf($tpl, "text"  , "pattern='".$this->master->patternDate."'", "YYYY-MM-DD");
		else if($att->isUrl)
			$furtherInput = sprintf($tpl, "text"  , ""                                        , "http://"   );
		else if($att->attribute=="cablelength")
			$furtherInput = sprintf($tpl, "number", "min='0' max='999.99' step='0.01'"        , "0.00"      );
		else
			$furtherInput = sprintf($tpl, "text"  , ""                                        , ""          );

		if     ($att->attribute=="eqcomment")
			$furtherAdd = "<input type='text' class='invisiblelike' id='furtherAdd' name='furtherAdd' onchange='changecolor(\"furtherAdd\")' placeholder='comment'>";
		else if($att->attribute=="majorlocid")
			$furtherAdd = "<input type='text' class='invisiblelike' id='furtherAdd' name='furtherAdd' onchange='changecolor(\"furtherAdd\")' placeholder='building or room'>";
		else if($att->attribute=="docid")
			$furtherAdd = "<input type='file' class='invisiblelike' id='furtherAdd' name='uplnewdoc' onchange='changecolor(\"furtherAdd\")' accept='".implode(",",$this->master->allowedUplTypes)."'>";

		$this->html->set("furtherInput"   , $furtherInput);
		$this->html->set("furtherInputAdd", $furtherAdd  );
		$this->html->set("saveButton"     , $this->html->template("edititems_saveButton"));
		$this->html->set("eventDateField" , $this->html->template("eventDate", array("title"=>"event date", "name"=>"eventDate", "addd"=>"", "addl"=>""), NULL, "all"));
	}

	// submitClear
	// ---------------------------------------- 
	private function submitClear(){
		/* Clears the entire table */
		$this->items = array();
	}

	// submitSave
	// ---------------------------------------- 
	private function submitSave(){
		/* Updating the information */

		if(count($this->items)==0) return true;

		// eqids
		$eqids = array();
		foreach($this->items as $idx=>$range){
			if(!in_array($idx+1, $this->post["subRanges"])) continue;
			foreach($range as $item)
				array_push($eqids, $item->eqid);
		}
		if(count($eqids)==0) return true;

		// Attributes
		if(!isset($this->post["attId"]) || !array_key_exists($this->post["attId"]-1, $this->attlist)) return;
		$att = $this->attlist[$this->post["attId"]-1];
		if(empty($att)) return false;

		// Update tables
		$res = false;
		if(in_array($att->table, array("equipment", "equipmentdetails"))) $res = $this->submitSaveEquip  ($att, $eqids);
		if($att->table=="statuslocation"                                ) $res = $this->submitSaveStatLoc($att, $eqids); 
		if($att->table=="eqcomments"                                    ) $res = $this->submitSaveComment($att, $eqids); 
		if($att->table=="doclink"                                       ) $res = $this->submitSaveDoc    ($att, $eqids); 

		return true;
	}

	// submitSaveComment
	// ---------------------------------------- 
	private function submitSaveComment($att, $eqids){
		/* Updating eq comment */
		return addComment($this, $this->post["furtherAdd"], intval($this->post["further"]), $this->post["eventDate"], $eqids, array(), "other", true);
	}

	// submitSaveDoc
	// ---------------------------------------- 
	private function submitSaveDoc($att, $eqids){
		/* Updating document */
		return uploadDoc($this, "uplnewdoc", intval($this->post["further"]), $this->post["eventDate"], $eqids, array(), "other", true);
	}

	// submitSaveEquip
	// ---------------------------------------- 
	private function submitSaveEquip($att, $eqids){
		/* Updating equipment info */

		if(empty($att) || count($eqids)==0) return false;

		$eventDateDb = isValidDate($this->post["eventDate"]) ? dbStringDate($this->post["eventDate"]) : $this->master->eventDateDb;

		$cn = new DbConfig($this->master, "update");
		if($att->table=="equipment"       ) $cn->select("id_equipment", $eqids);
		if($att->table=="equipmentdetails") $cn->select("eqentryid"   , $eqids);

		$value = $this->post["further"];
		if($att->attribute=="isnonconfflag" && $value=="X") $value="null";
		$this->db->get($att->table)->update(array($att->attribute=>$value,
		                                          "websiteusered"=>$this->globals["username"],
		                                          "edittime"     =>$eventDateDb), $cn);
		$this->db->push();
		if($this->db->error()){
			$this->vb->error("Equipment attribute could not be updated!", true);
			return false;
		}

		$this->vb->success("Equipment attribute updated successfully!");
		return true;
	}

	// submitSaveStatLoc
	// ---------------------------------------- 
	private function submitSaveStatLoc($att, $eqids){
		/* Updating statuslocation info */

		$eventDate      = isValidDate($this->post["eventDate"]) ? dbStringDate($this->post["eventDate"]) : $this->master->eventDateDb;
		$statuslocation = $this->db->statuslocation;

		$allowedStatuses  = array_keys($this->master->getOptionsStatus("prioreditmiflag"));
		$allowedLocations = array_keys($this->master->getOptionsSites ("prioreditmiflag"));
		$notAllowed       = array();

		foreach($eqids as $eqid){

			if($att->attribute=="statusid"){
				$current = $this->master->getEqStatus($eqid);
				if(!in_array($current->sl_statusid, $allowedStatuses)) {
					array_push($notAllowed, $eqid);
					continue;
				}
			}
			if($att->attribute=="majorlocid"){
				$current = $this->master->getEqLocation($eqid);
				if(!in_array($current->sl_majorlocid, $allowedLocations)) {
					array_push($notAllowed, $eqid);
					continue;
				}
			}
	
			// update in data base
			$insert = array("eqentryid"     => $eqid,
			                $att->attribute => $this->post["further"],
			                "websiteusercr" => $this->globals["username"],
			                "websiteusered" => $this->globals["username"],
			                "eventdate"     => $eventDate);
			if($att->attribute=="majorlocid") $insert["minorlocdesc"] = $this->post["furtherAdd"];
			$statuslocation->append($insert);

			// also change in the table displayed to the user
			$statuses = $this->master->getOptionsStatus();
			$sites    = $this->master->getOptionsSites ();
			foreach($this->items as $range){
				foreach($range as $item){
					if($item->eqid!=$eqid) continue; // retrieve the proper RangeEntry object
					if($att->attribute=="statusid"  ) $item->status = $statuses[$this->post["further"]];
					if($att->attribute=="majorlocid") $item->site   = $sites   [$this->post["further"]];
				}
			}	
		}

		$msg = sprintf("Could not update status/location information for %d equipments (%s) since the current status/location does not allow it!", count($notAllowed), implode(", ", array_map("strval", $notAllowed)));
		if(count($notAllowed)==count($eqids)){
			$this->vb->error($msg);
			return false;
		}
		if(count($notAllowed)>0)
			$this->vb->warning($msg);

		$this->db->push();
		if($this->db->error()){
			$this->vb->error("Equipment attribute could not be updated!", true);
			return false;
		}

		$this->vb->success("Equipment attribute updated successfully!");
		return true;
	}
}

$page = new EditItemsPage($this, "edititems");


?>
