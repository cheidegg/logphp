<?php

require "library/page.php";


// News Page
// ============================================
class NewsPage extends Page {


	// load
	// ---------------------------------------- 
	public function load() {
		/* Returns the content HTML when page is invoked via the menu */
		//if($this->master->getHost()=="hefr13") {
		//	return $this->html->template("news_hefr13");
		//}
		$c = new DbConfig($this->master, "pillory");
		$c->reformat("COUNT(sh.ID_SHIPPINGHISTORY)"               , "number_open_items");
		$c->reformat("l.sitename"                                 , "production_site");
		$c->reformat("TO_CHAR(min(sh.shippingdate), 'YYYY-MM-DD')", "oldest_shipping");
		$c->reformat("(CASE
              WHEN ABS(min(SH.SHIPPINGDATE)-sysdate) > 180 THEN
          			'ancient'
              WHEN ABS(min(SH.SHIPPINGDATE)-sysdate) > 60 THEN
          			'way too old'
              WHEN ABS(min(SH.SHIPPINGDATE)-sysdate) > 30 THEN
          			'quite old'  
        		ELSE
          			(null)
        		END)", "expiry_date");
		$c->columns = array("production_site");
		//$c->columns = array("production_site", "oldest_shipping", "expiry_date", "number_open_items");
		$c->joinon  = "SH.SHIPPINGDESTINATION = L.ID_LOCATIONS";
		$c->select("SH.RECEIVINGDATE", "null");
		$c->group ("l.sitename");
		$c->order ("oldest_shipping");
		$view = $this->db->read("pillory", $c);
		$rows = array();
		foreach($view as $row){
			$col=NULL; $weight=NULL;
			switch($row->expiry_date) {
				case "ancient"    : $col = "red"    ; $weigth = "bold"  ; break;   		      
				case "way too old": $col = "tomato" ; $weigth = "bold"  ; break;
				case "quite old"  : $col = "sienna" ; $weigth = "bold"  ; break;
				default           : $col = "#000000"; $weigth = "normal";   		      		   
			}
			array_push($rows, $this->html->template("news_pillory_row", array("site"    =>$row->production_site,
			                                                                  "oldestsh"=>$row->oldest_shipping,
			                                                                  "color"   =>$col,
			                                                                  "weight"  =>$weight,
			                                                                  "expdate" =>$row->expiry_date,
			                                                                  "numopit" =>$row->number_open_items)));
		}
		$pillory = $this->html->template("news_pillory", array("rows"=>implode("", $rows)));
		return $this->html->template("news", array("thepillory"=>$pillory));
	}

	public function submit(){
		if($this->post["do"]=="qaqc") {
			$_SESSION["theme"] = "qaqc";
			header('Location: '.rtrim(file_get_contents("configs/self"), "\n").'page.php');
		}
	}

}

$page = new NewsPage($this, "news");


?>
