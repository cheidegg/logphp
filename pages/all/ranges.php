<?php

require_once "library/page.php";
require_once "pages/all/splitting.php";


// RangesEntry
// ============================================
class RangesEntry {
	/* A container for a couple of variables, useful
	* for a nice handling of all the items in a range */

	// members
	// ----------------------------------------
	public $mode;
	public $emphasized;
	public $eqid;
	public $mtfid;
	public $otherid;
	public $quantity;

	// edititems and sendequip pages
	public $unit;
	public $status;
	public $site;

	// only print page
	public $parstart;
	public $parend;
	public $eqtypecode;
	public $projname;
	public $subbatchid;

	// print page and receiveequip
	public $eqtypename;

	// only receiveequip
	public $shipId;
	public $intId;
	public $fromId;
	public $toId;
	public $date;
	public $dateRaw;

	// __construct
	// ----------------------------------------
	public function __construct($row=NULL, $mode=0){
		/* Constructor, loading from DbRow instance */
		$this->mode       = $mode;
		$this->fromId     = -1;
		$this->toId       = -1;
		if(empty($row)) return;
		$this->emphasized = !empty($row["emphasized"]) ? $row["emphasized"] : false;
		$this->eqid       = $row["e_id_equipment"   ];
		$this->mtfid      = $row["e_partsbatchmtfid"];
		$this->otherid    = $row["e_otherid"        ];
		$this->quantity   = !empty($row["e_quantity"]) ? $row["e_quantity"] : 1;
		if($mode==0){
			$this->unit   = array_key_exists("eq_quantityunit", $row) ? $row["et_quantityunit"] : "";
			$this->status = $row["s_statusname"];
			$this->site   = $row["l_sitename"  ];
		}
		else if($mode==1){
			$this->parstart   = $row["sv_parstart"  ];
			$this->parend     = $row["sv_parend"    ];
			$this->eqtypecode = $row["et_eqtypecode"];
			$this->eqtypename = $row["et_eqtypename"];
			$this->projname   = $row["pj_projname"  ];
			$this->subbatchid = $row["e_subbatchid" ];
		}
		else {
			$this->shipId     = $row["sh_id_shippinghistory"];
			$this->intId      = $row["sh_shipmentintid"];
			$this->eqtypename = $row["et_eqtypename"];
			$this->fromId     = $row["sh_shippingfrom"];
			$this->toId       = $row["sh_shippingdestination"];
			$this->date       = timestamp(1, $row["sh_shippingdate"]);
			$this->dateRaw    = timestamp(0, $row["sh_shippingdate"]);
		}
	}

	// load
	// ----------------------------------------
	public function load($string){
		/* extracting from string as in POST field */
		$expl = explode(",", $string);
		if(($this->mode==0 && count($expl)!=7) || ($this->mode==1 && count($expl)!=10) || ($this->mode==2 && count($expl)!=10)) return;
		$this->eqid     = intval($expl[0]);
		$this->mtfid    = $expl[1];
		$this->otherid  = $expl[2];
		$this->quantity = intval($expl[3]);
		if($this->mode==0){
			$this->unit   = $expl[4];
			$this->status = $expl[5];
			$this->site   = $expl[6];
		}
		else if($this->mode==1){
			$this->parstart   = $expl[4];
			$this->parend     = $expl[5];
			$this->eqtypename = $expl[6];
			$this->projname   = $expl[7];
			$this->subbatchid = $expl[8];
		}
		else {
			$this->shipId     = $expl[4];
			$this->intId      = $expl[5];
			$this->eqtypename = $expl[6];
			$this->fromId     = $expl[7];
			$this->toId       = $expl[8];
			$this->date       = $expl[9];
		}
	}

	// save
	// ----------------------------------------
	public function save(){
		/* storing in string to be written into form */
		if($this->mode==0)
			return sprintf("%d,%s,%s,%d,%s,%s,%s", $this->eqid, $this->mtfid, $this->otherid, $this->quantity, $this->unit, $this->status, $this->site);
		else if($this->mode==1)
			return sprintf("%d,%s,%s,%d,%s,%s,%s,%s,%s,%d", $this->eqid, $this->mtfid, $this->otherid, $this->quantity, $this->parstart, $this->parend, $this->eqtypecode,$this->eqtypename, $this->projname, $this->subbatchid);
		return sprintf("%d,%s,%s,%d,%d,%d,%s,%d,%d,%s", $this->eqid, $this->mtfid, $this->otherid, $this->quantity, $this->shipId, $this->intId, $this->eqtypename,$this->fromId, $this->toId, $this->date);
	}
}


// rangesLoadItems
// -------------------------------------------- 
function rangesLoadItems($page, $mode=0) {
	/* Retrieving the already added items from the list */
	$page->items = array();
	if(empty($page->post["items"])) return;
	foreach(explode(";", $page->post["items"]) as $all) {
		array_push($page->items, array());
		foreach(explode(":", $all) as $line){
			if(strpos($line, ",")===false) continue;
			$e = new RangesEntry(NULL, $mode);
			$e->load($line);
			array_push($page->items[count($page->items)-1], $e);
		}
	}
}			


// rangesLoadPage
// -------------------------------------------- 
function rangesLoadPage($page, $doRmRange, $doBrkLink, $title, $careForSite=false, $careForGroup=false, $mode=0, $useDefHead=true, $customHead=NULL){
	/* Builds the top part of a form that contains ranges of equipments */

	// header
	$page->html->set("select_mtf"    , !empty($page->post["typeId"]) && $page->post["typeId"]=="mtf"   ? "emphasized" : "");
	$page->html->set("select_eqid"   , !empty($page->post["typeId"]) && $page->post["typeId"]=="eqid"  ? "emphasized" : "");
	$page->html->set("select_eqalias", !empty($page->post["typeId"]) && $page->post["typeId"]=="alias" ? "emphasized" : "");

	$checked  = "";
	$start1   = "single ID";
	$start2   = "single ID / start range #";
	$end1     = "";
	$end2     = "end range #";
	$start    = $start1;
	$end      = $end1;
	$disabled = "disabled";
	if(array_key_exists("chooseRange", $page->post) && $page->post["chooseRange"]==1){
		$checked  = "checked";
		$start    = $start2;
		$end      = $end2;
		$disabled = "";
	}
	$page->html->set("check_chooseRange", $checked );
	$page->html->set("startRangePlhd"   , $start   );
	$page->html->set("startRangePlhd1"  , $start1  );
	$page->html->set("startRangePlhd2"  , $start2  );
	$page->html->set("endRangePlhd"     , $end     );
	$page->html->set("endRangePlhd1"    , $end1    );
	$page->html->set("endRangePlhd2"    , $end2    );
	$page->html->set("endRangeDisabled" , $disabled);

	// buffered items
	$allSites        = $page->master->getOptionsSites();
	$rows            = array();
	$items           = array();
	$count           = 0;
	$faulty          = 0;
	$page->firstSite = NULL;
	if(count($page->items)>0){
		$page->firstSite = $page->items[0][0]->site;
		$heritage = $page->db->batchheritage;
		$i = 0;
		foreach($page->items as $idx=>$range){

			$count += count($range);

			$tdClass = $range[0]->emphasized ? "light" : "";
			$these   = array();
			$units   = array();
			$status  = $range[0]->status;
			$site    = $range[0]->site;
			foreach($range as $item){
				array_push($units, $item->unit);
				if($mode!=1){
					if($item->status != $status         ) $status = "various";
					if($item->site   != $site           ) $site   = "various";
					if($careForSite && $item->site != $page->firstSite) {
						$faulty  = 1;
						$tdClass = "foundFaultItems";
					}
					if($careForGroup){
						$cp = new DbConfig($page->master, "findingparent");
						$cp->column = "PARENTBATCHEQENTRYID";
						$cp->select(NULL, strtoupper("connect_by_isleaf = 1 start with eqentryid='".$item->eqid."' connect by prior parentbatcheqentryid = eqentryid"));
						$heritage->read($cp);
						unset($cp);
						if($heritage->count()==1) {
							$faulty = 2;
							$tdClass = "foundFaultItems";
						}
					}
				}
				array_push($these, $item->save());
			}
			
			$startIds = sprintf("MTFID: %s<br />EQID : %d<br />ALIAS: %s", $range[0]->mtfid, $range[0]->eqid, $range[0]->otherid);
			$endIds   = "";
			if(count($range)>1){
				$l = count($range)-1;
				$endIds = sprintf("MTFID: %s<br />EQID : %d<br />ALIAS: %s", $range[$l]->mtfid, $range[$l]->eqid, $range[$l]->otherid);
			}

			$sums  = array();
			$units = array_unique($units);
			foreach($units as $unit){
				$sum = 0;
				foreach($range as $item)
					$sum += $item->quantity;
				array_push($sums, sprintf("%d %s", $sum, $unit));
			}
			$rangeSum = "<span style=\"font-size:80%;\">".implode("<br />", $sums)."</span>";

			array_push($items, implode(":", $these));
			$checked = isset($page->post["subRanges"]) && in_array($idx+1, $page->post["subRanges"]) ? "checked" : "";
			$lastItemId = $i==count($page->items)-1 ? "id='lastItem'" : "";
			array_push($rows, $page->html->template("ranges_row".strval($mode), array("tdClass"          =>$tdClass,
			                                                                          "lastItemId"       =>$lastItemId,
			                                                                          "checked_subRanges"=>$checked,
			                                                                          "subRangeId"       =>strval($idx+1),
			                                                                          "eqId"             =>$range[0]->eqid,
			                                                                          "mtfId"            =>$range[0]->mtfid,
			                                                                          "otherId"          =>$range[0]->otherid,
			                                                                          "startIds"         =>$startIds,
			                                                                          "endIds"           =>$endIds,
			                                                                          "rangeLen"         =>count($range),
			                                                                          "rangeSum"         =>$rangeSum,
			                                                                          "status"           =>$status,
			                                                                          "site"             =>$site,
			                                                                          "parstart"         =>$range[0]->parstart,
			                                                                          "parend"           =>$range[0]->parend,
			                                                                          "projname"         =>$range[0]->projname,
			                                                                          "eqtypename"       =>$range[0]->eqtypename,
			                                                                          "shipId"           =>$range[0]->shipId,
			                                                                          "intId"            =>$range[0]->intId,
			                                                                          "siteFrom"         =>$range[0]->fromId>=0 ? $allSites[$range[0]->fromId] : "",
			                                                                          "siteTo"           =>$range[0]->toId  >=0 ? $allSites[$range[0]->toId] : "",
			                                                                          "shipDate"         =>$range[0]->date), NULL, "all"));
		}
	}

	$page->html->set("faulty"          , $faulty>0 ? 1 : 0        );
	$page->html->set("myItems"         , implode(";", $items)     );
	$page->html->set("rangeCountTotal" , $useDefHead ? $count : "");
	$page->html->set("rowsRangeTable"  , implode("" , $rows )     );

	$page->html->set("faultyElements"  , $faulty>0 ? $page->html->template("ranges_warning".strval($faulty), array(), NULL, "all") : "");
	$page->faulty = $faulty;


	$optionRmRange = "";
	if($doRmRange) $optionRmRange = sprintf("<option value='rmranges' %s>remove range(s)</option>", !empty($page->post["withselected"]) && $page->post["withselected"]=="rmranges"  ? "emphasized" : "");
	$optionBreakLink = "";
	if($doBrkLink) $optionBreakLink = sprintf("<option value='breaklink' %s>break link to group</option>", !empty($page->post["withselected"]) && $page->post["withselected"]=="breaklink" ? "emphasized" : "");

	$page->html->set("colspan"        , in_array($mode, array(0, 2)) ? 5  : 8                   );
	$page->html->set("infCols"        , in_array($mode, array(0, 2)) ? "" : "<td></td>"         ); // ugly, I know..
	$page->html->set("addCols"        , in_array($mode, array(0, 2)) ? "" : "<td></td><td></td>"); // ugly, I know..

	$page->html->set("title"          , $title          );
	$page->html->set("optionRmRange"  , $optionRmRange  );
	$page->html->set("optionBreakLink", $optionBreakLink);

	$page->html->set("colgroup"  , $page->html->template("ranges_header_cols".strval($mode), array(), NULL, "all"));
	$page->html->set("colhead"   , $page->html->template("ranges_header_head".strval($mode), array(), NULL, "all"));
	if($useDefHead) 
		$page->html->set("defHead"   , $page->html->template("ranges_header_defHead"       , array(), NULL, "all"));
	if(!empty($customHead)) 
		$page->html->set("customHead", $customHead);
	$page->html->set("header"    , $page->html->template("ranges_header"                   , array(), NULL, "all"));

}


// rangesSubmitAddRange
// -------------------------------------------- 
function rangesSubmitAddRange($page){
	/* Preparing id range for rangesSubmitAdd methods */

	if(!isset($page->post["typeId"]) || empty($page->post["typeId"])) return array();

	$hasRange = isset($page->post["chooseRange"]) && $page->post["chooseRange"]==1;
	$type     = $page->post["typeId"];

	$startId  = !empty($page->post["startRange"]) ? $page->post["startRange"] : 0;
	$endId    = !empty($page->post["endRange"  ]) ? $page->post["endRange"  ] : NULL;

	if($type=="mtf"){
		$startId =             $page->master->getEqId($page->post["startRange"]);
		$endId   = $hasRange ? $page->master->getEqId($page->post["endRange"]  ) : -1;
	}
	else if($type=="alias"){
		$startId =             $page->master->getEqId(NULL, $page->post["startRange"]);
		$endId   = $hasRange ? $page->master->getEqId(NULL, $page->post["endRange"]  ) : -1;
	}

	if($startId==-1)
		return array();
	if($hasRange && $endId==-1)
		return array();

	if($hasRange){
		$col = $type=="mtf" ? "e.partsbatchmtfid" : ($type=="alias" ? "e.otherid" : "e.id_equipment");
		return array($page->post["startRange"], $page->post["endRange"], $col);
	}

	return array($startId, NULL, "e.id_equipment");
}

// rangesSubmitAdd
// -------------------------------------------- 
function rangesSubmitAdd($page, $cs, $mode=0, $store=false, $cause=""){
	/* Adding an equipment or a range of equipments */

	$cs->slim = true;
	$stview = $page->db->read("subtable", $cs); 
	if(count($stview->slim)==0) {
		$page->vb->error("Could not find any equipment items for the given input parameters!");
		return false;
	}

	$fullList = array();
	foreach($page->items as $range){
		foreach($range as $item){
			array_push($fullList, $item->eqid);
		}
	}

	$eqIds = array();
	if(in_array($mode, array(1, 2))){ // store separately in printer view (mode=1) and receiveequip (2)
		$old = $store ? $page->getDbItems() : array();
		foreach($stview->slim as $row){
			if(in_array($row["e_id_equipment"], $fullList)) continue; // every equipment only once
			array_push($page->items, array());
			array_push($page->items[count($page->items)-1], new RangesEntry($row, $mode));
			if(in_array($row["e_id_equipment"], $old)) continue;
			array_push($eqIds, $row["e_id_equipment"]); // to write to disk
		}
	}
	else{
		$toAdd = array();
		foreach($stview->slim as $row){
			if(in_array($row["e_id_equipment"], $fullList)) continue; // every equipment only once
			array_push($toAdd, new RangesEntry($row, $mode));
			array_push($eqIds, $row["e_id_equipment"]);
		}
		if(count($toAdd)>0) array_push($page->items, $toAdd);
	}

	if($store) $page->storeDbItems($eqIds);

	return true;
}

// rangesSubmitAddAnyway
// -------------------------------------------- 
function rangesSubmitAddAnyway($page, $mtfId, $mode=0){
	/* Adding an MTF ID that does not yet exist */

	if($mode!=1) return true; // only for printer view (mode=1) yet

	$row = array("e_id_equipment"    => 0,
	             "e_partsbatchmtfid" => $mtfId,
	             "e_otherid"         => NULL,
	             "sv_parstart"       => NULL,
	             "sv_parend"         => NULL,
	             "et_eqtypecode"     => NULL,
	             "et_eqtypename"     => NULL,
	             "pj_projname"       => NULL,
	             "e_subbatchid"      => NULL,
	             "emphasized"        => true);
	array_push($page->items, array());
	array_push($page->items[count($page->items)-1], new RangesEntry($row, $mode));
	return true;
}

// rangesSubmitAddMode0
// -------------------------------------------- 
function rangesSubmitAddMode0($page, $isMultiEdit=false, $eqIds=array()){
	/* Adding an equipment or a range of equipments in mode 0 (edititems and sendequip pages) */

	$range = array();
	if(empty($eqIds)){
		$range = rangesSubmitAddRange($page);
		if(empty($range)) return false;
	}

	$locLock  = array_keys($page->master->getOptionsSites (!$isMultiEdit ? "priorshipflag" : "prioreditmiflag", "F"));
	$statLock = array_keys($page->master->getOptionsStatus(!$isMultiEdit ? "priorshipflag" : "prioreditmiflag", "F"));

	$cj = new DbConfig($page->master, "cj");
	$cj->column = "eqentryid";
	$cj->reformat("FIRST_VALUE(STATUSID   IGNORE NULLS) OVER (PARTITION BY EQENTRYID ORDER BY EVENTDATE DESC)", "last_status"  );
	$cj->reformat("FIRST_VALUE(MAJORLOCID IGNORE NULLS) OVER (PARTITION BY EQENTRYID ORDER BY EVENTDATE DESC)", "last_location");
	$cj->select("isvalidflag", "T");
	$cj->table = "statuslocation";

	$cs = new DbConfig($page->master, "subtable");
	$cs->columns = array("distinct e.id_equipment", "e.partsbatchmtfid", "e.subbatchid", "e.otherid", "e.quantity", "s.statusname", "l.sitename");
	if(!$isMultiEdit) $cs->column = "et.quantityunit";
	$cs->joinconfig = $cj;
	$cs->joinon = "*cj.eqentryid      = e.id_equipment";
	$cs->joinon = "s.id_statuses        = last_status";
	$cs->joinon = "l.id_locations       = last_location";
	if(!$isMultiEdit) $cs->joinon = "et.id_equipmenttypes = e.eqtypecodeid";
	$cs->select("s.statusname"     , "not null"             );
	$cs->select("l.sitename"       , "not null"             );
	$cs->slim = true;
	if(count($statLock)>0) $cs->select("last_status"  , $statLock, "neq");
	if(count($locLock )>0) $cs->select("last_location", $locLock , "neq");
	if($isMultiEdit){
		$cs->select("e.subbatchid", "null"); 
		$cs->select("e.quantity"  , "null");
	}
	if(count($range)>1 && $range[1]==$range[0]) $range[1] = NULL;
	if(count($eqIds)>0      ) $cs->select("e.id_equipment", $eqIds   );
	else if(empty($range[1])) $cs->select($range[2]       , $range[0]); 
	else                      $cs->select($range[2]       , array(array($range[0], $range[1])), "inrange"); 
	$cs->order("e.id_equipment");

	$cause = "";
	if(!$isMultiEdit)
		$cause = "<br />The equipment in question violates one of the following constraints:<br />(1) It must have a status and a location.<br />(2) It may not have status 'fully split up', 'in transit' or 'undefined'.<br />(3) It may not have location 'see parent' and thus be parented.";
	else 
		$cause = "<br />The equipment in question violates one of the following constraints:<br />(1) It must have a status and a location.<br />(2) It may not have status 'fully split up'.<br />(3) It must have a valid SubBatchId and a nonzero quantity value.";

	return rangesSubmitAdd($page, $cs, 0, false, $cause);
}


// rangesSubmitAddMode1
// -------------------------------------------- 
function rangesSubmitAddMode1($page, $eqIds=array()){
	/* Adding an equipment or a range of equipments in mode 1 (print page) */

	// adding MTFs that do not yet exist
	if(empty($eqIds) && isset($page->post["typeId"]) && $page->post["typeId"]=="mtf"){
		$startEqId = $page->master->getEqId($page->post["startRange"]);
		$endEqId   = !empty($page->post["endRange"]) ? $page->master->getEqId($page->post["endRange"]) : -1;
		if($startEqId==-1 && $endEqId==-1){ // start and end must be -1 (end will be -1 even if not given)
			// only start MTF given
			if(empty($page->post["endRange"]))
				return rangesSubmitAddAnyway($page, $page->post["startRange"], 1);
			// range of not yet existing MTFs given
			if(substr($page->post["startRange"], 0, 9)==substr($page->post["endRange"], 0, 9)){
				$s = intval(substr($page->post["startRange"], 9, 5));
				$e = intval(substr($page->post["endRange"  ], 9, 5));
				if($s>$e) {$b=$e; $e=$s; $s=$b;} // turn it around
				for($i=$s; $i<=$e; $i++){
					$mtf = substr($page->post["startRange"], 0, 9).str_pad(strval($i),5,"0",STR_PAD_LEFT);
					if(!rangesSubmitAddAnyway($page, $mtf, 1)) return false;
				}
				return true;
			}
			return false; // cannot continue below because given MTFs do not exist
		}
	}	

	$range = array();
	if(empty($eqIds)){
		$range = rangesSubmitAddRange($page);
		if(empty($range)) return false;
	}

	$cs = new DbConfig($page->master, "subtable");
	$cs->columns = array("distinct e.id_equipment", "e.partsbatchmtfid", "e.subbatchid", "e.otherid", "e.quantity", "sv.parstart", "sv.parend", "et.eqtypename", "pj.projname", "et.eqtypecode");
	$cs->joinon = "e.id_equipment = sv.eqentryid";
	$cs->joinon = "e.eqtypecodeid = et.id_equipmenttypes";
	$cs->joinon = "et.projectid = pj.id_projects";
	//$cs->joinon = "etgc.eqtypecode = et.eqtypecode"; // doubles the result
	$cs->order("e.id_equipment");
	$cs->slim = true;

	if(count($range)>1 && $range[1]==$range[0]) $range[1] = NULL;
	if(count($eqIds)>0      ) $cs->select("e.id_equipment", $eqIds);
	else if(empty($range[1])) $cs->select($range[2]       , $range[0]); 
	else                      $cs->select($range[2]       , array(array($range[0], $range[1])), "inrange"); 

	// add items
	return rangesSubmitAdd($page, $cs, 1, count($eqIds)==0 ? true : false);
}


// rangesSubmitAddMode2
// -------------------------------------------- 
function rangesSubmitAddMode2($page, $shipIntId=-1, $siteFromId=-1, $siteToId=-1){
	/* Adding an equipment or a range of equipments in mode 2 (receiveequip page) */

	$cs = new DbConfig($page->master, "subtable");
	$cs->columns = array("distinct e.id_equipment", "e.partsbatchmtfid", "e.subbatchid", "e.otherid", "e.quantity", "et.eqtypename", "sh.id_shippinghistory", "sh.shippingfrom", "sh.shippingdestination", "sh.shippingdate", "sh.shipmentintid");
	$cs->joinon = "sh.eqentryid = e.id_equipment";
	$cs->joinon = "e.eqtypecodeid = et.id_equipmenttypes";
	if($shipIntId>0) 
		$cs->select("sh.shipmentintid"      , $shipIntId );
	if($siteFromId>0)
		$cs->select("sh.shippingfrom"       , $siteFromId);
	else
		$cs->select("sh.shippingfrom"       , "not null" );
	if($siteToId  >0)
		$cs->select("sh.shippingdestination", $siteToId  );
	else
		$cs->select("sh.shippingdestination", "not null" );
	$cs->select("sh.receivingdate"      , NULL);
	$cs->order("sh.shipmentintid", "asc");
	$cs->order("e.id_equipment"  , "asc");
	$cs->slim = true;

	// add items
	return rangesSubmitAdd($page, $cs, 2);
}


// rangesSubmitGo
// -------------------------------------------- 
function rangesSubmitGo($page, $remove=false){
	/* Removing or editing entities from the range */

	// remove
	if($page->post["withselected"] == "rmranges"){

		$eqids = array();
		foreach($page->post["subRanges"] as $idx){
			$idx -= 1; // +1 in checkbox value
			if(!array_key_exists($idx, $page->items)) continue;
			foreach($page->items[$idx] as $re)
				array_push($eqids, $re->eqid);
			unset($page->items[$idx]);
		}
		if($remove){
			$page->removeDbItems($eqids);
		}
		$page->post["subRanges"] = array();
		$page->items = array_values($page->items);
		return true;
	}


	// break link
	else if($page->post["withselected"] == "breaklink"){

		$items = array();
		foreach($page->post["subRanges"] as $idx)
			array_push($items, $idx-1); // +1 in checkbox value

		// regroup items per parent within the range
		$heritage = $page->db->batchheritage;
		foreach($page->items as $rkey => $range){

			if(!in_array($rkey, $items)) continue; // only selected ranges
			$grouped = array();
			foreach($range as $item){
				$cp = new DbConfig($page->master, "findingparent");
				$cp->column = "PARENTBATCHEQENTRYID";
				$cp->select(NULL, strtoupper("connect_by_isleaf = 1 start with eqentryid='".$item->eqid."' connect by prior parentbatcheqentryid = eqentryid"));
				$heritage->read($cp);
				if($heritage->count()!=1) continue;
				$parent = $heritage->parentbatcheqentryid;
				if(!array_key_exists($parent, $grouped)) $grouped[$parent] = array();
				array_push($grouped[$parent], $item->eqid); // only keep the eqId
			}
		}

		// loop over every group that holds items
		foreach($grouped as $parEqId => $items){

			if($parEqId==0     ) continue; // only proper groups
			if(count($items)==0) continue; // only proper groups with multiple items

			if(!splittingIndependent($page, $parEqId, $items)){
				$page->vb->error("Could not copy data to new items!", true);
				return false;
			}

			// if managed till here, all data inserted properly, no remove batchheritage links
			foreach($items as $eqId){
				$page->db->writeSql(sprintf("DELETE FROM ATLAS_MUON_NSW_MM_LOG.BATCHHERITAGE WHERE EQENTRYID=%d", $eqId)); // FIXME: rewrite in better form via DbRows
				if($page->db->error()){
					$page->vb->error("Could not break batch heritage links!", true);
					return false;
				}
			}
		}

		// if managed till here, all is well
		$page->vb->success("Links broken successfully!");
		return true;
	}
}


?>
