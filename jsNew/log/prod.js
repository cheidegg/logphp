
// togglePlots
// --------------------------------------------
function togglePlots(id) {
	var plot = document.getElementById(id);
	if (plot.style.display == null || plot.style.display == "none") {
		plot.style.display = "block";		
	}
	else {
		plot.style.display = "none";
	}			
}
