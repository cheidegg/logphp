
// toggleDisplayOnOff
// --------------------------------------------
function toggleDisplayOnOff(id) {
	var x = document.getElementById(id);
	if (x.style.display === "none") {
		x.style.display = "block";
		if (id == "cTableSettings") {
			document.getElementById("cTable").style.display = "none";
		}               
	} else {
		x.style.display = "none";
	}                       
}

