<?php

require_once "box.php";



// CachePure
// ============================================
trait CachePure {
    /* An object that can hold other objects and iterate through them;
	* CachePure = Cache without Box, to be used in conjunction with
	* other, boxed classes or traits */


	// members
	// ---------------------------------------- 
	public    $cache        = array();
	protected $iter         = 0;
	protected $keyname      = "name";
	public    $hierarchical = false;
	protected $virtualList  = array();


	// construct
	// ---------------------------------------- 
	public function constructCachePure($keyname = "name"){
		/* Constructor */
		$this->cache        = array();
		$this->iter         = 0;
		$this->hierarchical = false;
		$this->virtualList  = array();
		$this->keyname      = $keyname;
	}




	// individual items
	// ========================================

	// __get
	// ---------------------------------------- 
	public function __get($key) {
		/* Returns the item with name $key if it exists */
		return $this->get($key);
	}

	// __isset
	// ---------------------------------------- 
	public function __isset($key) {
		/* Checks if the cache holds an item with name $key */
		return $this->has($key);
	}

	// __set
	// ---------------------------------------- 
	public function __set($key, $value) {
		/* Updates the item ith name $key */
		$this->set($key, $value);
	}

	// __unset
	// ---------------------------------------- 
	public function __unset($key) {
		/* Removes the object with name $key */
		$this->del($key);
	}

	// add
	// ---------------------------------------- 
	public function add($key, $value) {
		/* Updates the object with name $key; if the object already
		* exists, it is ignored */
		if($this->has($key)) return;
		$this->cache[$key] = $value;
	}

	// count
	// ---------------------------------------- 
	public function count() {
		/* Return the number of stored elements */
		return count($this->cache);
	}

	// del
	// ---------------------------------------- 
	public function del($key) {
		/* Removes the object with name $key */
		unset($this->cache[$key]);
	}

	// find
	// ---------------------------------------- 
	public function find($value) {
		/* Checks if the cache holds an object $value */
		return in_array($value, $this->cache);
	}

	// free
	// ---------------------------------------- 
	public function free() {
		/* Clears the entire memory of the cache and destroys
		* all objects in doing so */
		$this->cache = array();
	}

	// get
	// ---------------------------------------- 
	public function get($key) {
		/* Returns the object with name $key if it exists */
		if(!$this->has($key)) return NULL;
		return $this->cache[$key];
	}

	// has
	// ---------------------------------------- 
	public function has($key) {
		/* Checks if the cache holds an object with name $key */
		return array_key_exists($key, $this->cache);
	}

	// rebase
	// ---------------------------------------- 
	public function rebase() {
		/* Reassign key to value for all items in the cache; if key of the cache
		* is not the same as the value stored within the object, the key is updated,
		* unless it is already assigned to another object */
		if(count($this->cache)==0) return;
		foreach($this->cache as $key=>$obj){
			if($key==$obj->{$this->keyname}) continue;
			if(!array_key_exists($this->cache, $obj->{$this->keyname}))
				$this->cache[$obj->{$this->keyname}] = $obj; // new ref
			$this->del($key); // remove old ref
		}
	}

	// retrieve
	// ---------------------------------------- 
	public function retrieve($value) {
		/* Returns the key to the object $value if it is held by 
		* the cache, otherwise NULL */
		if(!$this->find($value)) return NULL;
		return array_search($value, $this->cache);
	}

	// set
	// ---------------------------------------- 
	public function set($key, $value) {
		/* Updates the object with name $key */
		$this->cache[$key] = $value;
	}




	// item iteration
	// ========================================

	// current
	// ---------------------------------------- 
	public function current() {
		/* Iteration function, returns pointer to current object */
		if($this->hierarchical) {
			$keys = array_keys($this->virtualList);
			return $this->virtualList[$keys[$this->iter]];
		}
		$keys = array_keys($this->cache);
		return $this->cache[$keys[$this->iter]];
	}
	
	// key
	// ---------------------------------------- 
	public function key() {
		/* Iteration function, returns key to current object */
		if($this->hierarchical) {
			$keys = array_keys($this->virtualList);
			return $keys[$this->iter];
		}
		$keys = array_keys($this->cache);
		return $keys[$this->iter];
	}
	
	// next
	// ---------------------------------------- 
	public function next() {
		/* Iteration function, iterates through objects */
		++$this->iter;
	}

	// rewind
	// ---------------------------------------- 
	public function rewind() {
		/* Iteration function, sets pointer back to first object */
		$this->iter = 0;
		$this->buildVirtualList();
	}
	
	// valid
	// ---------------------------------------- 
	public function valid() {
		/* Iteration function, checks if current pointer is valid */
		return $this->iter < count($this->cache);
	}




	// item hierarchy
	// ========================================

	// buildVirtualList
	// ---------------------------------------- 
	protected function buildVirtualList(){
		/* Builds the virtual list for iteration accounting for the
		* hierarchical parent-child structure */
		$this->virtualList = array();
		foreach($this->getParents() as $parent){
			array_push($this->virtualList, $parent);
			$this->virtualList = array_merge($this->virtualList, $parent->children(false));
		}
	}

	// getChildren
	// ---------------------------------------- 
	public function getChildren($parentname, $direct=true){
		/* Returns the list of children for a given parent */
		if(!$this->has($parentname)) return array();
		$parent = $this->get($parentname);
		return $parent->children($direct);
	}

	// getParent
	// ---------------------------------------- 
	public function getParent($childname=NULL){
		/* Returns the direct parent for a given child */
		if(!$this->has($childname)) return array();
		$child = $this->get($childname);
		return $child->parent;
	}

	// getParents
	// ---------------------------------------- 
	public function getParents($childname=NULL){
		/* Returns the list of parents for a given child */
		if(empty($childname)){
			$result = array();
			foreach($this->cache as $item){
				if(!empty($item->parent)) continue;
				array_push($result, $item);
			}
			return $result;
		}
		if(!$this->has($childname)) return array();
		$child = $this->get($childname);
		return $child->parents();
	}

	// sub
	// ---------------------------------------- 
	public function sub($key, $value, $parent){
		/* Adds a new item as child to a parent (subordinate) */
		if(!$this->has($parent->{$this->keyname})) $this->set($parent->{$this->keyname}, $parent);
		$this->set($key, $value);
		$parent->addChild($value);
	}

	// super
	// ---------------------------------------- 
	public function super($key, $value, $child){
		/* Adds a new item as parent to a child (superordinate) */
		if(!$this->has($child->{$this->keyname})) $this->set($child->{$this->keyname}, $child);
		$this->set($key, $value);
		$value->addChild($child);
	}




	// object-level functionality
	// ========================================

	// complement
	// ---------------------------------------- 
	public function complement($other) {
		/* Returns the list of items that are in the other cache
		* but not in this one. Note, the items are not cloned, rather
		* their references are returned. */
		$result = array();
		if(empty($other) || !is_object($other) || !property_exists($other, "cache")) return $result;
		foreach($other->cache as $entry){
			if($this->has($entry->{$this->keyname})) continue;
			$result[$entry->{$this->keyname}] = $entry;
		}
		return $result;
	}

	// duplicate
	// ---------------------------------------- 
	public function duplicate($newname=NULL) {
		/* Returns a new instance of the cache with all items cloned
		* as well */
		$new = new Cache($this->master, !empty($newname) ? $newname : $this->master->cloneION($this->name));
		foreach($this->cache as $key=>$obj)
			$new->set($key, $obj->duplicate());
		return $new;
	}

	// intersection
	// ---------------------------------------- 
	public function intersection($other) {
		/* Returns the list of items that are both in the other cache
		* and also in this one. Note, the items are not cloned, rather
		* their references are returned. */
		$result = array();
		if(empty($other) || !is_object($other) || !property_exists($other, "cache")) return $result;
		foreach($other->cache as $entry){
			if(!$this->has($entry->{$this->keyname})) continue;
			$result[$entry->{$this->keyname}] = $entry;
		}
		return $result;
	}

	// isNull
	// ---------------------------------------- 
	public function isNull(){
		/* Checks if the cache contains zero data */
		return (count($this->cache)==0);
	}

	// merge
	// ---------------------------------------- 
	public function merge($other) {
		/* Adds the items from another cache into this one. If 
		* the key is already assigned to an item, it is overwritten. */
		if(empty($other) || !is_object($other) || !property_exists($other, "cache")) return;
		foreach($other->cache as $key=>$entry)
			$this->set($key, $entry->duplicate());
	}

	// sum
	// ---------------------------------------- 
	public function sum($other) {
		/* Adds the items from another cache into this one. If 
		* the key is already assigned to an item, the entry is
		* ignored */
		if(empty($other) || !is_object($other) || !property_exists($other, "cache")) return;
		foreach($other->cache as $entry){
			if($this->has($entry->{$this->keyname})) continue;
			$this->set($entry->{$this->keyname}, $entry->duplicate());
		}
	}

	// union
	// ---------------------------------------- 
	public function union($other) {
		/* Returns the array of items that are in this or
		* the other cache; in conflicting cases, the items of
		* this cache are preferred; Note that the entries are
		* not copied, rather the references to them are returned. */ 
		$result = array();
		if(empty($other) || !is_object($other) || !property_exists($other, "cache")) return $result;
		foreach($other->cache as $entry) $result[$entry->{$this->keyname}] = $entry;
		foreach($this ->cache as $entry) $result[$entry->{$this->keyname}] = $entry;
		return $result;
	}
}




// Cache
// ============================================
trait Cache {
    /* Cache is CachePure with Box */

	// parent trait
	// ---------------------------------------- 
	use Box;
	use CachePure;

	// construct
	// ---------------------------------------- 
	public function constructCache($master, $name, $keyname="name"){
		/* Constructor */
		$this->constructBox      ($master, $name);
		$this->constructCachePure($keyname);
	}
}


?>
