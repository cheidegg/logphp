<?php

//require_once "box.php";


// Page Class
// ============================================
class Page {
	/* The mother template mega amazing class that
	* serves as fundament for all other classes. Contains
	* all functions which can be overloaded by the children. */

	// traits
	// ---------------------------------------- 
	//use Box;


	// START: copy-paste from Box (5.3 compatibility)
	// ========================================
	// ========================================

	// members
	// ---------------------------------------- 
	public    $master   = NULL;
	public    $name     = NULL;
	public    $isValid  = true;
	protected $subelms  = array();
	public    $parent   = NULL;

	// construct
	// ---------------------------------------- 
	public function constructBox($master, $name){
		/* Constructor */
		$this->master  = $master;
		$this->name    = $master->checkION($name) ? $name : $master->makeION($name);
		$this->master->registerION($this->name);
		$this->isValid = true;
		$this->subelms = array();
		$this->parent  = NULL;
	}

	// addChild
	// ---------------------------------------- 
	public function addChild($item){
		/* Adds another object as child to this entity */
		array_push($this->subelms, $item);
		$item->parent = $this;
	}

	// children
	// ---------------------------------------- 
	public function children($direct=true){
		/* Returns the list of children of this entity */
		if($direct) return $this->subelms;
		$result = array();
		foreach($this->subelms as $item){
			array_push($result, $item);
			$result = array_merge($result, $item->children(false));
		}
		return $result;
	}

	// delChild
	// ---------------------------------------- 
	public function delChild($item){
		/* Removes another object as child to this entity */
		if(!in_array($item, $this->subelms)) return;
		unset($this->subelms[array_search($item, $this->subelms)]);
	}

	// hasChild
	// ---------------------------------------- 
	public function hasChild($item){
		/* Removes another object as child to this entity */
		if(!in_array($item, $this->subelms)) return false;
		return true;
	}

	// parents
	// ---------------------------------------- 
	public function parents(){
		/* Returns the list of parents of this entity */
		if(empty($this->parent)) return array();
		$result = array();
		array_push($result, $this->parent);
		$item = $this->parent;
		while(true){
			if(empty($item->parent)) return $result;
			array_push($result, $item->parent);
			$item = $item->parent;
		}
		return $result;
	}

	// END: copy-paste from Box (5.3 compatibility)
	// ========================================
	// ========================================



	// members
	// ---------------------------------------- 
	public $id       = NULL; 
	public $db       = NULL; // all need to be public for common functions
	public $vb       = NULL;
	public $html     = NULL;
	public $lang     = NULL;
	public $globals  = array();
	public $get      = array();
	public $post     = array();

	// __construct
	// ---------------------------------------- 
	public function __construct($master, $name) {
		/* Constructor */
		$this->constructBox($master, $name);
		$this->id       = $name; // detach from ION stuff
		$this->db       = $master->db;
		$this->vb       = $master->vb;
		$this->html     = $master->html;
		$this->lang     = $master->lang;
		$this->globals  = $master->globals;
		$this->get      = $master->get;
		$this->post     = $master->post;
	}

	// load
	// ---------------------------------------- 
	public function load() {
		/* Returns the HTML code to be displayed in case
		* the page is accessed via the GET method. By default
		* empty, to be overloaded by children. */
		return "";
	}

	// permissions
	// ---------------------------------------- 
	public function permissions(){
		/* Returns true in case all requirements to access
		* this page (either GET or POST) have been met, false
		* otherwise. Default is true, to be overloaded by 
		* children. */
		return true;
	}

	// submit
	// ---------------------------------------- 
	public function submit() {
		/* Returns the HTML code to be displayed in case
		* the page is accessed via the POST method. By
		* default the same as load(), to be overloaded 
		* by children. */
		return $this->load();
	}

}


?>
