<?php

//require_once "box.php";
//require_once "cache.php";


// ContainerType
// ============================================
class ContainerType {
	/* A prescription for the values the associated
	* Container object can store. */


	// trait
	// ---------------------------------------- 
	//use Box;


	// START: copy-paste from Box (5.3 compatibility)
	// ========================================
	// ========================================

	// members
	// ---------------------------------------- 
	public    $master   = NULL;
	public    $name     = NULL;
	public    $isValid  = true;
	protected $subelms  = array();
	public    $parent   = NULL;

	// construct
	// ---------------------------------------- 
	public function constructBox($master, $name){
		/* Constructor */
		$this->master  = $master;
		$this->name    = $master->checkION($name) ? $name : $master->makeION($name);
		$this->master->registerION($this->name);
		$this->isValid = true;
		$this->subelms = array();
		$this->parent  = NULL;
	}

	// addChild
	// ---------------------------------------- 
	public function addChild($item){
		/* Adds another object as child to this entity */
		array_push($this->subelms, $item);
		$item->parent = $this;
	}

	// children
	// ---------------------------------------- 
	public function children($direct=true){
		/* Returns the list of children of this entity */
		if($direct) return $this->subelms;
		$result = array();
		foreach($this->subelms as $item){
			array_push($result, $item);
			$result = array_merge($result, $item->children(false));
		}
		return $result;
	}

	// delChild
	// ---------------------------------------- 
	public function delChild($item){
		/* Removes another object as child to this entity */
		if(!in_array($item, $this->subelms)) return;
		unset($this->subelms[array_search($item, $this->subelms)]);
	}

	// hasChild
	// ---------------------------------------- 
	public function hasChild($item){
		/* Removes another object as child to this entity */
		if(!in_array($item, $this->subelms)) return false;
		return true;
	}

	// parents
	// ---------------------------------------- 
	public function parents(){
		/* Returns the list of parents of this entity */
		if(empty($this->parent)) return array();
		$result = array();
		array_push($result, $this->parent);
		$item = $this->parent;
		while(true){
			if(empty($item->parent)) return $result;
			array_push($result, $item->parent);
			$item = $item->parent;
		}
		return $result;
	}

	// END: copy-paste from Box (5.3 compatibility)
	// ========================================
	// ========================================



	// members
	// ---------------------------------------- 
	public    $dtype     = NULL;
	public    $default   = NULL;
	protected $above     = NULL;
	protected $below     = NULL;
	protected $inset     = array();
	protected $outset    = array();
	protected $inranges  = array();
	protected $outranges = array();
	protected $intype    = array();
	protected $custom    = NULL;

	// __construct
	// ---------------------------------------- 
	public function __construct($master, $name, $dtype, $default, $custom=NULL, $above=NULL, $below=NULL, $intype=array(), $inset=array(), $outset=array(), $inranges=array(), $outranges=array()) {
		/* Constructor */
		$this->constructBox($master, $name);
		$this->dtype     = $dtype;
		$this->default   = $default;
		$this->above     = $above;
		$this->below     = $below;
		$this->intype    = $intype;
		$this->inset     = $inset;
		$this->outset    = $outset;
		$this->inranges  = $inranges;
		$this->outranges = $outranges;
		$this->custom    = $custom;
		$this->isValid   = true;
		$this->validate();
	}

	// __call
	// ---------------------------------------- 
	public function __call($method, $args){
		/* Executes a callable assigned to the object; i.e. necessary
		* to make the custom verification work */
		if(!is_callable(array($this, $method))) return;
		return call_user_func_array($this->$method, $args);
	}

	// validate
	// ---------------------------------------- 
	protected function validate() {
		/* Checks if the object is instantiated correctly. */
		$this->isValid = true;
		if(empty($this->dtype  )) $this->isValid = false;
		if(empty($this->default)) $this->isValid = false;
		if(!in_array($this->dtype, array("any", "array", "boolean", "double", "integer", "object", "string"))) $this->isValid = false;
		// FIXME: to be completed by semantical tests
	}

	// verify
	// ---------------------------------------- 
	public function verify($value, $raw) {
		/* Checks if a given value is compatible with the constraints in this object. */
		if(empty($value) && $raw=="null") return true;
		if(empty($value) && $value!==0 && $value!==0.0) return false;
		if(gettype($value)=="string") $value = trim($value);
		if(gettype($value)!=$this->dtype && $this->dtype!="any") return false;
		if(gettype($value)=="string" && !in_array($this->dtype, array("object","any")) && substr($value, 0, 1)=="%") return false;
		if(!empty($this->above ) && $value <= $this->above) return false;
		if(!empty($this->below ) && $value >= $this->below) return false;
		if(!empty($this->intype) && (gettype($value)!="object" || in_array(get_class($value), $this->intype))) return false;
		if(!empty($this->inset ) && !in_array($value, $this->inset )) return false;
		if(!empty($this->outset) &&  in_array($value, $this->outset)) return false;
		if(!empty($this->inranges)){
			foreach($this->inranges as $inrange){
				if($value < $inrange[0] || $value>$inrange[1]) return false;
			}
		}
		if(!empty($this->outranges)){
			foreach($this->outranges as $outrange){
				if($value >= $outrange[0] && $value<=$outrange[1]) return false;
			}
		}
		if(!empty($this->custom) && is_callable($this->custom) && !call_user_func($this->custom, $value)) return false;
		return true;
	}
}






// SuperContainerType
// ============================================
class SuperContainerType extends ContainerType {
    /* A container type that can hold other objects that need to be of a certain type. */

	// members
	// ---------------------------------------- 
	protected $keyname = NULL;
	public    $cache   = array();
	protected $keys    = array();

	// __construct
	// ---------------------------------------- 
	public function __construct($master, $name, $dtype, $default, $custom=NULL, $cache=array(), $above=NULL, $below=NULL, $intype=array(), $inset=array(), $outset=array(), $inranges=array(), $outranges=array(), $keyname="key") {
		/* Constructor */
		parent::__construct($master, $name, $dtype, $default, $above, $below, $intype, $inset, $outset, $inranges, $outranges, $custom);
		$this->keyname = $keyname;
		$this->cache   = $cache;
		$this->keys    = array();
		foreach($this->cache as $obj) array_push($this->keys, $obj->{$this->keyname});
	}

	// has
	// ---------------------------------------- 
	public function has($key){
		/* Returns true if a template object with name $key exists in 
		the cached list of allowed objects, otherwise false */
		return in_array($key, $this->keys);
	}

	// get
	// ---------------------------------------- 
	public function get($key){
		/* Returns the pointer to the template object if stored under in 
		the cache with name $key, else NULL */
		if(!$this->has($key)) return NULL;
		return $this->cache[array_search($key, $this->keys)];
	}

}




// Container
// ============================================
class Container {
	/* A buffer optimized for storing a value in connection
	* with a name and in accordance with the validity conditions 
	* (data type, range, ...) in the ContainerType */

	// traits
	// ---------------------------------------- 
	//use Box;


	// START: copy-paste from Box (5.3 compatibility)
	// ========================================
	// ========================================

	// members
	// ---------------------------------------- 
	public    $master   = NULL;
	public    $name     = NULL;
	public    $isValid  = true;
	protected $subelms  = array();
	public    $parent   = NULL;

	// construct
	// ---------------------------------------- 
	public function constructBox($master, $name){
		/* Constructor */
		$this->master  = $master;
		$this->name    = $master->checkION($name) ? $name : $master->makeION($name);
		$this->master->registerION($this->name);
		$this->isValid = true;
		$this->subelms = array();
		$this->parent  = NULL;
	}

	// addChild
	// ---------------------------------------- 
	public function addChild($item){
		/* Adds another object as child to this entity */
		array_push($this->subelms, $item);
		$item->parent = $this;
	}

	// children
	// ---------------------------------------- 
	public function children($direct=true){
		/* Returns the list of children of this entity */
		if($direct) return $this->subelms;
		$result = array();
		foreach($this->subelms as $item){
			array_push($result, $item);
			$result = array_merge($result, $item->children(false));
		}
		return $result;
	}

	// delChild
	// ---------------------------------------- 
	public function delChild($item){
		/* Removes another object as child to this entity */
		if(!in_array($item, $this->subelms)) return;
		unset($this->subelms[array_search($item, $this->subelms)]);
	}

	// hasChild
	// ---------------------------------------- 
	public function hasChild($item){
		/* Removes another object as child to this entity */
		if(!in_array($item, $this->subelms)) return false;
		return true;
	}

	// parents
	// ---------------------------------------- 
	public function parents(){
		/* Returns the list of parents of this entity */
		if(empty($this->parent)) return array();
		$result = array();
		array_push($result, $this->parent);
		$item = $this->parent;
		while(true){
			if(empty($item->parent)) return $result;
			array_push($result, $item->parent);
			$item = $item->parent;
		}
		return $result;
	}

	// END: copy-paste from Box (5.3 compatibility)
	// ========================================
	// ========================================



	// members
	// ---------------------------------------- 
	public    $key       = NULL;
	public    $type      = NULL;
	public    $value     = NULL;
	public    $isGood    = false;
	protected $valueLast = NULL;
	protected $valueOrig = NULL;

	// __construct
	// ---------------------------------------- 
	public function __construct($master, $name, $key, $value, $ctype){
		/* Constructor */
		$this->constructBox($master, $name);
		$this->key       = $key;
		$this->type      = $ctype;
		$this->isGood    = false;
		$this->value     = NULL;
		$this->valueLast = NULL;
		$use = !empty($this->type) ? $this->type->default : NULL;
		$use = ($value!="default" && !empty($value)) ? $value : $use;
		$this->valueOrig = convertTo($this->master, $use, $this->type->dtype);
		$this->setValue($use, true);
	}

	// __get
	// ---------------------------------------- 
	public function __get($key) {
		/* Returns the value of the property $key of the container */
		if($key=="value") return $this->value;
		return NULL;
	}

	// __invoke
	// ---------------------------------------- 
	public function __invoke() {
		/* Returns the value of the container upon calling */
		return $this->value;
	}

	// __set
	// ---------------------------------------- 
	public function __set($key, $value) {
		/* Sets a new value to the property $key of the container */
		if($key=="value") $this->setValue($value);
	}

	// __toString
	// ---------------------------------------- 
	public function __toString() {
		/* Returns the value as string */
		return get_class($this)."(".$this->name.": ".$this->key."; ".convertTo($this->master, $this->value, "string").")";
	}


	// last
	// ---------------------------------------- 
	public function last() {
		/* Resets the value to the last valid one */
		$this->setValue($this->valueLast);
	}

	// original
	// ---------------------------------------- 
	public function original() {
		/* Resets the value to the original one at construction */
		$this->setValue($this->valueOrig);
	}

	// preset
	// ---------------------------------------- 
	public function preset() {
		/* Resets the value to the default one from the ContainerType */
		$this->setValue($this->type->default);
	}

	// setValue
	// ---------------------------------------- 
	public function setValue($value, $init=false) {
		/* Stores a new value in the internal buffer */
		if(empty($value) && !$init) return;
		if(is_string($value)){
			$value = trim($value);
			if(strtolower($value)=="default") $value = $this->type->default;
			if(substr($value, 0, 1)=="%") $value = $this->master->getObj(substr($value, 1));
		}
		$conv        = convertTo($this->master, $value, $this->type->dtype);
		$this->value = $conv;
		if(!$this->type->verify($conv, $value)){ // could also use the real raw value
			$this->isGood = false;
			$this->validate();
			return;
		}
		$this->valueLast = $conv;
		$this->isGood    = true;
		$this->validate();
	}

	// validate
	// ---------------------------------------- 
	protected function validate() {
		/* Sets the isValid flag to true if all is good for the object */
		$this->isValid = true;
		if(empty($this->type )) $this->isValid = false;
		//if(empty($this->value)) $this->isValid = false; // already in isGood
		if(!$this->isGood     ) $this->isValid = false;
	}
}




// SuperContainer
// ============================================
class SuperContainer extends Container implements Iterator {
    /* An enhanced container that can hold other objects that need to be of a certain type. */

	// members and traits
	// ---------------------------------------- 
	//use CachePure;


	// START: copy-paste from CachePure (5.3 compatibility)
	// ========================================
	// ========================================

	// members
	// ---------------------------------------- 
	public    $cache        = array();
	protected $iter         = 0;
	protected $keyname      = "name";
	public    $hierarchical = false;
	protected $virtualList  = array();

	// construct
	// ---------------------------------------- 
	public function constructCachePure($keyname = "name"){
		/* Constructor */
		$this->cache        = array();
		$this->iter         = 0;
		$this->hierarchical = false;
		$this->virtualList  = array();
		$this->keyname      = $keyname;
	}

	// add
	// ---------------------------------------- 
	public function add($key, $value) {
		/* Updates the object with name $key; if the object already
		* exists, it is ignored */
		if($this->has($key)) return;
		$this->cache[$key] = $value;
	}

	// count
	// ---------------------------------------- 
	public function count() {
		/* Return the number of stored elements */
		return count($this->cache);
	}

	// del
	// ---------------------------------------- 
	public function del($key) {
		/* Removes the object with name $key */
		unset($this->cache[$key]);
	}

	// find
	// ---------------------------------------- 
	public function find($value) {
		/* Checks if the cache holds an object $value */
		return in_array($value, $this->cache);
	}

	// free
	// ---------------------------------------- 
	public function free() {
		/* Clears the entire memory of the cache and destroys
		* all objects in doing so */
		$this->cache = array();
	}

	// get
	// ---------------------------------------- 
	public function get($key) {
		/* Returns the object with name $key if it exists */
		if(!$this->has($key)) return NULL;
		return $this->cache[$key];
	}

	// has
	// ---------------------------------------- 
	public function has($key) {
		/* Checks if the cache holds an object with name $key */
		return array_key_exists($key, $this->cache);
	}

	// rebase
	// ---------------------------------------- 
	public function rebase() {
		/* Reassign key to value for all items in the cache; if key of the cache
		* is not the same as the value stored within the object, the key is updated,
		* unless it is already assigned to another object */
		if(count($this->cache)==0) return;
		foreach($this->cache as $key=>$obj){
			if($key==$obj->{$this->keyname}) continue;
			if(!array_key_exists($this->cache, $obj->{$this->keyname}))
				$this->cache[$obj->{$this->keyname}] = $obj; // new ref
			$this->del($key); // remove old ref
		}
	}

	// retrieve
	// ---------------------------------------- 
	public function retrieve($value) {
		/* Returns the key to the object $value if it is held by 
		* the cache, otherwise NULL */
		if(!$this->find($value)) return NULL;
		return array_search($value, $this->cache);
	}

	// set
	// ---------------------------------------- 
	public function set($key, $value) {
		/* Updates the object with name $key */
		$this->cache[$key] = $value;
	}

	// current
	// ---------------------------------------- 
	public function current() {
		/* Iteration function, returns pointer to current object */
		if($this->hierarchical) {
			$keys = array_keys($this->virtualList);
			return $this->virtualList[$keys[$this->iter]];
		}
		$keys = array_keys($this->cache);
		return $this->cache[$keys[$this->iter]];
	}
	
	// key
	// ---------------------------------------- 
	public function key() {
		/* Iteration function, returns key to current object */
		if($this->hierarchical) {
			$keys = array_keys($this->virtualList);
			return $keys[$this->iter];
		}
		$keys = array_keys($this->cache);
		return $keys[$this->iter];
	}
	
	// next
	// ---------------------------------------- 
	public function next() {
		/* Iteration function, iterates through objects */
		++$this->iter;
	}

	// rewind
	// ---------------------------------------- 
	public function rewind() {
		/* Iteration function, sets pointer back to first object */
		$this->iter = 0;
		$this->buildVirtualList();
	}
	
	// valid
	// ---------------------------------------- 
	public function valid() {
		/* Iteration function, checks if current pointer is valid */
		return $this->iter < count($this->cache);
	}

	// buildVirtualList
	// ---------------------------------------- 
	protected function buildVirtualList(){
		/* Builds the virtual list for iteration accounting for the
		* hierarchical parent-child structure */
		$this->virtualList = array();
		foreach($this->getParents() as $parent){
			array_push($this->virtualList, $parent);
			$this->virtualList = array_merge($this->virtualList, $parent->children(false));
		}
	}

	// getChildren
	// ---------------------------------------- 
	public function getChildren($parentname, $direct=true){
		/* Returns the list of children for a given parent */
		if(!$this->has($parentname)) return array();
		$parent = $this->get($parentname);
		return $parent->children($direct);
	}

	// getParent
	// ---------------------------------------- 
	public function getParent($childname=NULL){
		/* Returns the direct parent for a given child */
		if(!$this->has($childname)) return array();
		$child = $this->get($childname);
		return $child->parent;
	}

	// getParents
	// ---------------------------------------- 
	public function getParents($childname=NULL){
		/* Returns the list of parents for a given child */
		if(empty($childname)){
			$result = array();
			foreach($this->cache as $item){
				if(!empty($item->parent)) continue;
				array_push($result, $item);
			}
			return $result;
		}
		if(!$this->has($childname)) return array();
		$child = $this->get($childname);
		return $child->parents();
	}

	// sub
	// ---------------------------------------- 
	public function sub($key, $value, $parent){
		/* Adds a new item as child to a parent (subordinate) */
		if(!$this->has($parent->{$this->keyname})) $this->set($parent->{$this->keyname}, $parent);
		$this->set($key, $value);
		$parent->addChild($value);
	}

	// super
	// ---------------------------------------- 
	public function super($key, $value, $child){
		/* Adds a new item as parent to a child (superordinate) */
		if(!$this->has($child->{$this->keyname})) $this->set($child->{$this->keyname}, $child);
		$this->set($key, $value);
		$value->addChild($child);
	}

	// complement
	// ---------------------------------------- 
	public function complement($other) {
		/* Returns the list of items that are in the other cache
		* but not in this one. Note, the items are not cloned, rather
		* their references are returned. */
		$result = array();
		if(empty($other) || !is_object($other) || !property_exists($other, "cache")) return $result;
		foreach($other->cache as $entry){
			if($this->has($entry->{$this->keyname})) continue;
			$result[$entry->{$this->keyname}] = $entry;
		}
		return $result;
	}

	// duplicate
	// ---------------------------------------- 
	public function duplicate($newname=NULL) {
		/* Returns a new instance of the cache with all items cloned
		* as well */
		$new = new Cache($this->master, !empty($newname) ? $newname : $this->master->cloneION($this->name));
		foreach($this->cache as $key=>$obj)
			$new->set($key, $obj->duplicate());
		return $new;
	}

	// intersection
	// ---------------------------------------- 
	public function intersection($other) {
		/* Returns the list of items that are both in the other cache
		* and also in this one. Note, the items are not cloned, rather
		* their references are returned. */
		$result = array();
		if(empty($other) || !is_object($other) || !property_exists($other, "cache")) return $result;
		foreach($other->cache as $entry){
			if(!$this->has($entry->{$this->keyname})) continue;
			$result[$entry->{$this->keyname}] = $entry;
		}
		return $result;
	}

	// isNull
	// ---------------------------------------- 
	public function isNull(){
		/* Checks if the cache contains zero data */
		return (count($this->cache)==0);
	}

	// merge
	// ---------------------------------------- 
	public function merge($other) {
		/* Adds the items from another cache into this one. If 
		* the key is already assigned to an item, it is overwritten. */
		if(empty($other) || !is_object($other) || !property_exists($other, "cache")) return;
		foreach($other->cache as $key=>$entry)
			$this->set($key, $entry->duplicate());
	}

	// sum
	// ---------------------------------------- 
	public function sum($other) {
		/* Adds the items from another cache into this one. If 
		* the key is already assigned to an item, the entry is
		* ignored */
		if(empty($other) || !is_object($other) || !property_exists($other, "cache")) return;
		foreach($other->cache as $entry){
			if($this->has($entry->{$this->keyname})) continue;
			$this->set($entry->{$this->keyname}, $entry->duplicate());
		}
	}

	// union
	// ---------------------------------------- 
	public function union($other) {
		/* Returns the array of items that are in this or
		* the other cache; in conflicting cases, the items of
		* this cache are preferred; Note that the entries are
		* not copied, rather the references to them are returned. */ 
		$result = array();
		if(empty($other) || !is_object($other) || !property_exists($other, "cache")) return $result;
		foreach($other->cache as $entry) $result[$entry->{$this->keyname}] = $entry;
		foreach($this ->cache as $entry) $result[$entry->{$this->keyname}] = $entry;
		return $result;
	}

	// END: copy-paste from CachePure (5.3 compatibility)
	// ========================================
	// ========================================


	// __construct
	// ---------------------------------------- 
	public function __construct($master, $name, $key, $value, $ctype){
		/* Constructor */
		Container::__construct($master, $name, $key, $value, $ctype);
		$this->constructCachePure("key");
	}

	// __get
	// ---------------------------------------- 
	public function __get($key) {
		/* Returns the item with name $key if it exists */
		if($key=="value") return $this->value;
		return $this->get($key);
	}

	// __isset
	// ---------------------------------------- 
	public function __isset($key) {
		/* Checks if the cache holds an item with name $key */
		if($key=="value") return true;
		return $this->has($key);
	}

	// __set
	// ---------------------------------------- 
	public function __set($key, $value) {
		/* Updates the item ith name $key */
		if($key=="value") { $this->setValue($value); return; }
		$this->set($key, $value);
	}

	// __unset
	// ---------------------------------------- 
	public function __unset($key) {
		/* Removes the object with name $key */
		if($key=="value") return;
		$this->del($key);
	}
}



?>
