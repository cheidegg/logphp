<?php

require_once "functions.php";


// logic in template naming
// templates/<mode>/<pagename>[_<feature>].tpl
// * all templates of pages associated to xyz theme are in directory templates/xyz/ (e.g. "log")
// * exceptions: templates that are general to all modes are in templates/_all/
// * first part of the template name is always the page name
// * the big template (replacing $mainContent) when calling the page in default (probably $GET mode) is called as the page, <pagename>.tpl
// * any subfeatures of the page, forms that are placed inside this big template have an underscore and feature name


// replaceVars
// --------------------------------------------
function replaceVars($raw, $vars) {
	/* Function that replaces the variables (e.g. $variable) in the
	* raw HTML code $raw by what is given in $vars array; here, the key
	* is the variable name (without dollar symbol) and the value is the
	* string that is to be replaced; the function extracts all placeholders 
	* first and then replaces them, i.e. if no replacement is given, 
	* it will write naught. */
	$allvars = array();
	preg_match_all('/([$]\S\w*\b)+/', $raw, $allvars); 
	$thevars = array_unique($allvars[0]);
	$new = $raw;
	$seeking = array();
	$replace = array();
	foreach($thevars as $var) {
		$key = str_replace("\$","",$var);
		array_push($seeking, '/\\'.$var.'\\b/');
		array_push($replace, array_key_exists($key, $vars) ? $vars[$key] : "");
	}
	for($i=0;$i<count($seeking);++$i)
		$new = preg_replace($seeking[$i], $replace[$i], $new);
	return $new;
}



// HtmlHandler
// ============================================
class HtmlHandler {
	/* A manager for HTML templates; has an internal
	* buffer for maintaining placeholders until they
	* are applied to the template */

	private $master    = NULL;
	private $namespace = NULL;
	private $cache     = array();
	public  $messages  = array();
	public  $debug     = array();

	// __construct
	// ---------------------------------------- 
	public function __construct($master, $namespace) {
		/* Constructor */
		$this->master            = $master;
		$this->namespace         = $namespace;
		$this->cache             = array();
		$this->cache["all"     ] = array();
		$this->cache[$namespace] = array();
		$this->master->vb->redirect(array(0,1,2,3), $this, "messages");
		$this->master->vb->redirect(array(4      ), $this, "debug"   );
	}




	// caching variables
	// ========================================

	// add
	// ---------------------------------------- 
	public function add($key, $val, $nsp=NULL) {
		/* Set a replacement $key => $val to the internal buffer
		* under the given namespace. If the key is already used,
		* the new rule is ignored. */
		if(empty($nsp)) $nsp = $this->namespace;
		if(!array_key_exists($nsp, $this->cache)) $this->cache[$nsp] = array();
		if(array_key_exists($key, $this->cache[$nsp])) return;
		$this->cache[$nsp][$key] = $val;
	}

	// del
	// ---------------------------------------- 
	public function del($key, $nsp=NULL) {
		/* Removes a replacement $key => $val from the internal buffer
		* under the given namespace. */
		if(empty($nsp)) $nsp = $this->namespace;
		if(array_key_exists($key, $this->cache[$nsp])) return;
		unset($this->cache[$nsp][$key]);
	}

	// has
	// ---------------------------------------- 
	public function has($key, $nsp=NULL) {
		/* Checks if a replacement $key => $val is available in the internal
		* buffer under the given namespace. */
		if(empty($nsp)) $nsp = $this->namespace;
		if(array_key_exists($key, $this->cache[$nsp])) return true;
		return false;
	}

	// reset
	// ---------------------------------------- 
	public function reset($nsp=NULL) {
		/* Clears the entire internal buffer for either a single
		* or all namespaces. */
		if(!empty($nsp)) {
			unset($this->cache[$nsp]);
			$this->cache[$nsp] = array();
			return;
		}
		unset($this->cache);
		$this->cache                   = array();
		$this->cache["all"           ] = array();
		$this->cache[$this->namespace] = array();
	}

	// set
	// ---------------------------------------- 
	public function set($key, $val, $nsp=NULL) {
		/* Set a replacement $key => $val to the internal buffer
		* under the given namespace. If the key is already used,
		* the value will be replaced. */
		if(empty($nsp)) $nsp = $this->namespace;
		if(!array_key_exists($nsp, $this->cache)) $this->cache[$nsp] = array();
		$this->cache[$nsp][$key] = $val;
	}

	// setVars
	// ---------------------------------------- 
	public function setVars($variables, $nsp=NULL) {
		/* Sets multiple replacements to the internal buffer 
		* under a given namespace. If the key is already used, the
		* value will be replaced. */
		foreach($variables as $key=>$val)
			$this->set($key, $val, $nsp);
	}

	// setVarsDb
	// ---------------------------------------- 
	public function setVarsDb($row, $nsp=NULL) {
		/* Sets multiple replacements to the internal buffer 
		* under a given namespace; The replacement rules come 
		* from the variables stored in the DbRow (or DbView, last row)
		* instance */
		if(empty($row) || !is_object($row)) return;
		if(!in_array(get_class($row), array("DbRow","DbView","DbTable"))) return;
		foreach($row->columns() as $col)
			$this->set($col, $row->get($col)->value, $nsp);
	}

	// template
	// ---------------------------------------- 
	public function template($name, $variables=array(), $row=NULL, $folder=NULL, $nsp=NULL) {
		/* Returns a template with cleared placeholders; placeholders are
		* cleared using the buffered variables and the variables given 
		* to the method */
		if(!empty($variables)) $this->setVars  ($variables, $nsp);
		if(!empty($row      )) $this->setVarsDb($row      , $nsp);
		if( empty($nsp      )) $nsp    = $this->namespace;
		if( empty($folder   )) $folder = $nsp;
		$path = $this->master->config->get("base")->value."/templates/".$folder."/".$name.".tpl";
		if(!file_exists($path)) return "";
	    $raw   = file_get_contents($path);
		$repls = array_key_exists($nsp, $this->cache) ? $this->cache[$nsp] : array();
		$repls["messages"] = implode("<br />", $this->messages);
		$repls["debug"   ] = implode("<br />", $this->debug   );
		foreach($this->cache["all"] as $key=>$val){
			if(array_key_exists($key, $repls)) continue; // priority to this namespace
			$repls[$key] = $val;
		}
		foreach($repls as $key=>$val){
			if(is_int($val) && $val==0) $repls[$key]="";
		}
		return replaceVars($raw, $repls);
	}




	// HTML building block -- the menu
	// ========================================


	// makeMenuNew
	// ---------------------------------------- 
	public function makeMenuNew(){
		/* Builds the drop-down menu over all pages from the total
		* configuration */

		// get menu entries
		$entries0 = $this->master->config->getByType("menu", true);
		
		// build list with levels and filtered by permissions and setting side from top levels
		$entries = array();
		$i       = 0;
		foreach($entries0 as $entry){
			$topLevel = ($entry->parent == NULL);
			if($i==0 && !$topLevel) return array("", ""); // this is not possible! first entry must be top level!

			$id = !empty($entry->get("pageId")->value) ? $entry->get("pageId")->value : $entry->key;
			if(!$this->master->getPermission($id)) continue;

			$pLevel = 0;
			if(!$topLevel){
				foreach($entries as $other){
					if($other[0]->name != $entry->parent->name) continue;
					$pLevel = $other[1];
					$entry->setOpt("side", $other[0]->get("side")->value);
				}
			}

			if(!$topLevel && $pLevel<0) continue; // this can be if the parent does not have permissions => don't accept child either

			array_push($entries, array($entry, $topLevel ? 1 : $pLevel+1));
			++$i;	
		}

		// build html for both sides
		$left  = array();
		$right = array();
		foreach($entries as $line){

			$entry = $line[0];
			$level = $line[1];
			$id    = !empty($entry->get("pageId")->value) ? $entry->get("pageId")->value : $entry->key;

			// href and onclick
			$setvar = $entry->get("setvar")->value;
			$choice = $entry->get("choice")->value;
			$add    = (!empty($setvar) && !empty($choice)) ? sprintf("setName('dummy','%s');set('%s', '%s');set('do', '%s');", $setvar, $setvar, $choice, $setvar) : "";
			$path   = sprintf("href=\"#\" onclick=\"%sget('%s')\"", $add, $id);

			// external url
			if(!empty($entry->get("url")->value)) {
				$path = $entry->get("url")->value;
				if(strpos($path, "?")!==false) $path.="&sessId=".$this->master->globals["sessId"];
				else $path.="?sessId=".$this->master->globals["sessId"];
				$path = sprintf("href=\"%s\"", $path);
			}

			// build html
			$element = sprintf("<a %s%s>%s</a>", $path, (($this->master->globals["pageId"]==$id) ? " class=\"headerNaviLinkActive\"" : ""), $entry->value);
			if($entry->has("side") && $entry->get("side")->value=="left"){
				array_push($left , array($element, $level));
			}
			else
				array_push($right, array($element, $level));
		}

		//return array($this->makeUlTree("menuLeft", $left, true), "");
		return array($this->makeUlTree("headerNaviLeftUl", $left, true), $this->makeUlTree("headerNaviRightUl", $right, true));
	}

	// makeMenu
	// ---------------------------------------- 
	public function makeMenu(){
		/* Builds the drop-down menu over all pages from the total
		* configuration */
		
		$left    = array();
		$right   = array();
		$entries = $this->master->config->getByType("menu");
		foreach($entries as $entry){
			if($entry->parent != NULL) continue;

			$id = !empty($entry->get("pageId")->value) ? $entry->get("pageId")->value : $entry->key;
			if(!$this->master->getPermission($id)) continue;

			// href and onclick
			$setvar = $entry->get("setvar")->value;
			$choice = $entry->get("choice")->value;
			$add    = (!empty($setvar) && !empty($choice)) ? sprintf("set('%s', '%s')", $setvar, $choice) : "";
			$path   = sprintf("href=\"#\" onclick=\"%sget('%s')\"", $add, $id);

			// external url
			if(!empty($entry->get("url")->value)) {
				$path = $entry->get("url")->value;
				if(strpos($path, "?")!==false) $path.="&sessId=".$this->master->globals["sessId"];
				else $path.="?sessId=".$this->master->globals["sessId"];
				$path = sprintf("href=\"%s\"", $path);
			}

			$element = sprintf("<li><a %s%s>%s</a></li>", $path, (($this->master->globals["pageId"]==$id) ? " class=\"header_a_active\"" : ""), $entry->value);
			if($entry->has("side") && $entry->get("side")->value=="left")
				array_push($left , $element);
			else
				array_push($right, $element);
		}	
		return array(implode("", $left), implode("", $right));
	}

	// makeOptions
	// ---------------------------------------- 
	public function makeOptions($values, $selected=NULL, $disabled=array(), $addOpts=array()){
		/* Building multiple <option> elements for the entries in a list; Format:
		* (1) $values either [..., key=>value, ...] (then the $key is the value in the option)
                      or     [..., value     , ...] (then the option value is the index in the array)
		* (2) $selected either NULL (nothing) or =key (or index) of the value
		* (3) $disabled = [..., key, ...] the list of options (via their key) to be disabled; use "all" to disable all options
		* (4) $addOpts  = [..., key=>add, ...] additional stuff per option (select option via key); use "all" as key to add it to all options */
		if(count($values)==0) return "";
		$result = array();
		$i      = 0;
		foreach($values as $key=>$value){
			$sel = $selected===$key                                                      ? "selected"     : "";
			$dis = (in_array($key, $disabled, true) || in_array("all", $disabled, true)) ? "disabled"     : "";
			$add = array_key_exists($key, $addOpts)                                      ? $addOpts[$key] : "";
			if(array_key_exists("all", $addOpts)) $add .= " ".$addOpts["all"];
			array_push($result, sprintf("<option value=\"%s\" %s %s %s>%s</option>", strval($key), $sel, $dis, $add, $value));
			++$i;
		}
		return implode("", $result);
	}	

	// makeSelect
	// ---------------------------------------- 
	public function makeSelect($name, $options, $selected=NULL, $disOpts=array(), $addOpts=array(), $disabled=false, $onchange="", $add="", $addOff="", $addOn="", $notOn=array()){
		/* Building the full <select> element including its options; Format:
		* (1) The name (variable name _and_ ID of the <select> entity) of the select
		* (2)-(4) as (1)-(4) in HtmlHandler::makeOptions
		* (5) true if you want to disable the entire <select> entity
		* (6) additional stuff to be added to the <select ...> statement 
		* (7) same as (6) but only if nothing is selected
		* (8) same as (6) but only if an option is selected */

		//"class=\"invisiselectmenu general\"";
		$dis   = $disabled ? "disabled" : "";
		$onoff = array_key_exists($selected, $options) && !array_key_exists($selected, $notOn) ? $addOn : $addOff;
		$html  = sprintf("<select id=\"%s\" name=\"%s\" onchange=\"changecolor('%s');%s\" %s %s %s>", $name, $name, $name, $onchange, $dis, $add, $onoff);
		$html .= $this->makeOptions($options, $selected, $disOpts, $addOpts);
		$html .= "</select>";
		return $html;
	}


	// makeTablerows
	// ---------------------------------------- 
	public function makeTablerows($view, $format, $slim=false, $rowformat=NULL){
		/* Building the <tr> and <td> elements from a DbView according to
		* a formatting prescription encoded in the $format function */
		$rows = array();
		$i    = 0;
		$collection = $slim ? $view->slim : $view->rows;
		foreach($collection as $row){
			$add = $rowformat!=NULL ? $rowformat($row) : "";
			array_push($rows, sprintf("<tr %s>\n", $add));
			foreach($row as $k=>$field){
				if($slim && $k=="_idx") continue;
				if($slim) $content = $format($field, $row, $k);
				else      $content = $format($field, $row);
				if($content===false) continue;
				$rows[$i] .= "<td>".$content."</td>\n";
			}
			$rows[$i] .= "</tr>\n";
			++$i;
		}
		return implode("", $rows);
	}

	// makeUlTree
	// ---------------------------------------- 
	public function makeUlTree($name, $entries, $makeNav=false){
		/* Builds an unordered list with top <ul> element having
		* id and name $name. Entries can be arrays with level (starting
		* from 0) as second element in order to get a tree structure. */
		if(count($entries)==0) return "";
		$html = "";
		if($makeNav) $html.="<nav class=\"headerNaviNav\">";
		$html .= "<ul id=\"".$name."\">";
		if(!is_array($entries[0])){
			$newlist = array();
			foreach($entries as $entry) array_push($newlist, array($entry, 0));
			$entries = $newlist;
		}
		$former = 1;
		$i      = 0;
		foreach($entries as $entry){
			$content = $entry[0];
			$level   = $entry[1];
			$diff    = $former - $level;
			if($diff==0 && $i>0) 
				$html.="</li>";
			else if($former>0 && $diff>0){
				for($i=1;$i<=$diff;++$i) 
					$html.="</li></ul>";
			}
			else if($i>0)
				$html .= "<ul>";
			$html  .= "<li>".$content;
			$former = $level;
			++$i;
		}
		for($i=1;$i<=$former;++$i) 
			$html.="</li></ul>";
		$html.="</ul>";
		if($makeNav) $html.="</nav>";
		return $html;		
	}


}

?>
