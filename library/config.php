<?php

require_once "container.php";
//require_once "cache.php";


// getParent
// --------------------------------------------
function getParent(&$last, $indent){
	/* Extracts the indentation and thus the parent of the entry 
	* (which is the most recent one with a smaller indentation);
	* uses the list of last entries for given indentations
	* $last = array(<indentation> => <ref to entry>, ...)
	* Logic:
	* if $last is empty and indent is 0: no parent
	* if $last is empty and indent is not 0 (first entry): format error, treat as indent = 0
	* if $last is empty and indent is not 0 (other entries): parent = last with indent 0
	* if $last is not empty: take parent from lower but highest indentation, remove all other entries
	* e.g. $last = [0=>$elmA, 3 => $elmB, 7 => $elmC) and indent = 3
	* it means, the parent of the current entry has indent 0, thus is $elmA
	* furthermore, need to replace $elmB by the current one
	* also, need to remove everything with indent > current indent (i.e. "7 => $elmC")
	* this is because if the next entry has indent 7 again, the current entry will be parent
	* to that entry (a new branch is opened in that case, while if we go lower in indentations
	* we are closing the branch again);
	* The function returns the ref to the parent and updates $last */
	if($indent==0) return NULL;
	$parent = NULL;
	$new    = array(); 
	$rem    = NULL;
	foreach($last as $ind=>$elm){
		if($ind < $indent){
			$new[$ind] = $elm;
			$rem       = $ind; 
			continue; 
		}
		break;
	}
	$parent = $last[$rem];
	$last   = $new;
	return $parent;
}







// ConfigOptionTpl
// ============================================
class ConfigOptionTpl extends Container {
	/* Same as ConfigOption but passing master reference directly;
	* needed because object is built at production */

	// members
	// ---------------------------------------- 
	public $config = NULL;
	public $entry  = NULL;

	// __construct
	// ---------------------------------------- 
	public function __construct($master, $name, $key, $value, $ctype) {
		/* Constructor */
		parent::__construct($master, $name, $key, $value, $ctype);
		$this->entry  = NULL;
		$this->config = NULL;
	}

	// __invoke
	// ---------------------------------------- 
	public function __invoke() {
		/* Returns the value */
		return $this->value;
	}

	// derive
	// ---------------------------------------- 
	public function derive($entry) {
		/* Returns a copy of this tpl as ConfigOption for a new entry */
		$new = new ConfigOption($entry, $entry->name."_".$this->name, $this->key, $this->value, $this->type);
		return $new;
	}
}




// ConfigOption
// ============================================
class ConfigOption extends Container {
	/* An option to an entry in a configuration; essentially
	* an assignment of a value to a name, hence, the ConfigOption
	* inherits from the Container class */

	// members
	// ---------------------------------------- 
	public $config = NULL;
	public $entry  = NULL;

	// __construct
	// ---------------------------------------- 
	public function __construct($entry, $name, $key, $value, $ctype) {
		/* Constructor */
		parent::__construct($entry->master, $name, $key, $value, $ctype);
		$this->entry  = $entry;
		$this->config = $entry->config;
	}

	// __invoke
	// ---------------------------------------- 
	public function __invoke() {
		/* Returns the value */
		return $this->value;
	}

	// __toString
	// ---------------------------------------- 
	public function __toString() {
		/* Returns the value as string */
		return strval($this->value);
	}

	// duplicate
	// ---------------------------------------- 
	public function duplicate($entry = NULL) {
		/* Returns a copy of this ConfigOption for a new entry */
		$use = !empty($entry) ? $entry : $this->entry;
		$new = new ConfigOption($use, $this->master->cloneION($this->name, $use), $this->key, $this->value, $this->type);
		return $new;
	}
}




// ConfigEntry
// ============================================
class ConfigEntry extends SuperContainer {
	/* Implementation of an entry in a formatted configuration */
	
	// members
	// ---------------------------------------- 
	public    $config   = NULL;
	public    $full     = false;
	protected $fullList = array();

	// __construct
	// ---------------------------------------- 
	public function __construct($config, $name, $key, $value, $ctype, $repr=NULL, $opts=NULL) {
		/* Constructor */
		$this->full = false;
		if(!empty($repr)){
			$this->master = $config->master; // otherwise ConfigEntry::parse does not work
			$elms         = $this->parse($repr);
			if(!empty($elms)){
				$ctype = $elms[0];
				$key   = $elms[1];
				$value = $elms[2];
				$opts  = !empty($opts) ? $opts." ".$elms[3] : $elms[3];
			}
		}
		if(empty($name)) $name = "configentry_".$key;
		parent::__construct($config->master, $name, $key, $value, $ctype);
		$this->loadOptionstring($opts);
		$this->config = $config;
	}




	// individual items
	// ========================================

	// get
	// ---------------------------------------- 
	public function get($key) {
		/* Returns the pointer to the option with name $key if it exists;
		* in fact, the ConfigOption entities are searched first,
		* but if the key is not found in there, the ConfigOptionTpl
		* (stored in the cache of the SuperContainerType) are searched */
		if(!array_key_exists($key, $this->cache)){
			if(!$this->type->has($key)) return NULL;
			return $this->type->get($key);
		}
		return $this->cache[$key];
	}

	// has
	// ---------------------------------------- 
	public function has($key) {
		/* Checks if the entry holds an option with name $key;
		* in fact, the ConfigOption entities are searched first,
		* but if the key is not found in there, the ConfigOptionTpl
		* (stored in the cache of the SuperContainerType) are searched */
		if(array_key_exists($key, $this->cache)) return true;
		return false;
		//return $this->type->has($key);
	}




	// item iteration
	// ========================================

	// buildFullList
	// ---------------------------------------- 
	protected function buildFullList(){
		/* Builds the full list for iteration also taking into 
		* account the ConfigOptionTpl objects */
		$this->fullList = array();
		foreach($this->type->cache as $key=>$tpl)
			$this->fullList[$key] = $tpl;
		foreach($this->cache       as $key=>$opt)
			$this->fullList[$key] = $opt;
	}

	// current
	// ---------------------------------------- 
	public function current() {
		/* Iteration function, returns pointer to current object */
		if($this->full) {
			$keys = array_keys($this->fullList);
			return $this->fullList[$keys[$this->iter]];
		}
		$keys = array_keys($this->cache);
		return $this->cache[$keys[$this->iter]];
	}
	
	// key
	// ---------------------------------------- 
	public function key() {
		/* Iteration function, returns key to current object */
		if($this->full) {
			$keys = array_keys($this->fullList);
			return $keys[$this->iter];
		}
		$keys = array_keys($this->cache);
		return $keys[$this->iter];
	}

	// rewind
	// ---------------------------------------- 
	public function rewind() {
		/* Iteration function, sets pointer back to first object */
		$this->iter = 0;
		$this->buildFullList();
	}
	
	// valid
	// ---------------------------------------- 
	public function valid() {
		/* Iteration function, checks if current pointer is valid */
		if($this->full) return $this->iter < $this->countAll();
		return $this->iter < count($this->cache);
	}




	// option access
	// ========================================

	// countAll
	// ---------------------------------------- 
	public function countAll() {
		/* Returns the total number of all options, inlcuding ConfigOptionTpl entities */
		return count($this->type->cache);
	}

	// lastOpt
	// ---------------------------------------- 
	public function lastOpt($key = NULL) {
		/* Sets the value of the option with name $key to its last valid value;
		* if $key is NULL, all options are updated */
		if(empty($key)){
			foreach($this->cache as $option)
				$option->last();
		}
		else if($this->has($key))
			$this->get($key)->last();
	}

	// originalOpt
	// ---------------------------------------- 
	public function originalOpt($key = NULL) {
		/* Sets the value of the option with name $key to its original value;
		* if $key is NULL, all options are updated */
		if(empty($key)){
			foreach($this->cache as $option)
				$option->original();
		}
		else if($this->has($key))
			$this->get($key)->original();
	}

	// presetOpt
	// ---------------------------------------- 
	public function presetOpt($key = NULL) {
		/* Sets the value of the option with name $key to its default;
		* if $key is NULL, all options are updated */
		if(empty($key)){
			foreach($this->cache as $option)
				$option->preset();
		}
		else if($this->has($key))
			$this->get($key)->preset();
	}

	// setOpt
	// ---------------------------------------- 
	public function setOpt($key, $value) {
		/* Sets the value of the option with name $key to a given value; if 
		* the ConfigOption entity does not exist yet, it is derived */
		if(!$this->type->has($key)) return;
		$opt = $this->get($key);
		if(get_class($opt)=="ConfigOptionTpl") {
			$tpl = $this->type->get($key);
			$opt = $tpl->derive($this);
			$this->set($key, $opt);
		}
		$opt->setValue($value);
	}




	// import and export
	// ========================================

	// duplicate
	// ---------------------------------------- 
	public function duplicate($config = NULL) {
		/* Returns a copy of the entry with all its entries copied too. */
		$use = !empty($config) ? $config : $this->config;
		$new = new ConfigEntry($use, $this->master->cloneION($this->name, $use), $this->key, $this->value, $this->type);
		$new->load($this->repr());
		return $new;
	}

	// load
	// ---------------------------------------- 
	public function load($repr) {
		/* Builds the entry from a string representation; note that this function
		* can only be called _after_ the constructor, hence, the name cannot be changed */
		$elements = $this->parse($repr);
		if(empty($elements)) return;
		$this->type = $elements[0];
		$this->key  = $elements[1];
		$this->setValue($elements[2]);
		$this->loadOptionstring($elements[3]);
	}

	// loadOptionstring
	// ---------------------------------------- 
	public function loadOptionstring($optionstring) {
		/* Builds the options from the optionstring */
		if(!ConfigEntry::probeOptionstring($optionstring)) return;
		$elms  = explode(";", $optionstring);
		$items = array_map("trim", $elms);
		foreach($items as $item){
			$elms  = explode(":=", $item);
			$split = array_map("trim", $elms); // key:=value
			if(count($split)!=2) continue;
			$this->setOpt($split[0], $split[1]);
		}
	} 

	// parse
	// ---------------------------------------- 
	public function parse($repr) {
		/* Extracts the information from the string representation */
		if(!ConfigEntry::probe($this->master, $repr)) return NULL;
		$elms  = explode("#", $repr);
		$repr  = trim($elms[0]);
		$elms  = explode("::", $repr);
		$items = array_map("trim", $elms);
		$num   = count($items);
		if($num<2 || $num>4) return NULL;
		if($num==2) return array($this->master->getObj("SuperContainerType"), $items[0], $items[1], "");
		if($num==3) {
			$hasOpts = $this->probeOptionstring($items[2]);
			if(!$hasOpts) return array($this->master->getObj("SuperContainerType", $items[0]), $items[1], $items[2], ""       );
			else          return array($this->master->getObj("SuperContainerType"           ), $items[0], $items[1], $items[2]);
		}
		if($num==4) return array($this->master->getObj("SuperContainerType", $items[0]), $items[1], $items[2], $items[3]);
		return NULL;
	}

	// probe
	// ---------------------------------------- 
	public static function probe($master, $repr=NULL) {
		/* Returns true if the given representation is valid */
		if(empty($repr)     ) return false;
		if(!is_string($repr)) return false;
		$repr = trim($repr);
		if(substr($repr, 0, 1)=="#") return false;
		$elms  = explode("::", $repr);
		$items = array_map("trim", $elms);
		$num   = count($items);
		if($num<2 || $num>4) return false;
		if($num==3) {
			$isOpt = self::probeOptionstring($items[2]);
			if($isOpt) return true;
			return $master->hasObj("SuperContainerType", $items[0]);
		}
		if($num==4) {
			$isOpt = self::probeOptionstring($items[3]);
			if(!$isOpt) return false;
			return $master->hasObj("SuperContainerType", $items[0]);
		}
		return true;
	}

	// probeOptionstring
	// ---------------------------------------- 
	public static function probeOptionstring($optionstring) {
		/* Returns true if the given optionstring is valid */
		if(empty($optionstring)     ) return false;
		if(!is_string($optionstring)) return false;
		$items = explode(";", $optionstring);
		foreach($items as $item){
			$split = explode(":=", $item); // key:=value
			if(count($split)!=2) continue;
			return true; // at least one valid option
		}
		return false;
	} 

	// repr
	// ---------------------------------------- 
	public function repr() {
		/* Build the string representation */
		$repr = $this->type->name."::".$this->key."::".convertTo($this->master, $this->value, "string");
		$opts = array();
		foreach($this->cache as $key=>$option)
			array_push($opts, $key.":=".convertTo($this->master, $option->value, "string"));
		if(count($this->cache)>0) $repr.="::".implode(";", $opts);
		return $repr;
	}
}




// Config
// ============================================
class Config implements Iterator {
	/* Implementation of a formatted configuration, i.e., object
	* that holds certain data in a structured way*/


	// traits
	// ---------------------------------------- 
	//use Cache;

	// members
	// ---------------------------------------- 
	public $key = NULL;

	// __construct
	// ---------------------------------------- 
	public function __construct($master, $key, $repr=NULL, $path=NULL) {
		/* Constructor */
		$this->key = $key;
		$this->constructCache($master, "config_".$key, "key");
		$this->import($path);
		$this->load  ($repr); // preference to string representation
	}

	// __toString
	// ---------------------------------------- 
	public function __toString() {
		/* Returns the string representation */
		return $this->repr();
	}


	// START: copy-paste from Box and Cache and CachePure (5.3 compatibility)
	// ========================================
	// ========================================

	// members
	// ---------------------------------------- 
	public    $master   = NULL;
	public    $name     = NULL;
	public    $isValid  = true;
	protected $subelms  = array();
	public    $parent   = NULL;

	// construct
	// ---------------------------------------- 
	public function constructBox($master, $name){
		/* Constructor */
		$this->master  = $master;
		$this->name    = $master->checkION($name) ? $name : $master->makeION($name);
		$this->master->registerION($this->name);
		$this->isValid = true;
		$this->subelms = array();
		$this->parent  = NULL;
	}

	// validate
	// ---------------------------------------- 
	protected function validate(){
		/* Validates itself and sets the status to true if all
		* is fine; to be overloaded by children */
		$this->isValid = true;
	}

	// addChild
	// ---------------------------------------- 
	public function addChild($item){
		/* Adds another object as child to this entity */
		array_push($this->subelms, $item);
		$item->parent = $this;
	}

	// children
	// ---------------------------------------- 
	public function children($direct=true){
		/* Returns the list of children of this entity */
		if($direct) return $this->subelms;
		$result = array();
		foreach($this->subelms as $item){
			array_push($result, $item);
			$result = array_merge($result, $item->children(false));
		}
		return $result;
	}

	// delChild
	// ---------------------------------------- 
	public function delChild($item){
		/* Removes another object as child to this entity */
		if(!in_array($item, $this->subelms)) return;
		unset($this->subelms[array_search($item, $this->subelms)]);
	}

	// hasChild
	// ---------------------------------------- 
	public function hasChild($item){
		/* Removes another object as child to this entity */
		if(!in_array($item, $this->subelms)) return false;
		return true;
	}

	// parents
	// ---------------------------------------- 
	public function parents(){
		/* Returns the list of parents of this entity */
		if(empty($this->parent)) return array();
		$result = array();
		array_push($result, $this->parent);
		$item = $this->parent;
		while(true){
			if(empty($item->parent)) return $result;
			array_push($result, $item->parent);
			$item = $item->parent;
		}
		return $result;
	}

	// members
	// ---------------------------------------- 
	public    $cache        = array();
	protected $iter         = 0;
	protected $keyname      = "name";
	public    $hierarchical = false;
	protected $virtualList  = array();

	// construct
	// ---------------------------------------- 
	public function constructCachePure($keyname = "name"){
		/* Constructor */
		$this->cache        = array();
		$this->iter         = 0;
		$this->hierarchical = false;
		$this->virtualList  = array();
		$this->keyname      = $keyname;
	}

	// __get
	// ---------------------------------------- 
	public function __get($key) {
		/* Returns the item with name $key if it exists */
		return $this->get($key);
	}

	// __isset
	// ---------------------------------------- 
	public function __isset($key) {
		/* Checks if the cache holds an item with name $key */
		return $this->has($key);
	}

	// __set
	// ---------------------------------------- 
	public function __set($key, $value) {
		/* Updates the item ith name $key */
		$this->set($key, $value);
	}

	// __unset
	// ---------------------------------------- 
	public function __unset($key) {
		/* Removes the object with name $key */
		$this->del($key);
	}

	// add
	// ---------------------------------------- 
	public function add($key, $value) {
		/* Updates the object with name $key; if the object already
		* exists, it is ignored */
		if($this->has($key)) return;
		$this->cache[$key] = $value;
	}

	// count
	// ---------------------------------------- 
	public function count() {
		/* Return the number of stored elements */
		return count($this->cache);
	}

	// del
	// ---------------------------------------- 
	public function del($key) {
		/* Removes the object with name $key */
		unset($this->cache[$key]);
	}

	// find
	// ---------------------------------------- 
	public function find($value) {
		/* Checks if the cache holds an object $value */
		return in_array($value, $this->cache);
	}

	// free
	// ---------------------------------------- 
	public function free() {
		/* Clears the entire memory of the cache and destroys
		* all objects in doing so */
		$this->cache = array();
	}

	// get
	// ---------------------------------------- 
	public function get($key) {
		/* Returns the object with name $key if it exists */
		if(!$this->has($key)) return NULL;
		return $this->cache[$key];
	}

	// has
	// ---------------------------------------- 
	public function has($key) {
		/* Checks if the cache holds an object with name $key */
		return array_key_exists($key, $this->cache);
	}

	// rebase
	// ---------------------------------------- 
	public function rebase() {
		/* Reassign key to value for all items in the cache; if key of the cache
		* is not the same as the value stored within the object, the key is updated,
		* unless it is already assigned to another object */
		if(count($this->cache)==0) return;
		foreach($this->cache as $key=>$obj){
			if($key==$obj->{$this->keyname}) continue;
			if(!array_key_exists($this->cache, $obj->{$this->keyname}))
				$this->cache[$obj->{$this->keyname}] = $obj; // new ref
			$this->del($key); // remove old ref
		}
	}

	// retrieve
	// ---------------------------------------- 
	public function retrieve($value) {
		/* Returns the key to the object $value if it is held by 
		* the cache, otherwise NULL */
		if(!$this->find($value)) return NULL;
		return array_search($value, $this->cache);
	}

	// set
	// ---------------------------------------- 
	public function set($key, $value) {
		/* Updates the object with name $key */
		$this->cache[$key] = $value;
	}

	// current
	// ---------------------------------------- 
	public function current() {
		/* Iteration function, returns pointer to current object */
		if($this->hierarchical) {
			$keys = array_keys($this->virtualList);
			return $this->virtualList[$keys[$this->iter]];
		}
		$keys = array_keys($this->cache);
		return $this->cache[$keys[$this->iter]];
	}
	
	// key
	// ---------------------------------------- 
	public function key() {
		/* Iteration function, returns key to current object */
		if($this->hierarchical) {
			$keys = array_keys($this->virtualList);
			return $keys[$this->iter];
		}
		$keys = array_keys($this->cache);
		return $keys[$this->iter];
	}
	
	// next
	// ---------------------------------------- 
	public function next() {
		/* Iteration function, iterates through objects */
		++$this->iter;
	}

	// rewind
	// ---------------------------------------- 
	public function rewind() {
		/* Iteration function, sets pointer back to first object */
		$this->iter = 0;
		$this->buildVirtualList();
	}
	
	// valid
	// ---------------------------------------- 
	public function valid() {
		/* Iteration function, checks if current pointer is valid */
		return $this->iter < count($this->cache);
	}

	// buildVirtualList
	// ---------------------------------------- 
	protected function buildVirtualList(){
		/* Builds the virtual list for iteration accounting for the
		* hierarchical parent-child structure */
		$this->virtualList = array();
		foreach($this->getParents() as $parent){
			array_push($this->virtualList, $parent);
			$this->virtualList = array_merge($this->virtualList, $parent->children(false));
		}
	}

	// getChildren
	// ---------------------------------------- 
	public function getChildren($parentname, $direct=true){
		/* Returns the list of children for a given parent */
		if(!$this->has($parentname)) return array();
		$parent = $this->get($parentname);
		return $parent->children($direct);
	}

	// getParent
	// ---------------------------------------- 
	public function getParent($childname=NULL){
		/* Returns the direct parent for a given child */
		if(!$this->has($childname)) return array();
		$child = $this->get($childname);
		return $child->parent;
	}

	// getParents
	// ---------------------------------------- 
	public function getParents($childname=NULL){
		/* Returns the list of parents for a given child */
		if(empty($childname)){
			$result = array();
			foreach($this->cache as $item){
				if(!empty($item->parent)) continue;
				array_push($result, $item);
			}
			return $result;
		}
		if(!$this->has($childname)) return array();
		$child = $this->get($childname);
		return $child->parents();
	}

	// sub
	// ---------------------------------------- 
	public function sub($key, $value, $parent){
		/* Adds a new item as child to a parent (subordinate) */
		if(!$this->has($parent->{$this->keyname})) $this->set($parent->{$this->keyname}, $parent);
		$this->set($key, $value);
		$parent->addChild($value);
	}

	// super
	// ---------------------------------------- 
	public function super($key, $value, $child){
		/* Adds a new item as parent to a child (superordinate) */
		if(!$this->has($child->{$this->keyname})) $this->set($child->{$this->keyname}, $child);
		$this->set($key, $value);
		$value->addChild($child);
	}

	// isNull
	// ---------------------------------------- 
	public function isNull(){
		/* Checks if the cache contains zero data */
		return (count($this->cache)==0);
	}

	// construct
	// ---------------------------------------- 
	public function constructCache($master, $name, $keyname="name"){
		/* Constructor */
		$this->constructBox      ($master, $name);
		$this->constructCachePure($keyname);
	}

	// END: copy-paste from Box and Cache and CachePure (5.3 compatibility)
	// ========================================
	// ========================================


	// entry treatment
	// ========================================

	// preset
	// ---------------------------------------- 
	public function preset($key=NULL) {
		/* Sets to its default setup the entry with name $key or all entries if NULL is given */
		if(empty($key)){
			foreach($this->cache as $entry)
				$entry->preset();
		}
		else if($this->has($key))
			$this->get($key)->preset();
	}

	// presetFull
	// ---------------------------------------- 
	public function presetFull($key=NULL) {
		/* Sets to its default value the entry with name $key or all entries if NULL is given */
		if(empty($key)){
			foreach($this->cache as $entry){
				$entry->preset   ();
				$entry->presetOpt();
			}
		}
		else if($this->has($key)){
			$this->get($key)->preset   ();
			$this->get($key)->presetOpt();
		}
	}

	// last
	// ---------------------------------------- 
	public function last($key=NULL) {
		/* Sets to its last valid setup the entry with name $key or all entries if NULL is given */
		if(empty($key)){
			foreach($this->cache as $entry)
				$entry->last();
		}
		else if($this->has($key))
			$this->get($key)->last();
	}

	// lastFull
	// ---------------------------------------- 
	public function lastFull($key=NULL) {
		/* Sets to its last valid value the entry with name $key or all entries if NULL is given */
		if(empty($key)){
			foreach($this->cache as $entry){
				$entry->last   ();
				$entry->lastOpt();
			}
		}
		else if($this->has($key)){
			$this->get($key)->last   ();
			$this->get($key)->lastOpt();
		}
	}

	// original
	// ---------------------------------------- 
	public function original($key=NULL) {
		/* Sets to its original setup the entry with name $key or all entries if NULL is given */
		if(empty($key)){
			foreach($this->cache as $entry)
				$entry->original();
		}
		else if($this->has($key))
			$this->get($key)->original();
	}

	// originalFull
	// ---------------------------------------- 
	public function originalFull($key=NULL) {
		/* Sets to its original value the entry with name $key or all entries if NULL is given */
		if(empty($key)){
			foreach($this->cache as $entry){
				$entry->original   ();
				$entry->originalOpt();
			}
		}
		else if($this->has($key)){
			$this->get($key)->original   ();
			$this->get($key)->originalOpt();
		}
	}




	// import and export
	// ========================================

	// export
	// ---------------------------------------- 
	public function export($path) {
		/* Builds the string representation and exports it to a txt file if a valid path is given */
		$repr = $this->repr();
		if(!is_writable($path)) return; 
		file_put_contents($path, $repr);
	}

	// import
	// ---------------------------------------- 
	public function import($path) {
		/* Extracts the config from a file */
		if(empty($path) || !file_exists($path)) return;
		$repr = file_get_contents($path);
		$this->load($repr);
	}

	// load
	// ---------------------------------------- 
	public function load($repr) {
		/* Builds the config from a string representation */
		if(!Config::probe($this->master, $repr)) return;
		$lines = explode("\n", $repr);
		$last  = array(); // the last entry per level
		foreach($lines as $line){
			if(!ConfigEntry::probe($this->master, $line)) continue;
			// get indentation and thus the parent right
			$line   = str_replace("\t", "    ", $line);
			$indent = strspn($line, " ");
			if(count($last)==0) $indent = 0; // first entry never has a parent
			$parent = getParent($last, $indent);
			// build the entry
			$entry  = new ConfigEntry($this, NULL, NULL, NULL, NULL, $line);
			$this->set($entry->key, $entry);
			if(!empty($parent)){
				$entry->parent = $parent;
				$parent->addChild($entry);
			}
			$last[$indent] = $entry;
		}
	}

	// probe
	// ---------------------------------------- 
	public static function probe($master, $repr) {
		/* Builds the config from a string representation */
		if(!is_string($repr)) return false;
		if(empty($repr)) return false;
		$result = false;
		$lines  = explode("\n", $repr);
		foreach($lines as $line){
			if(!ConfigEntry::probe($master, $line)) continue;
			return true;
		}
		return false;
	}

	// repr
	// ---------------------------------------- 
	public function repr() {
		/* Builds and returns the string representation */
		$repr = "";
		foreach($this->cache as $entry)
			$repr.=$entry->repr()."\n";
		return $repr;
	}




	// object-level functionality
	// ========================================

	// complement
	// ---------------------------------------- 
	public function complement($other) {
		/* Returns a copy of the Config holding entries that are in the 
		* other config but not in this one. Note, the entries are _not_ 
		* cloned, rather their references are linked. */
		$new = new Config($this->master, $this->name.(!empty($other) ? "_".$other->name : "")."_complement");
		if(empty($other) || !is_object($other) || !property_exists($other, "cache")) return $new;
		foreach($other->cache as $key=>$entry){
			if($this->has($key)) continue;
			$new->cache[$key] = $entry;
		}
		return $new;
	}

	// duplicate
	// ---------------------------------------- 
	public function duplicate($newname = NULL) {
		/* Returns a copy of the config with all its entries copied too. */
		$new = new Config($this->master, !empty($newname) ? $newname : $this->master->cloneION($this->name));
		foreach($this->cache as $entry)
			$new->set($entry->name, $entry->duplicate($new));
		return $new;
	}

	// getByType
	// ---------------------------------------- 
	public function getByType($typename=NULL, $hierarchical=false) {
		/* Returns a list of all entries that have the desired entry type */
		$result = array();
		$this->hierarchical = $hierarchical;
		$this->buildVirtualList();
		foreach($this as $key=>$entry){
			if(!empty($typename) && $entry->type->name!=$typename) continue;
			$result[$key] = $entry;
		}
		return $result;
	}

	// identical
	// ---------------------------------------- 
	public function identical($other) {
		/* Returns true if the other config holds the same information
		* as this config, otherwise false */
		if(empty($other) || !is_object($other) || get_class($other)!=get_class($this)) return false;
		if(count($this->cache) != count($other->cache)) return false;
		foreach($other->cache as $key=>$entry){
			if(!$this->has($key)) return false;
			if($this->get($key)->repr() != $entry->repr()) return false;
		}
		return true;
	}

	// intersection
	// ---------------------------------------- 
	public function intersection($other) {
		/* Returns a copy of the Config holding entries that are in the 
		* other config and also in this one. Note, the entries are _not_ 
		* cloned, rather their references are linked. */
		$new = new Config($this->master, $this->name.(!empty($other) ? "_".$other->name : "")."_intersection");
		if(empty($other) || !is_object($other) || get_class($other)!="Config") return $new;
		foreach($other->cache as $key=>$entry){
			if(!$this->has($key)) continue;
			$new->cache[$key] = $entry;
		}
		return $new;
	}

	// merge
	// ---------------------------------------- 
	public function merge($other) {
		/* Adds the entries from another config into this one. If 
		* the key is already assigned to an entry, it is overwritten. */
		if(empty($other) || !is_object($other) || strpos(get_class($other), "Config")===false) return;
		foreach($other->cache as $key=>$entry)
			$this->set($key, $entry->duplicate($this));
	}

	// sum
	// ---------------------------------------- 
	public function sum($other) {
		/* Adds the entries from another config into this one. If 
		* the key is already assigned to an entry, it is ignored. */
		if(empty($other) || !is_object($other) || strpos(get_class($other), "Config")===false) return;
		foreach($other->cache as $key=>$entry){
			if($this->has($ekey)) continue;
			$this->set($key, $entry->duplicate($this));
		}
	}

	// union
	// ---------------------------------------- 
	public function union($other) {
		/* Returns a copy of the Config holding entries that are in the 
		* other config or in this one. Note, the entries are _not_ 
		* cloned, rather their references are linked. */
		$new = new Config($this->master, $this->name.(!empty($other) ? "_".$other->name : "")."_union");
		if(empty($other) || !is_object($other) || get_class($other)!="Config") return $new;
		foreach($this->cache as $key=>$entry)
			$new->cache[$key] = $entry;
		foreach($other->cache as $key=>$entry)
			$new->cache[$key] = $entry;
		return $new;
	}
}





// DbConfig
// ============================================
class DbConfig extends Config {
	/* Special config implementation for data base handler */


	// members
	// ---------------------------------------- 
	public $suspended = false;


	// __construct
	// ---------------------------------------- 
	public function __construct($master, $name) {
		/* Constructor */
		parent::__construct($master, $name);
		$this->suspended = false;
		$this->buildEntries();
		$this->addJointype("left outer");
	}

	// __call
	// ---------------------------------------- 
	public function __call($key, $args) {
		/* Setting parameters via call, e.g. $config->order('column1', 'desc'); */
		if($key=="group"   ) return call_user_func_array(array($this, "addGroup"   ), $args);
		if($key=="limit"   ) return call_user_func_array(array($this, "addLimit"   ), $args);
		if($key=="order"   ) return call_user_func_array(array($this, "addOrder"   ), $args);
		if($key=="raw"     ) return call_user_func_array(array($this, "addRaw"     ), $args);
		if($key=="reformat") return call_user_func_array(array($this, "addReformat"), $args);
		if($key=="select"  ) return call_user_func_array(array($this, "addSelect"  ), $args);
		return false;
	}

	// __set
	// ---------------------------------------- 
	public function __set($key, $value) {
		/* Setting parameters via assignment, e.g. $config->limit = 2; */
		if($key=="avoid"     ) $this->addAvoid     ($value);
		if($key=="avoids"    ) $this->addAvoids    ($value);
		if($key=="column"    ) $this->addColumn    ($value);
		if($key=="columns"   ) $this->addColumns   ($value);
		if($key=="joinconfig") $this->addJoinconfig($value);
		if($key=="joinon"    ) $this->addJoinon    ($value);
		if($key=="jointype"  ) $this->addJointype  ($value);
		if($key=="slim"      ) $this->addSlim      ($value);
		if($key=="spattern"  ) $this->addSpattern  ($value);
		if($key=="table"     ) $this->addTable     ($value);
	}

	// addAvoid
	// ---------------------------------------- 
	public function addAvoid($value) {
		/* Adds a ConfigEntry objects for the column to be avoided */
		if(is_array($value)) return;
		$res = $this->cache["avoids"]->value;
		array_push($res, strtolower($value));
		$this->cache["avoids"]->value = $res;
	}

	// addAvoids
	// ---------------------------------------- 
	public function addAvoids($value) {
		/* Adds a ConfigEntry objects for the columns to be avoided */
		if(!is_array($value)) return;
		foreach($value as $elm) 
			$this->addAvoid($elm);
	}

	// addColumn
	// ---------------------------------------- 
	public function addColumn($value) {
		/* Adds a ConfigEntry objects for the column to be selected */
		if(is_array($value)) return;
		$res = $this->cache["columns"]->value;
		array_push($res, strtolower($value));
		$this->cache["columns"]->value = $res;
		//array_push($this->cache["columns"]->value, $value);
	}

	// addColumns
	// ---------------------------------------- 
	public function addColumns($value) {
		/* Adds a ConfigEntry objects for the columns to be selected */
		if(!is_array($value)) return;
		foreach($value as $elm) 
			$this->addColumn($elm);
	}

	// addGroup
	// ---------------------------------------- 
	public function addGroup($name) {
		/* Adds a new rule on how to group the selected rows */
		$name  = strtolower($name);
		$entry = new ConfigEntry($this, NULL, NULL, NULL, NULL, sprintf("dbgroup::%s::", $name));
		$this->set("dbgroup_".$entry->name, $entry);
	}

	// addJoinconfig
	// ---------------------------------------- 
	public function addJoinconfig($value) {
		/* Adds a ConfigEntry objects for the join on statements */
		$res = $this->cache["joinconfig"]->value;
		array_push($res, $value);
		$this->cache["joinconfig"]->value = $res;
		//array_push($this->get("joinconfig")->value, $value);
	}

	// addJoinon
	// ---------------------------------------- 
	public function addJoinon($value) {
		/* Adds a ConfigEntry objects for the join on statements */
		$res = $this->cache["joinon"]->value;
		array_push($res, strtolower($value));
		$this->cache["joinon"]->value = $res;
		//array_push($this->get("joinon")->value, $value);
	}

	// addJointype
	// ---------------------------------------- 
	public function addJointype($value) {
		/* Adds or updates the ConfigEntry for the join type */
		$this->get("jointype")->value = strtolower($value);
	}

	// addLimit
	// ---------------------------------------- 
	public function addLimit($value, $offset=0) {
		/* Adds or updates the ConfigEntry for the limit */
		// problematic to do so when overwriting it... stupid oracle
		//if($value >0) $this->addSelect("rownum", $value   , "below");
		//if($offset>0) $this->addSelect("rownum", $offset-1, "above");
		$this->get("limit")->value         = $value;
		$this->get("limit")->offset->value = $offset;
	}

	// addOrder
	// ---------------------------------------- 
	public function addOrder($name, $value="asc", $add=NULL) {
		/* Adds a new rule on how to order the selected rows */
		$name  = strtolower($name );
		$value = strtolower($value);
		$entry = new ConfigEntry($this, NULL, NULL, NULL, NULL, sprintf("dborder::%s::%s%s", $name, $value, !empty($add) ? "::add:=".$add : ""));
		$this->set($entry->name, $entry);
	}

	// addRaw
	// ---------------------------------------- 
	public function addRaw($name, $value=0) {
		/* Adds a raw string to be appended to the select statement 
		* at different locations */
		//$name  = strtolower($name );
		$key   = (strlen($name)<=20) ? strtolower($name) : strtolower(substr($name, 0, 20));
		$entry = new ConfigEntry($this, NULL, NULL, NULL, NULL, sprintf("dbraw::%s::%s::pos:=%d", $key, $name, $value));
		$this->set($entry->name, $entry);
	}

	// addReformat
	// ---------------------------------------- 
	public function addReformat($name, $value) {
		/* Adds a reformat rule, e.g. 'SELECT $name AS $value, ...' */
		$name  = strtolower($name );
		$value = strtolower($value);
		$key   = (strlen($name)<=20) ? $name : substr($name, 0, 20);
		$entry = new ConfigEntry($this, NULL, NULL, NULL, NULL, sprintf("dbreformat::%s::%s::repl:=%s", $key, $name, $value));
		$this->set($entry->name, $entry);
	}

	// addSlim
	// ---------------------------------------- 
	public function addSlim($value) {
		/* Sets the value of the slim parameter to the new value */
		$this->get("slim")->value = $value;
	}

	// addSpattern
	// ---------------------------------------- 
	public function addSpattern($value) {
		/* Updates the spattern value */
		$this->get("spattern")->value = strtolower($value);
	}

	// addSelect
	// ---------------------------------------- 
	public function addSelect($name, $value, $type="eq", $group=0, $link=NULL) {
		/* Adds a ConfigEntry of type dbselect to the config */
		$name  = strtolower($name );
		//$value = strtolower($value);
		$type  = strtolower($type );
		$link  = strtolower($link );

		$opts = "";
		if(gettype($value)=="object" && get_class($value)!="DbConfig") 
			return;
		else if(gettype($value)=="object" && get_class($value)=="DbConfig") {
			$this->master->setObj($value);
			$opts = "config:=".$value->name;
		}
		else if(is_array($value)){
			if($type=="eq" ) $type="inset";
			if($type=="neq") $type="outset";
			if($type=="inset" || $type=="outset"){
				if(count($value)==0) return;
				$value = array_filter($value, function($element) {return (gettype($element)=="integer" || gettype($element)=="float" || !empty($element));});
				$opts = $type.":=(".implode(",", array_map("strval", $value)).")";
			}
			else if($type=="inrange" || $type=="outrange"){
				if(count($value)<1) return;
				$ranges = array();
				foreach($value as $range){
					if(!is_array($range) || count($range)!=2) continue;
					array_push($ranges, "(".strval($range[0]).",".strval($range[1]).")");
				}
				if(count($ranges)<1) return;
				$opts = $type.":=(".implode(",", $ranges).")";
			}
			else 
				return;
		}
		else {
			$opts = $type.":=".$value;
		}

		// bracket group
		$opts .= "; group:=".strval($group);

		// closing bracket group
		if($link) $opts .= "; link:=".$link;

		// build entry and append it
		$ion = $this->master->makeION("dbselect_".$name);
		$entry = new ConfigEntry($this, $ion, $name, $name, $this->master->getObj("SuperContainerType", "dbselect"), NULL, $opts);
		$this->set($ion, $entry);
		//$this->set($entry->name, $entry);
	}

	// addTable
	// ---------------------------------------- 
	public function addTable($value) {
		/* Adds or updates the ConfigEntry for the table */
		$this->get("table")->value = strtolower($value);
	}

	// buildEntries
	// ---------------------------------------- 
	private function buildEntries(){
		/* Builds all required ConfigEntry entities */
		$this->set("avoids"    , new ConfigEntry($this, NULL, NULL, NULL, NULL, "stringlist::avoids::default"    ));
		$this->set("columns"   , new ConfigEntry($this, NULL, NULL, NULL, NULL, "stringlist::columns::default"   ));
		$this->set("joinconfig", new ConfigEntry($this, NULL, NULL, NULL, NULL, "objectlist::joinconfig::default"));
		$this->set("joinon"    , new ConfigEntry($this, NULL, NULL, NuLL, NULL, "stringlist::joinon::default"    ));
		$this->set("jointype"  , new ConfigEntry($this, NULL, NULL, NULL, NULL, "string::jointype::left outer"   ));
		$this->set("limit"     , new ConfigEntry($this, NULL, NULL, NULL, NULL, "dblimit::limit::0::offset:=0"   ));
		$this->set("slim"      , new ConfigEntry($this, NULL, NULL, NULL, NULL, "boolean::slim::false"           ));
		$this->set("spattern"  , new ConfigEntry($this, NULL, NULL, NULL, NULL, "string::spattern::default"      ));
		$this->set("table"     , new ConfigEntry($this, NULL, NULL, NULL, NULL, "string::table::default"         ));
	}

	// getReformat
	// ---------------------------------------- 
	public function getReformat($value) {
		/* Returns the full name for a short name */
		$value = strtolower($value);
		foreach($this->getByType("dbreformat") as $entry){
			if($entry->repl->value != $value) continue;
			return $entry->value;
		}
		return NULL;
	}

	// merge
	// ---------------------------------------- 
	public function merge($other) {
		/* Adds the entries from another DbConfig object into this one. In 
		* conflicting cases, the entry in this config is overwritten. */
		if(empty($other) || !is_object($other) || get_class($other)!="DbConfig") return;
		$avoids     = array_unique(array_merge($this->avoids    ->value, $other->avoids    ->value));
		$columns    = array_unique(array_merge($this->columns   ->value, $other->columns   ->value));
		$joinon     = array_unique(array_merge($this->joinon    ->value, $other->joinon    ->value));
		$joinconfig = array_unique(array_merge($this->joinconfig->value, $other->joinconfig->value));
		$this->columns   ->value = $columns;
		$this->joinon    ->value = $joinon;
		$this->joinconfig->value = $joinconfig;
		if(!empty($other->jointype->value)) $this->jointype     ->value = $other->jointype     ->value;
		if(!empty($other->limit   ->value)) $this->limit        ->value = $other->limit        ->value;
		if(!empty($other->limit   ->value)) $this->limit->offset->value = $other->limit->offset->value;
		if(!empty($other->table   ->value)) $this->table        ->value = $other->table        ->value;
		if(!empty($other->slim    ->value)) $this->slim         ->value = $other->slim         ->value;
		if(!empty($other->spattern->value)) $this->spattern     ->value = $other->spattern     ->value;
		$this->mergeByType($other, "dbgroup"   );
		$this->mergeByType($other, "dborder"   );
		$this->mergeByType($other, "dbraw"     );
		$this->mergeByType($other, "dbreformat");
		$this->mergeByType($other, "dbselect"  );
	}

	// mergeByType
	// ---------------------------------------- 
	public function mergeByType($other, $typename) {
		/* Merges all entries of type dbgroup, dborder, dbselect, dbraw,
		* or dbreformat from another DbConfig into this one; the
		* point is, both configs can have multiple entries of this
		* type; if the key is shared by two entries in the two
		* configs, though, the values and options stored in the
		* respective entries must be merged; otherwise the entries
		* simply can be imported */
		if(!in_array($typename, array("dbgroup", "dborder", "dbraw", "dbreformat", "dbselect"))) return;
		$my    = $this->getByType($typename);
		$names = array_keys($my);
		foreach($other->cache as $key=>$entry){
			if($entry->type->name!=$typename) continue;
			//print "merging ".$entry->type->name." ".$key."<br />";
			if(!in_array($key, $names)){
				if($this->has($key)) continue; // key already occupied with different type
				$this->cache[$key] = $entry->duplicate($this);
			}
			else {
				$myentry = $this->get($key);
				if($myentry->type->name!=$typename) continue;
				$myentry->value = !empty($entry->value) ? $entry->value : $myentry->value;
				$myentry->merge($entry);
			}
		}
	}

}



/*
// Instructions on how to use the DbConfig:

$master = NULL;
$ID_Equipment = 1;

$config = new DbConfig($master, "myConfig");
$config->joinon = "eq.id_equipment = statuslocation.eqentryid";
$config->joinon = "statuslocation.majorlocid = locations.id_locations";
$config->field = "eq.eqentryid";
$config->field = "eq.eqtypeid";
$config->reformat("equipment"                                       , "eq");
$config->reformat("to_char(createtime, 'YYYY-MM-DD')"               , "createtime");
$config->reformat("to_char(eventdate , 'YYYY-MM-DD HH24:MI:SS.FF6')", "eventdate" );
$config->select("id_equipment",$ID_Equipment);
$config->select("eventdate"   ,$otherconfig );
*/




?>

