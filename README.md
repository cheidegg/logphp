# Quick readme on new WebView framework.

## General

This is the repository for the new WebView framework that grew out of
https://gitlab.cern.ch/atlas-muon-nsw-db/web (esp. LOG.php therein).
It contains central modules (c.f. `library/`) with pages (c.f. `pages/`)
that are organized in themes (log, qaqc, ...). In particular, the original
pages (e.g. LOG.php) do not exist anymore, rather they are resolved in
a theme (in this case "log") with the individual pages of the theme 
implemented in dedicated classes in the `pages/<theme>/` folders. The
templates are stored accordingly in the `templates/<theme>/` folder.
To set up the framework correctly, one needs to also choose a "setup",
i.e. a set of parameters related to the data base connection (either
"playground" or "production").

The code originally was written in modern PHP, but then made backwards
compatible with PHP 5.3.3. (some dereferencing statements and the use
of traits have been removed), which unfortunately is the version installed
on the default CERN webservers accessing EOS. Also, there are some fixes
related to the Oracle version (playground >12, production <12), which
can be found in `library/database.py`.


## Installation

First, extract the repo to your desired location:

```
cd <root directory>
git clone ssh://git@gitlab.cern.ch:7999/cheidegg/logphp.git .
```

Then, make sure the webserver has write access to that directory.
Alternatively, if you are only able to give write permissions to
a sub directory, you should first create the directory `sessions`
and give write permissions to the web server in that directory.

Third, ask Kim or Constantin to pass you the configuration files 
for the setup. They are not part of the repository as they contain
sensitive information. You may not add it to the repository or any
other public space. Copy the two files (`setup_playground` and
`setup_production`) to the `configs` directory. 

Fourth, you need to choose the setup that your framework should use.
For this, create a soft link to the desired configuration. For example,
for the playground this would be (from the root directory):
```
ln -s <absolute path to root>/configs/setup_playground configs/setup_default
```

In the `index.php` file around line 183 you should pick the proper
setup, e.g. again for the playground:
```
$this->session["setup"] = "playground";
```

Fifth, in the setup file of your choice is the specification of a
parameter called `base`. Please put the absolute path to the
root directory there. Then there is also a file called `configs/self`
which contains only the web path to the root (i.e. what you have to enter
in the web browser). Adjust it accordingly.

Sixth, one of the first things the framework is doing is to access
the data base and retrieve all tables and views and their columns. 
This is an integral part of how the data base reader in `library/database.php`
works. Unfortunately, the necessary requests timeout in the production
setup (though they are fine in the playground environment). To bypass
this, you shall ask Kim or Constantin for a python script `runFix.py` which
you should execute in the root directory of this framework. It accesses
the data base and retrieves the tables and columns once and stores the 
output in two text files that are added to the `configs` folder. These
are linked in the config file of the setup under the variables
`pathTables` and `pathColumns`. Unfortunately, you shall maintain these
files by hand. Also, do _not_ add them to the git repository, nor do so
for the `runFix.py` script.

Now your framework should be ready to run. If you find any problems
or bugs, please let us know. 

Note that points 4, 5, and 6 may be improved in the future.


## Development Status

Status 19th February 2020:
* The log theme is done and tested, only few (minor) issues, oftentimes
  related to different PHP or Oracle versions on different platforms. Also, 
  the generation of the time development plots that used to be outsourced 
  to dedicated scripts is integrated to the `pages/log/prod.php` code.
  Furthermore, several pages have been improved while debugging. 
* No additional changes since October in relation to the QAQC theme or 
  the java script or CSS files. 

Status 7th October 2019:
* The log theme is mostly done including templates and pages. Currently 
  debugging.
* For the production page in log, the display of the time development plots
  is done via the original scripts (`timeDevPlots.php` and `timeDevMultiPlots.php`)
  that are included in the repo. In case this is changed, the scripts will be
  removed.
* A new theme for the QAQC page (theme will be called "qaqc") is planned.
* In general, the CSS and JS can be improved for all themes.
  The original CSS and JS files are included in the repo (`styles/` and
  `js/` directories) but new files have been added (`stylesNew/` and 
  `jsNew/` folders). For the CSS, the original
  files are used for the templates layout while the new files are only related
  to the new functionality (i.e. dropdown menu). For the JS, the relevant
  functions have been copy-pasted from the original to the new scripts and
  only the new scripts are included. Some of the functions have been renamed,
  reorganized, or rewritten.

