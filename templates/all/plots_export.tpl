<script type='text/javascript'>
// export buttons (csv and xlsx)
var tableId = '$tableId';
var ExportButtons = document.getElementById(tableId);
var instance = new TableExport(ExportButtons, {
                                               filename: '$expFileName',
                                               formats : ['xlsx','csv'],
                                               exportButtons: false});
var XLSX = instance.CONSTANTS.FORMAT.XLSX;
var CSV  = instance.CONSTANTS.FORMAT.CSV;
var exportDataXLSX = instance.getExportData()[tableId][XLSX];
var exportDataCSV  = instance.getExportData()[tableId][CSV];
var CSVbutton = document.getElementById('$linkCsv');
CSVbutton.addEventListener('click', function (e) {
	e.preventDefault();
	instance.export2file(exportDataCSV.data,
	                     exportDataCSV.mimeType,
	                     exportDataCSV.filename,
	                     exportDataCSV.fileExtension);
});
var XLSXbutton = document.getElementById('$linkXls');
XLSXbutton.addEventListener('click', function (e) {
	e.preventDefault();
	instance.export2file(exportDataXLSX.data,
	                     exportDataXLSX.mimeType,
	                     exportDataXLSX.filename,
	                     exportDataXLSX.fileExtension);
});
</script>
