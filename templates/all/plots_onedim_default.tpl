<script type='text/javascript'>
google.load('visualization', '1', {packages:['corechart']});
google.setOnLoadCallback(drawChart);		                   

// drawChart
function drawChart() {

	// the data
	var data = new google.visualization.DataTable($json)

	// chart parameters
	var options = {				                 	
		title:  '$plotTitle ($plotDate)',
		legend: {
		         maxLines: 3, 
		         position: 'top'
		        },
		width:  720,
		height: 360,
		colors: [$gcolors],
		annotations: {
		              alwaysOutside: true, 
		              textStyle: {fontName: 'Tahoma', fontSize: 11}, 
		              stem: {length: 17, color: 'grey'}
		             },
		pointShape: 'circle',
		pointsVisible: true,
		pointSize: 5,
		dataOpacity: 0.5,
		hAxis: {
		        title: '',
		        slantedText: true,
		        slantedTextAngle: 45,          								
		       },
		vAxis: {
		        title: 'Number of Items',
		        gridlines: {color: '#dddddd'},          								
		        baselineColor: '#ddd',
		       }, 
 		backgroundColor: '#f8f8f8',
 		chartArea: {
		            backgroundColor: {stroke: '#888', fill: '#fff'}
		           }
	};

	// prepare chart				                 
	var chart = new google.visualization.$plotType(document.getElementById('$canvasId'));

	// add link for png
	google.visualization.events.addListener(chart, 'ready', function () {
		document.getElementById('$png').innerHTML = '<a id="plotLinkPng" class="plotLink" target="_blank" href="' +chart.getImageURI()+'">png</a>';
	});

	// draw the plot
	chart.draw(data, options);
}
</script>
