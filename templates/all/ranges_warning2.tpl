<div class='warning'>
	<span class='warningteaser'>
		You have selected items created by splitting groups (MORE...)
	</span>
	<span class='warningtext'>
		There are items in your selection, which have been created by splitting a group.
		They are still linked to the group and therefore do not have their own details
		like &quot;supplier&quot; or &quot;manufacturing date&quot;. The detail attributes
		of these items cannot be modified by this tool, unless you break the link to the
		former group.
	</span>
</div>
