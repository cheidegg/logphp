<label class='invisdesc'>HISTORY:</label>
<table class='greytable smaller'>
	<colgroup>
		<col width='4%'>
		<col width='7%'>
		<col width='27%'>
		<col width='16%'>
		<col width='16%'>
		<col width='20%'>
		<col width='9%'>
	</colgroup>
	<thead>
		<tr>
			<th><center><button type="button" onclick="submitIt('delstat')" class="buttonlike flex" style="margin: 0px; font-size: 120%" $saveActive>&#8999;</button></center></th>
			<th>SHIP ID</th>
			<th>SENT TO<br />SENT FROM</th>
			<th>STATUS</th>
			<th>LOCATION</th>
			<th>DATE</th>
			<th>AUTHOR</th>
		</tr>
	</thead>
	<tbody>
		$rows
	</tbody>
</table>
<div style="display:none">
	<ul>
		<li>REG = registered</li>
		<li>SC  = status created</li>
		<li>LC  = location created</li>
		<li>SLC = status &amp; location created</li>
		<li>GC  = child created</li>
		<li>SH  = shipping</li>
		<li>MT  = mounted</li>                                                                          
	</ul>
</div>
