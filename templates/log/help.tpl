<link rel="stylesheet" href="stylesNew/log/help.css">
<div id="help_main" class="gbox">
	<div id="help_navleft" class="gbox column whalf">
		<h4> Structure </h4>
		<ul class="help_navlist">
			<li><div class="help_navlistitem"><a class="help_link" href="#news">1 News</a></div></li>
			<li><div class="help_navlistitem"><a class="help_link" href="#display_eq">2 Display Equipment</a> </div></li>
			<ul>
				<li><div class="help_navlistsubitem"> <a class="help_link" href="#display_eq_overview">2.1 Main Window</a> </div></li>
				<li><div class="help_navlistsubitem"> <a class="help_link" href="#display_eq_adddocs">2.2 Adding Documents</a> </div></li>
				<li><div class="help_navlistsubitem"> <a class="help_link" href="#display_eq_comment">2.3 Comments</a> </div></li>
				<li><div class="help_navlistsubitem"> <a class="help_link" href="#display_eq_childparent">2.4 Parents and Children</a> </div></li>
				<li><div class="help_navlistsubitem"> <a class="help_link" href="#display_eq_sort">2.5 Display Eq Class and Conformity</a> </div></li>
				<li><div class="help_navlistsubitem"> <a class="help_link" href="#display_eq_subb">2.6 Display Subbatches</a> </div></li>
				<li><div class="help_navlistsubitem"> <a class="help_link" href="#display_eq_status">2.7 Overview of Workflow</a> </div></li>
			</ul>
			<li> <div class="help_navlistitem"> <a class="help_link" href="#register_new_eq">3 Register Equipment</a> </div></li>
			<ul>
				<li> <div class="help_navlistsubitem"> <a class="help_link" href="#register_new_eq_singleitem">3.1 Register Single Eq. Item</a> </div></li>
				<li> <div class="help_navlistsubitem"> <a class="help_link" href="#register_new_eq_groupitems">3.2 Register Group of Items</a> </div></li>
				<li> <div class="help_navlistsubitem"> <a class="help_link" href="#register_new_eq_splititems">3.3 Split Group of Items</a> </div></li>
				<li> <div class="help_navlistsubitem"> <a class="help_link" href="#register_new_eq_regbatch">3.4 Register Batch</a> </div></li>
				<li> <div class="help_navlistsubitem"> <a class="help_link" href="#register_new_eq_createsubb">3.5 Create Subbatches</a> </div></li>
			</ul>
			<li> <div class="help_navlistitem"> <a class="help_link" href="#ship">4 Ship Equipment</a> </div></li>
			<ul>
				<li> <div class="help_navlistsubitem"> <a class="help_link" href="#ship_view">4.1 View Shipment</a></div></li>
				<li> <div class="help_navlistsubitem"> <a class="help_link" href="#ship_send">4.2 Send Equipment</a></div></li>
				<li> <div class="help_navlistsubitem"> <a class="help_link" href="#ship_receive">4.3 Receive Equipment</a> </div></li>
			</ul>
			<li> <div class="help_navlistitem"> <a class="help_link" href="#display_tables">5 Display Tables</a> </div></li>
			<ul>
				<li> <div class="help_navlistsubitem"> <a class="help_link" href="#display_tables_working_with_tb">5.1 Working with Tables</a> </div></li>
				<li> <div class="help_navlistsubitem"> <a class="help_link" href="#display_tables_usr_tbl">5.2 Users Tables</a> </div></li>
				<li> <div class="help_navlistsubitem"> <a class="help_link" href="#display_tables_admin_tbl">5.3 Admin Tables</a> </div></li>
				<li> <div class="help_navlistsubitem"> <a class="help_link" href="#display_tables_DB_Structure">5.4 DB Structure</a> </div></li>
				<li> <div class="help_navlistsubitem"> <a class="help_link" href="#display_tables_ex">5.5 Examples</a> </div></li>
			</ul>
			<li> <div class="help_navlistitem"> <a class="help_link" href="#admin">6 Admin</a> </div></li>
			<ul>
				<li> <div class="help_navlistsubitem"> <a class="help_link" href="#admin_lock">6.1 Lock/Unlock User</a></div></li>
				<li> <div class="help_navlistsubitem"> <a class="help_link" href="#admin_edit">6.2 Edit Admin Tables</a> </div></li>
			</ul>
			$multiedit_toc
		</ul>
	</div>
	<div id="help_mainright" class="bbox column whalf">


		<a name="news"></a>
		<h2 class="help_heading">1 News</h2>
		<p>
		On the news page you will find any actual information about the web interface or the database 
		itself, for example information about maintenance work or other important news. <br />
		Please check this page regularly. To reach the news page, click on <b>News</b> in the upper 
		navigation bar. It is also displayed at each login.
		</p>


		<a name="display_eq"></a>
		<h2 class="help_heading">2 Display Equipment </h2>
		<p>
		If you want to edit an already registered equipment or just 
		want to look up some details of it, go to <b>Display Equipment</b>
		in the navigation bar. Then you will see the following screen:
		</p>
		<p>
			<img src='img/help/Display_eq_header.png' class="help_img" alt='Display equipment header'/>
		</p>
		<p>
		Here you need to enter the MTF-number, the unique database ID (EQ_ID) 
		or the Equipment-Alias of the equipment you want to look at. Then click on 
		the corresponding search button and the frame which is shown below this paragraph will open. </br>
		In the <a class="help_link" href="#display_eq_overview">main window</a>, you get more information about the chosen equipment. 
		If you have the needed rights, you can also edit the fields or add information, which has not been provided during the registration. </br>
		Below this main screen, an <a class="help_link" href="#display_eq_adddocs">overview of documents</a> appended to this equipment is provided. 
		You can also upload new files there. Below the documents field, there is the <a class="help_link" href="#display_eq_comment">comments</a> field, 
		where the existing comments are displayed, and new ones can be added.<br />
		<br />
		You will find the overview of parents and children of the 
		current equipment on the bottom of the window. You can add a parent or child relationship here. 
		A parent is an equipment, to which this equipment is attached (for example: this equipment is a resistive foil 
		and it is attached to a readout board. A child equipment in this case would be some raw material, 
		that was used to produce the resistive foil.) </br></br>
		
		On the top right is a field 
		which displays the <a class="help_link" href="#display_eq_sort">class of equipment</a> (e.g. item, batch,...). 
		You can declare the <a class="help_link" href="#display_eq_sort">conformity</a> of your equipment there. <br />
		The field below displays the 
		<a class="help_link" href="#display_eq_subb">subbatches</a> of the equipment. And last but not least, there is a field, 
		which shows the <a class="help_link" href="#display_eq_subb">production flow</a> of the equipment. <br />
		<br />
		All the different windows are described in more detail in the following chapters.
		</p>


		<a name="display_eq_overview"></a>
		<h3 class="help_heading">2.1 Main Window </h3>
		<p>
			<img src='img/help/Display_eq_mainfield.png' class="help_img" alt='Register Equipment screen'/>
		</p>
		<p>
		All the fields which have text input fields (white box with light green boarder) below their headline, are editable. 
		Just click on the text box and then put in some free text or choose a value from the drop down menu if there is one available. 
		Any fields holding values that have been changed will appear with yellow-orange background. 
		If you want to discard your changes, click on the button <b>Undo Changes in Input Fields</b>. 
		If you verified your changes and you are sure that you want to save them to the database, click on <b>Save Changes in Input Fields </b>. 
		After clicking this button, there is no way to undo the recent changes anymore, so be sure to double-check any input.<br />
		<br />
		The following paragraph is a short description of each field: <br />
		<br />
		In the top left corner, you will find the the <b>MTF Number</b> of your equipment. 
		Also the type of equipment is given in a text description.<br />
		Next to the MTF number you will find the field for the <b>Equipment Alias</b>. 
		You can use this field if your equipment type has a numbering system besides the official MTF IDs (for example, 
		the supplier already numbered them in some way), 
		but it is recommended to leave it free and to use only the MTF number as identifier. Please be aware, 
		that also any given Alias ID must be unique for any item in the database. The system will not allow for two identical Aliases, 
		so this might be a reason why it refuses your changes during save actions. <br />
		<br />
		Beside this field you will find the <b>Subbatch ID</b> of your equipment. 
		This number is automatically set by the system when a subbatch is created, and can not be changed. 
		This is also the case for the unique <b>EQ ID</b>. 
		If your equipment is a batch or a group of items, the <b>Units in Batch</b> field has been set during 
		the registration, but can be edited here for the case, that it has changed or 
		maybe the supplier made a mistake in counting the items. Also you can set/edit the number of good (= passed 
		first QAQC) units within the batch.<br />
		In the <b>Current Status</b> field, you can change the status of your equipment. Therefore you have to 
		choose one of the statuses from the drop down menu. 
		Free text is not allowed in this field. The <b>Status Since</b> and <b>EQ Registered</b> fields are 
		again set by the system and therefore are not editable.<br />
		<br />
		The next row is used to provide information about were the equipment is located at the moment. 
		In the field <b>Current Location / Site</b> you should declare the 
		actual location by choosing the corresponding entry of the drop down menu. In the next column 
		you can specify the <b>Building or Room</b> where the equipment is stored. 
		Here free text is allowed. A drop down list of known rooms will be provided soon. <br />
		<br />
		The last two rows should be used to provide information about the order and the supplier. The 
		<b>Supplier</b> field is foreseen to just enter the name of the supplying company. 
		The more detailed order information (the DAI) should be provided in the <b>Order Reference</b> 
		whose link can be inserted in the corresponding field. 
		The <b>Ordered by</b> field should be used to give the name of the person, who ordered the 
		equipment and would be a good address for any upcoming questions regarding this order, 
		not the person processing the document itself. The <b>Order Date</b> field should be used to provide the date of the order.
		</p>


		<a name="display_eq_adddocs"></a>
		<h3 class="help_heading">2.2 Adding Documents </h3>
		<p>
			<img src='img/help/Display_eq_doc.png' class="help_img" alt='Display equipment documents screen'/>
		</p>
		<p>
		Here you get an overview of documents which are attached to your equipment. Next to this, 
		you can upload new files or you can add files from other equipment IDs. 
		Allowed file types are: txt, csv, jpg, png, pdf, root-makros. If you need additional document 
		upload types, please contact the Web Display Admins! <br />
		<br />
		To upload a new file click on <b>Upload new Doc</b>. Please provide the type of the document 
		by clicking on the corresponding entry of the dropdown menu. 
		Afterwards a file browser opens, and you can select the document to upload from your hard 
		drive. At the moment, you can only upload a single file at once. 
		The upload will start directly, without the need to click on <b>Save Changes in Input Fields</b>. 
		The upload might take a while, depending on your internet connection! 
		Afterwards, the file should appear in the list of files.
		</p>
		<p>
		To attach a file that has already been uploaded to some other equipment, you first have to 
		click on the <b>MTF-No.</b> button in the <b>Documents</b> tab. 
		A popup window appears, which asks you to provide the MTF number of the equipment where the 
		document is already attached.
		</p>
		<p>
			<img src='img/help/Display_eq_MTFfordoc.png' class="help_img" alt='Display equipment MTF for document screen'/>
		</p>
		<p>
		A click on the <b>Add existing Doc</b> button will show you a list of documents attached to the 
		provided MTF number.
		</p>
		<p>
			<img src='img/help/Display_eq_docfrommtf.png' class="help_img" alt='Register Equipment screen'/>
		</p>
		<p>
		Choose the one you want to attach to the current equipment and it will appear in the list 
		of attached documents for the currently dsiplayed equipment.
		</p>


		<a name="display_eq_comment"></a>
		<h3 class="help_heading">2.3 Comments </h3>
		<p>
		In this field you can find all comments about the currently displayed equipment. You can also add a new comment.<br />
		Therefore click on <b>Enter New Comment</b>, and a drop down menu appears which asks you for the type of the comment:
		</p>
		<p>
			<img src='img/help/Display_eq_commenttyp.png' class="help_img" alt='Register Equipment screen'/>
		</p>
		<p>
		Then a popup window appears, which asks you for the content of your comment:
		</p>
		<p>
			<img src='img/help/Display_eq_commentcont.png' class="help_img" alt='Register Equipment screen'/>
		</p>
		<p>
		After entering your comment, click on ok. Then the comment should appear without any further confirmation needed in the overview.
		</p>


		<a name="display_eq_childparent"></a>";
		<h3 class="help_heading">2.4 Children and Parents </h3>
		<p>
			<img src='img/help/Display_eq_parent_child.png' class="help_img" alt='Display Equipment: Children/Parent'/>
		</p>
		<p>
			In the children and parents tab, you can see the other equipment which is connected to your 
			displayed equipment. You can also add new connections. From this data, the tree view of the 
			modules on the left side is created, where you can navigate through all modules. All parts 
			are assigned to the Orphanage by default, if they do not have any other connections. When a real parent 
			equipment is declared, the affected part will be removed from the orphanage.<br />
			<br />
			To <b>attach a parent or a child</b> to the displayed entry, just click on <b>Attach this 
			entry to a parent EQ</b> or respetivly <b>Attach a child to this EQ Entry</b>. 
			A popup window will appear, asking you for the MTF ID of the parent/child. Please confirm your entry by clicking on ok. 
			If you want to cancel the process, cancel this and the following popups. This is the last 
			possibility to cancel, you will not be asked for any confirmation for this process.<br />
			<br />
			If you have clicked on ok, the next window appears and asks you for the Subbatch ID. If 
			your equipment has one, enter it. If not, you should leave the field free. <br />
			<br />
			Click on ok, then the last popup window appears, where you may specify the position of the 
			parent/child in the equipment it is connected to. It may be used to specify the position of the 
			child within the parent. For example, if you have a panel with two meshes, you may enter &quot;upper mesh&quot; 
			or &quot;lower mesh&quot;, to distinguish between the two meshes. Here you can enter free text, 
			but you should know and use any naming conventions. If you are not sure, check similar 
			equipments in the tree structure on the left.
		</p>
		<p>
			<img src='img/help/Display_eq_pc_mtfnr.png' class="help_img" alt='Display Equipment: Children/Parent'/>
		</p>
		<p>
			<img src='img/help/Display_eq_pc_subbatchid.png' class="help_img" alt='Display Equipment: Children/Parent'/>
		</p>
		<p>
			<img src='img/help/Display_eq_pc_pos.png' class="help_img" alt='Display Equipment:  Children/Parent'/>
		</p>
		<p>
			If the entered parent child relation is allowed, you should find the corresponding entry in the parents 
			or children overview now. Otherwise the creation of the parent child relation is canceled in the 
			background, without any error message.<br />
			<br />
			The allowed parentings can be looked up in the &quot;Display Table&quot; menu, in the &quot;Allowed Parenting Table&quot; 
			in the &quot;Admin Tables&quot; section. An explanation for these tables is given 
			<a class="help_link" href="#display_tables_admin_tbl">here</a>:
		</p>


		<a name="display_eq_sort"></a>
		<h3 class="help_heading">2.5 Display Eq Class and Conformity </h3>
		<p>
			<img src='img/help/Display_eq_type.png' class="help_img" alt='Display Equipment: Subbatch overview'/>
		</p>
		<p>
			Here the class of equipment (e.g. batch, item) is displayed. The left field may be used to specify the conformity of the equipment. 
			If it failed in QAQC, you can set it to <b>Non Conform</b>. If it passed QAQC, you can set it to <b>Conform</b>. The default value is <b>Unkown</b>.
		</p>


		<a name="display_eq_subb"></a>
		<h3 class="help_heading">2.6 Display Subbatches </h3>
		<p>
			<img src='img/help/Display_eq_subbatches.png' class="help_img" alt='Display Equipment: Subbatch overview'/>
		</p>
		<p>
			In this screen you get an overview of all existing subbatches of your equipment. By clicking on the <b>EQ ID</b> you can directly go to the subbatch.<br />
			If you divide a subbatch into sub subbatches, the subbatch still remains in the list at the moment. This is a known issue and will be corrected soon.
		</p>


		<a name="display_eq_status"></a>
		<h3 class="help_heading">2.7. Overview of Workflow</h3>
		<p>
			<img src='img/help/Display_eq_statuses.png' class="help_img" alt='Display Equipment: Subbatch overview'/>
		</p>
		<p>
			In this tab you get an overview about the workflow of the displayed equipment. The graphic is driven by the status field of your equipment.<br />
			<br />
			The bright green buttons indicate statuses which were explicitly set for the equipment in the past (or are still set now). The dark green ones are indirectly assumed from statuses, 
			which have been actively set at some point in time. For example, if the status of the equipment is delivered, it has had to be ordered first. If the equipment was 
			registered after the delivery only and nobody ever set the ordered-status, the ordered-button field will appear in dark green while the delivered-button will be bright green.
		</p>


		<a name="register_new_eq"></a>
		<h2 class="help_heading">3 Register Equipment </h2>
		<p>
			If you want to register some new equipment, go to <b>Register Equipment</b> in the navigation bar. You will see the following screen:
		</p>
		<p>
			<img src='img/help/RegEq.png' class="help_img" alt='Register Equipment screen'/>
		</p>
		<p>
			If you want to register just one single item or batch, then choose <a class="help_link" href="#register_new_eq_singleitem">Register Single Equipment Item</a> 
			or respectively <a class="help_link" href="#register_new_eq_regbatch">Register Batch</a>. If you want to register many items of the same type, first go to 
			<a class="help_link" href="#register_new_eq_groupitems">Register Group of Items</a> and if you registered the group of items, split it into single items by using 
			<a class="help_link" href="#register_new_eq_splititems">Split Group of Items</a>. If you want to split an existing batch into subbatches, just use the  
			<a class="help_link" href="#register_new_eq_createsubb">Create Subbatch</a> button.
		</p>


		<a name="register_new_eq_singleitem"></a>
		<h3 class="help_heading">3.1 Register Single Equipment Item </h3>
		<p>
			<img src='img/help/RegSingItem_EQType.png' class="help_img" alt='Part of the Register single item screen'/>
		</p>
		<p>
			At first you have to put in the identifier for your type of equipment, which can be looked up 
			<a class="help_link" href="#display_tables_ex">here</a> . Then press &#60TAB&#62 and wait a second until the version field appears:
		</p>
		<p>
			<img src='img/help/RegSingItem_EQVersion.png' class="help_img" alt='Part of the Register single item screen'/>
		</p>
		<p>
			Here you have to put in the current version number of your Equipment. Then confirm the version number by again pressing &#60TAB&#62.<br />
			If you have done all this correctly, you will find the generated MTF number for your item in the top right corner of the page, below the upper navigation bar.<br />
			<br />
			If you start building a new version and therefore a higher version number is needed, please write an email to kim.temming@physik.uni-freiburg.de and a new version number will be enabled.<br />
			<br />
			Up to here, all fields were mandatory. All the other fields, which are described in the following, can be filled out right now or later in the &quot;Display Equipment&quot; environment.
		</p>
		<a name="rest_of_form"></a>
		<p>
			<img src='img/help/RegSingItem_EQRest.png' class="help_img"  alt='Part of the Register single item screen'/>
		</p>
		<p>
			The field <b>Other ID (Alias)</b> can be used, if your equipment type has an numbering system besides the official MTF IDs. 
			But it is recommended to leave it free and to use only the MTF Number as identifier. If you want to provide an Other ID (Alias), 
			it must be unique! <br />
			<br />
			The field <b>Status</b> should be used to declare the current status of your item. Here you should choose one of the statuses from the drop down menu. 
			Free text is not allowed.<br />
			<br />
			The <b>Supplier</b> field should be used to give the name of the supplying company. Any detailed information about the supplier should be included in the order reference 
			document which can be linked in the corresponding field.<br />
			<br /> 
			The next few fields should be self descriptive.<br />
			<br /> 
			In the field <b>Select Document to upload</b> you can attach a document to your item. Please specify the <b>Type of Document</b> below. 
			Allowed document types for the upload are: txt, csv, jpg, png, pdf, root-makros. If you want to upload more than one document, 
			you can do this in the <a class="help_link" href="#display_eq_adddocs">Display Equipment</a> menu after the registration.<br />
			<br />
			The <b>User Creating this Entry</b> field is automatically filled with your username and cannot be changed.<br />
			<br />
			After you have filled all the input fields you want to fill now, you can finish the registering process by clicking on the button <b>Register Item</b>.
		</p>


		<a name="register_new_eq_groupitems"></a>
		<h3 class="help_heading">3.2 Register Group of Items</h3>
		<p>
			<img src='img/help/RegSingItem_EQTyp.png' class="help_img" alt='Part of the Register single item screen'/>
		</p>
		<p>
			At first you have to fill in the identifier for your type of equipment, which can be looked up
			<a class="help_link" href="#display_tables_ex">here</a> . Then press &#60TAB&#62 and wait a second till the version field appears:
		</p>
		<p>
			<img src='img/help/RegSingItem_EQVersion.png' class="help_img" alt='Part of the Register single item screen'/>
		</p>
		<p>
			Here you have to put in the current version number of your equipment. Then confirm the version number by again pressing &#60TAB&#62.<br />
			If you have done all this correctly, you will find the generated MTF number for your group of items in the top right corner of the page, below the upper navigation bar.<br />
			<br />
			If you start building a new version and therefore a higher version number is needed, please write an email to the administrator of this page (kim.temming@physik.uni-freiburg.de) 
			and it will be enabled.
		</p>
		<p>
			<img src='img/help/RegGroupItem_Quantity.png' class="help_img" alt='Part of the Register single item screen'/>
		</p>
		<p>
			Now you have to specify the number of items within the group in the field <b>Quantity of Parts in batch</b> <br />
			Up to this, all fields were mandatory. All the following fields can be filled out right now or later in the &quot;Display Equipment&quot; environment. 
			Since they are the same as in the &quot;Register Single Item Form&quot; they are described in 
			the <a class="help_link" href="#rest_of_form">corresponding chapter</a>.
		</p>


		<a name="register_new_eq_splititems"></a>
		<h3 class="help_heading">3.3 Split Group of Items</h3>
		<p>
			<img src='img/help/SplitGroup_header.png' class="help_img" alt='Header of the split group of item screen'/>
		</p>
		<p>
			At first you have to search for the group of items, which you want to split up. Therefore, enter the MTF number or the unique database ID (EQ ID) 
			and click on the corresponding search button. Then the following screen appears:
		</p>
		<p>
			<img src='img/help/SplitGroup_alias_overview.png' class="help_img" alt='Alias range field of the split group of item screen'/>
		</p>
		<p>
			Here you can see more information about your group. In the lower frame, you can optionally set a free text <b>equipment alias</b>, to which a running number is appended, to make it unique. 
			The range of the running number can be set in the two fields beside it. The last field can be used to set the total number of digits of the appended number. 
			If you have set the EQ Alias, click on <b>Select ALIAS Range</b> to see which MTF Number will be linked to which Other ID. <br />
			<br />
			If you get an error message, the reason may be that the Other ID (including the numbers) is already used by another equipment. 
			So you may have to adapt the range of the number and select the alias range again. If the Other ID is ok, you will get the following screen:
		</p>
		<p>
			<img src='img/help/SplitGroup_overview.png' class="help_img" alt='Part of the split group of item screen'/>
		</p>
		<p>
			Now you can actually split the group into its single items by clicking on <b>Split Group of Items</b>. This could take several seconds or even longer, depending on the size of your group. 
			Afterwards the items and their MTF numbers and EQ-IDs are displayed. If you do not want to set an EQ ALIAS, you can directly search for your group and split it, 
			without setting any EQ ALIAS or range. If the spliiting was succesfull, you will find the success message on the bottom of your screen, which is shown below this paragraph.<br />
			<br />
			If you split the group in this regular way, the single items will inherite all information about the ordering and manufacturing process from the group even after splitting, 
			also if the group information will be changed in future.<br />
			Therefore this information cannot be changed at the single items but only in the group.<br />
			<br />
			If you want to create items, whose delivery information is individually editable, set the checkbox <b>Split group into individual items</b>. 
			Then the items will copy the information from the group, but will be independed of it afterwards. So if you change the groups information later, 
			the items will not change, but will have their own delivery information (which can also be changed individually through the Display-Equipment-screen.).
		</p>
		<p>
			<img src='img/help/SplitGroup_success.png' class="help_img" alt='Part of the Register single item screen'/>
		</p>


		<a name="register_new_eq_regbatch"></a>
		<h3 class="help_heading">3.4 Register Batch </h3>
		<p>
			<img src='img/help/RegSingItem_EQType.png' class="help_img" alt='Part of the Register single item screen'/>
		</p>
		<p>
			At first you have to put in the identifier for your type of equipment, which can be looked up <a class="help_link" href="#display_tables_ex">here</a>. 
			Then press &#60TAB&#62 and wait a second until the version field appears:
		</p>
		<p>
			<img src='img/help/RegSingItem_EQVersion.png' class="help_img" alt='Part of the Register single item screen'/>
		</p>
		<p>
			Here you have to put in the version number of your equipment. Then confirm the version number by again pressing &#60TAB&#62.<br />
			If you have done all this correctly, you will find the generated MTF number for your batch in the top right corner of the page, 
			below the upper navigation bar. <br />
			<br />
			If you start building a new version and therefore a higher version number is needed, please write an email to the
			administrator of this page (kim.temming@physik.uni-freiburg.de) and it will be enabled.
		</p>


		<a name="register_new_eq_createsubb"></a>
		<h3 class="help_heading">3.5 Create subbatches </h3>
		<p>
			Subbatches are inheriting all delivery information from the mother batch. This includes the supplier, 
			the manufacturing and delivery date, the information about the ordering process, 
			and so on. Because of this, you should only create subbatches from an already delivered batch where 
			you should set the corresponding delivery information for the main batch. 
			If an ordered batch already arrives in subbatches, you need the &quot;split into independend subbatches&quot;
			function, which will be provided later.<br />
			In subbatches, you cannot change the delivery information individually. <br />
			<br />
			Splitting subbatches is also possible, if needed for shipping purposes. The sub subbatches then inherit 
			the delivery information of the subbatch and so from the original mother batch. <br />
			In the following, I will always write about splitting batches, which also includes splitting subbatches.
		</p>
		<p>
			<img src='img/help/SplitBash_header.png' class="help_img" alt='Part of the split group of item screen'/>
		</p>
		<p>
			To split a batch, at first you have to search for the batch, which you want to split up. Therefore enter its MTF number or the unique database ID (EQ ID) 
			and click on the corresponding search button. A search field for the &quot;EQ Alias&quot; will be added later. Then the following screen appears:
		</p>
		<p>
			<img src='img/help/SplitBash_overview.png' class="help_img" alt='Part of the split group of item screen'/>
		</p>
		<p>
			Here you can see more information about the chosen batch. In the field <b>Quantity of required Subbatches</b> you have to choose how many subbatches you want to create. 
			Afterwards you have to confirm this by clicking on <b>Define Quantities</b>. You will then get the following window where you can divide the batch into the single subbatches.
		</p>
		<p>
			<img src='img/help/SplitBash_subbatches.png' class="help_img" alt='Part of the split group of item screen'/>
		</p>
		<p>
			If you have divided all quantities and all good quantities of the batch into the several subbatches, you must confirm your choices by clicking on 
			<b>Save Subbatches</b> on the top of the window. You will then get a success message on the bottom of the screen. 
			Be aware: Once you clicked on <b>Save Subbatches</b>, you <b>can not undo the splitting</b>.
		</p>


		<a name="ship"></a>
		<h2 class="help_heading"> 4 Ship Equipment </h2>
		<p>
			In the shipping section you can send and receive equipment. Go to <b>Ship Equipment</b> in the navigation bar. You will see the following screen:
		</p>
		<p>
			<img src='img/help/Ship_Overview.png' class="help_img" alt='Shipping Overview'/>
		</p>
		<p>
			You can find out everything about a given shipment with its shipment ID at <a class="help_link" href="#ship_view">Shipping View</a>. 
			If you want to send some equipment to another prodcution site, choose 
			<a class="help_link" href="#ship_send">Send Equipment</a>. <br />
			If someone has shipped something 
			to you and you want to tell the databse that it has arrived, go to <a class="help_link" href="#ship_receive">Receive Equipment</a>. <br />
		</p>


		<a name="ship_view"></a>
		<h3 class="help_heading">4.1 Shipping View </h3>
		<p>
			In this part of the shipping menu you can examine a given shipment, if you know its shipment ID. <br />
			Ideally, the person shipping something to you will write this number down somewhere in the shipping papers. <br />
			You can also go to <a class="help_link" href="#ship_receive">Receive Equipment</a> and look up, which shippings are on 
			their way towards you and you will probably find the one you are looking for, especially if you additionally know the sending site.<br />
			<br />
			Enter the shipment number in the field <b>Shipment-ID</b> and click on <b>load shipment</b>.
			 All available information regarding this shipment will be loaded into the mask, which might look like in the following example:
		</p>
		<p>
			<img src='img/help/Shipping_View2.png' class="help_img" alt='Shipping View'/>
		</p>
		<p>
			This view includes a complete list of all the equipment in this shipment, the sending site, person and date, the receiving site, person and date 
			as well as the export and import papers link and the shipping service provider. If existing, any papers attached to this shipment are listed here, too. 
			You can download them, if you like.
		</p>


		<a name="ship_send"></a>
		<h3 class="help_heading">4.2 Send Equipment</h3>
		<p>
			<img src='img/help/send_equipment_1.png' class="help_img" alt='Send equipment screen part 1'/>
		</p>
		<p>
			The send equipment tab brings you to a screen, where you can choose equipment that you want to ship around in the world. 
			You can select any equipment for shipping, which fulfills the following criteria: 
			<ul id="help_list">
				<li> Its location entry in the database is currently some production site.</li>
				<li> It is neither already in a shipping process nor is it attached to some parent equipment 
				(i.e. you cannot ship the resistive foil, once it is on a readout board. You would ship only the readout board, then).</li>
				<li> You cannot ship groups, once they are split up. Please ship the single equipment pieces, the group ceased to exist 
				once it was split (except for its delivery information).</li>
				<li> All equipment that will be shipped together needs to be at the <b>same location</b>. The interface will not let you 
				choose equipment with different locations, because shipping them together would not make sense in reality and would also confuse the DB.
				So, the interface will only let you add material from the <b>location of the first equipment in the selection</b> 
				and will mark the others as being wrong. </li>
			</ul>
			It is possible to enter ranges of MTF-IDs, EQ-IDs or aliases into the selection. Please be aware, that the interface will also complain, 
			if the items inside these ranges do not fulfill above critera, especially should they all be at the <b>same location</b>.<br />
			<br />
			If there are only valid items in the shipping list, you can proceed to fill in the information about the shipment in the following fields:
		</p>
		<p>
			<img src='img/help/send_equipment.png' class="help_img" alt='Send equipment screen part 2'/>
		</p>
		<p>
			You need to specify the shipping destination and the shipping date. Ideally you also fill in the person packing the box and/or ordering the truck, 
			depending on the size of the equipment.<br />
			It is possible to enter an expected returning date (if you send it somewhere for machining, for example) and to specify a link to the 
			export papers (if the equipment leaves CERN and will be brought back later).<br />
			<br />
			Optionally, you can attach shipping papers.<br />
			<br />
			If everything is filled in and the equipment list is valid, the <b>send equiment</b> - button will be made active. 
			After clicking it, you will receive your <b>shipment ID</b>. 
			You should write this number onto your parcel or onto the shipping papers somewhere, because the recipient will look for it.
		</p>


		<a name="ship_receive"></a>
		<h3 class="help_heading">4.3 Receive Equipment</h3>
		<p>
			The receiving of shippings is simpel, if you have the shipment ID (which should come together with the package or the shipping papers to you).
			You simply need to enter this number in the upper part of the <b>receive equipment</b> - screen. In the picture it was the shipment number 28, 
			then click on <b>load shipment</b>.
		</p>
		<p>
			<img src='img/help/receive_1.png' class="help_img" alt='Receive screen first part'/>
		</p>
		<p>
			The system will look up all the items that shipment 28 contains. You should now tick all the items that you 
			want to receive (probably in most cases, this will just be all items of a given shipment...). There is also a button to select all, if you scroll down to the end of the list.<br />
			<br />
			You could also only receive the first 5 of these 6 items: For example, if they were send in 3 packages and one got lost.<br />
			<br />
			If you do not know the shippment ID of the shipping that just came by mail, you can search for it. It should be safe to assume, 
			that you either know where it arrived (obvious, if you are the receiving person), or you know where it was sent from (also obvious, 
			if you are the sender). Ideally, you know both. You can then select the correct sending and receiving locations from the drop down menus, 
			instead of entering a shipment-ID. If you do not know one of these, simply select <b>all</b> in the corresponding field.<br />
			<br />
			The interface will now present you all items with the selected attributes (i.e. sent from Japan to CERN-PCB), that are currently in transit 
			and have not been received yet. You now have to select the ones, that you want to receive. 
			<br><br>Please notice: The shipment IDs are given in the table. If you find one of the items in your shipment, you can simply look up the 
			shipment ID and enter above. The equipment table will be reduced to this shipment only afterwards.
		</p>
		<p>
			<img src='img/help/receive_2.png' class="help_img" alt='Receive screen second part'/>
		</p>
		<p>
			Now that you selected all of the items which you want to receive, you need to enter new information for them. 
			They need a new status as well as a new location (both mandatory).<br />
			<br />
			If you find, that some part of the equipments to be received should have a different status than another part of them, you should receive 
			them in two DB operations or simply change the status of one part afterwards again, since there is only one status to choose for 
			the whole equipment list.<br />
			<br />
			You can again attach papers as well as an import link. Please also give your name or the name of the person who received the shipment, 
			if this is not identical.<br />
			<br />
			Complete the request by clicking on the <b>receive equiment</b> - button. Now the interface will set the new status and location information 
			to all equipments that you selected. It will also close this shipping operation and reopen the possibility to ship these items again to another destination.
		</p>


		<a name="display_tables"></a>
		<h2 class="help_heading">5 Display Tables </h2>
		<p>
			<img src='img/help/Display_tb_main.png' class="help_img" alt='Part of the split group of item screen'/>
		</p>
		<p>
			If you are interested in the architecture of the database, or you just want to find your newly registered equipment again, this menu will help you. 
			It shows you the raw tables inside the database. There are three different categories. In <b>User Tables</b> there are several tables, which provide 
			the full information on registered equipment.<br />
			In <b>DB Structure</b>, you will find tables, which are providing the architecture of the database. <br />
			In <b>Admin Tables</b> you will find tables, which are used as a dictionary, 
			for example for several drop down menus or even to define the existing equipment types. <br />
			<br />
			In the following there will be a short chapter on how to work with these tables. Then the most important tables will be described in detail. 
			Afterwards you will find some examples about how to extract useful information from the tables.
		</p>


		<a name="display_tables_working_with_tb"></a>
		<h3 class="help_heading">5.1 Working with Tables</h3>
		<p>
			All the shown tables are read only. If you are an admin you can edit some tables in the admin menu. <br />
			<br />
			To sort the table by a column, just click on the column header. The double arrow will be replaced by a single one, which gives you information about the sorting direction. 
			If you want to invert the sorting direction, again click on the header, and the sorting direction as well as the arrow direction will be inverted. <br />
			To sort by two or more columns, click on the first one. Then hold &#60SHIFT&#62 and click on the other columns. The table should look like this then:
		</p>
		<p>
			<img src='img/help/Display_tb_sort_many_cols.png' class="help_img" alt='Part of the split group of item screen'/>
		</p>
		<p>
			If you are searching for a single entry, you can do this with the search function of your browser (often &#60Ctrl&#62+&#60f&#62 ).<br />
			<br />
			For each table you have the posibillity to <b>export it as csv or excel file</b>.<br />
			<br />
			There is also a template provided, which can be used to import data directly into a table without the need to use this web interface. 
			Therefore fill the provided csv file with your data and send it to the administrator of this site(kim.temming@physik.uni-freiburg.de). 
			Then the data will be imported for you. <b>Prior to this, please talk to the site developers!</b>. <br />
			<br />
			To find these files, just click on the corresponding buttons on the top right of the tables.
		</p>
		<p>
			<img src='img/help/Display_tb_export_import.png' class="help_img" alt='Part of the split group of item screen'/>
		</p>


		<a name="display_tables_usr_tbl"></a>
		<h3 class="help_heading">5.2 User Tables</h3>
		<p>
			In these tables you will find all the information about any registered equipment.<br />
			<br />
			The <b>Basic Equipment Table</b> contains information like the subbatch ID ,the type of equipment, the quantities in a batch and the conformity flag.<br />
			<br />
			The <b>Details Equipment Table</b> contains all the information about the manufactering process. <br />
			<br />
			In the <b>Parenting Table Historic</b> you will find all links between children and parents which were ever set. The is active flag shows, if the relationship is still holding or not. <br />
			<br />
			The <b>Batch Heritage Table</b> contains all the links between the subbatches and their mother batches, from which they inherit information, e.g. about the supplier.<br />
			<br />
			The table <b>Status and Location Table Historic</b> contains all changes in status or location for each equipment. So if you want to reconstruct the history of an equipment, 
			you are right here.<br />
			<br />
			The <b>Documents Table</b> contains all uploaded documents. The links between the documents and the equipment is shown in the <b>Document Linking Table</b>. <br />
			<br />
			The <b>Equipment Comments</b> table shows all the comments, which were given. Also the type of comment and the MTF ID of the commented equipment is shown.<br />
			<br />
			And last but not least, the <b>Stickers to be printed</b> table shows all the stickers which should be printed, the MTF ID of the corresponding equipment and 
			the user who wants to print stickers for this equipment ID.
		</p>


		<a name="display_tables_admin_tbl"></a>
		<h3 class="help_heading">5.3 Admin Tables</h3>
		<p>
			The admin tables are generally used as dictionaries.<br />
			<br />
			In the <b>Listing of Statuses</b> you can see all the possible statuses, which appear in the dropdown menus at registration or in the display equipment menu. 
			Beside the name of the status, you will find its  ID , which is used in the users tables and in the database. Also you will find several flags, to manage the menu at registration, 
			and the workflow monitor on the right side of display equipment menu. <br />
			<br />
			The <b>Location Table</b> provides detailed informations over all locations where the equipment could be, e.g. the responsible person, the address, ... . 
			Also you have flags to show whether the location is a site or a company or both. The <b>Equipment Type Table</b> shows all possible equipment types. 
			You will find its name, the code, the unit of equipment, whether it is a batch or not and some other information. <br />
			<br />
			In the <b>Allowed Parenting Table</b> you will find all allowed possibilities of two types beeing in a child and parent relationship.
		</p>


		<a name="display_tables_DB_Structure"></a>
		<h3 class="help_heading">5.4 DB Structure</h3>
		<p>
			Here you will find different tables which provides you information about the DB Structure.<br />
			The <b>Sub Projects in the NSW</b> table provides the different projects within the NSW. This is necessary to set the corresponding letter in the MTF Number. <br />
			<br />
			The <b>Website Column Translations</b> is used to translate the coulumn headers from the database to human readable text in the columns of the interface. <br />
			<br />
			The <b>Content tables of the database</b> shows all tables in the database. It is also used to translate their names into a human readable description. <br />
			<br />
			In the <b>Logins of the Webview</b> table you can find all users of the interface. There are also the information about their rights (read/write/admin), 
			their home site and the number of faulty login attemps. <br />
			<br />
			In the table <b>Defined Printers and Settings</b> you can find the registered printers and their margins. This is necessary for the printing of the stickers.
		</p>


		<a name="display_tables_ex"></a>
		<h3 class="help_heading">5.5 Examples</h3>
		<p>
			<b>How do I find my last registered equipment?</b><br />
			Go to the &quot;Basic Equipment Table&quot; in the Users Tables. Then sort by the &quot;User creating this entry&quot; by clicking on its column heading. 
			Then set the &quot;User Creating this entry&quot; column as second search criteria by holding &#60SHIFT&#62 and clicking on the column header. 
			You have to scroll through the sorted table to find the corresponding entries or use the search function of your browser (often &#60SHIFT&#62 + &#60f&#62) to find your username.
		</p>
		<p>
			<b>How do I find the code of my equipment type?</b><br />
			Go to the &quot;Equipment Type Table&quot; in &quot;Admin Tables&quot;. Then sort the table by the &quot;Equipment Type Name&quot; by clicking on the column header. 
			Now you can search for your equipment type by scrolling through the table or using the search function of your browser (often &#60SHIFT&#62 + &#60f&#62). 
			The equipment code is displayed in the column left to the names.
		</p>


		<a name="admin"></a>
		<h2 class="help_heading"> 6 Admin </h2>
		<p>
			In the admin section you can lock or unlock users, and you can edit the admin tables.
		</p>


		<a name="admin_lock"></a>
		<h3 class="help_heading">6.1 Lock/Unlock User </h3>
		<p>
			<img src='img/help/admin_lock.png' class="help_img" alt='Part of the split group of item screen'/>
		</p>
		<p>
			In this part of the admin menu you can lock users. Also you can unlock users which were locked by hand for any reason or by entering their password wrong five times.<br />
			<br />
			To lock/unlock someone click on the text field were default_user is written. Then choose a username from the dropdown menu. 
			Afterwards click on <b>Unlock this User</b>	or <b>Lock this User</b>. Then you should get a success message.
		</p>


		<a name="admin_edit"></a>
		<h3 class="help_heading">6.2 Edit Admin Table</h3>
		<p>
			<img src='img/help/admin_table.png' class="help_img" alt='Part of the split group of item screen'/>
		</p>
		<p>
			The second purpose of the admin section is to edit the admin tables. Therfore click on <b>Select a Table</b> and choose the table you want to edit.<br />
			You will be asked whether you want to <b>Register, Edit or Delete an Entry</b>.
		</p>
		<p>
			<img src='img/help/admin_table_expanded.png' class="help_img" alt='Part of the split group of item screen'/>
		</p>
		<p>
			If you want to edit or delete an entry you will be asked for the Status ID of the entry you want to work on. Confirm it by clicking on <b>Load Entry</b>.<br />
			In the upper region of the frame, also the type of access, and the accessed table is displayed.
		</p>
		<p>
			<img src='img/help/admin_ID.png' class="help_img" alt='Part of the split group of item screen'/>
		</p>
		<p>
			After loading the entry you will be provided the following screen:
		</p>
		<p>
			<img src='img/help/admin_edit_entry.png' class="help_img" alt='Part of the split group of item screen'/>
		</p>
		<p>
			The grey fields are set by the system and cannot be edited. The white fields can be edited by the user. 
			If you are registering a new entry or you are editing one, fill out all the fields and then click on <b>Register Entry</b> or <b>Update Entry</b>.<br />
			<br />
			If you want to delete a newly, but faulty registered entry, you cannot edit any field, but just delete the entry by clicking on <b>Delete Entry</b>.
		</p>


		$multiedit_body
	</div>
</div>
