<div class="bbox simple">
	<div class="height70">
		<div id="eqinfoRow1Col1" class="bbox column wfifth">
			<div id="eqinfoMtfId" class="hhalf">
				<input id='mtfId'        name='mtfId'        class='invisreadonly' type="text" value="$mtfId" readonly>
			</div>
			<div class="hhalf">
				<input id='eqTypeName'   name='eqTypeName'   class='invisreadonly' type="text" value="$eqTypeName" readonly>
			</div>
		</div>
		<div id="eqinfoRow1Col2" class="bbox column wfifth">
			<div class="hhalf"><label class='invisdesc'>EQ ALIAS</label></div>
			<div class="hhalf"><input id='otherId' name='otherId' class='invisiblelike full' type="text" 
							onchange="changecolor('otherId')" value="$otherId" pattern="$patternText"></div>
		</div>
		<div id="eqinfoRow1Col3" class="bbox column wfifth">
			<div class="hhalf"><label class='invisdesc'>EQ SUB BATCH ID</label></div>
			<div class="hhalf"><input id='subBatchId' name='subBatchId' class='invisreadonly' type="text" 
							value="$subBatchId" readonly></div>
		</div>
		<div id="eqinfoRow1Col4" class="bbox column wfifth">
			<div class="hhalf"><label class='invisdesc'>EQ ID</label></div>	
			<div class="hhalf"><input id='eqId' name='eqId' class='invisreadonly' type="text" 
							value="$eqId" readonly></div>			
		</div>
		<div id="eqinfoRow1Col5" class="bbox column wfifth">
			<div class="hhalf"><label class='invisdesc'>( GOOD UNITS / UNITS IN BATCH )</label></div>
			<div class="hhalf">
				<input id='goodQuantity' name='goodQuantity' class='invisiblelike half' type="text" 
							onchange="changecolor('goodQuantity')" value="$goodQuantity" $statLocDisable pattern="$patternInt"> $quantityUnit / 
				<input id='quantity' name='quantity' class='invisiblelike half' type="text" 
				            onchange="changecolor('quantity')" value="$quantity" $statLocDisable pattern="$patternInt"> $quantityUnit</div>
		</div>
	</div>
</div>
<div class="bbox simple">
	<div class="border padd6x12 height55">
		<div id="groupsRow2Col1" class="column wfifth">
			<div class="hhalf"><label class='desc'>Other ID (EQ ALIAS)<span class='mandatory'>*</span>:</label></div>
			<div class="hhalf"><input class='invisiblelike full $inClass' type="text" id='groupsRangeOtherId' name='rangeOtherId' value='$rangeOtherId' onclick='setSeqFieldsColor()' pattern="$patternText"></div>
		</div>
		<div id="groupsRow2Col2" class="column wfifth">
			<div class="hhalf"><label class='desc'>Seq. No. (from)<span class='mandatory'>*</span>:</label></div>
			<div class="hhalf"><input class='invisiblelike medium $inClass' type="number" id='groupsRangeFrom' name='rangeFrom' min='1' max='$rangeMaxQuant' value='$rangeFrom' onclick='setSeqFieldsColor()' onchange='setRangeTo()'></div>
		</div>
		<div id="groupsRow2Col3" class="column wfifth">
			<div class="hhalf"><label class='desc'>Seq. No. (to)<span class='mandatory'>*</span>:</label></div>
			<div class="hhalf"><input class='invisiblelike medium $inClass' type="number" id='groupsRangeTo' name='rangeTo' min='1' max='$rangeMax' value='$rangeTo' onclick='setSeqFieldsColor()' onchange='setRangeFrom()'></div>
		</div>
		<div id="groupsRow2Col4" class="column wfifth">
			<div class="hhalf"><label class='desc'>Seq. No. (digits)<span class='mandatory'>*</span>:</label></div>
			<div class="hhalf"><input class='invisiblelike medium $inClass' type="number" id='groupsRangeDigits' name='rangeDigits' min='2' max='5' value='$rangeDigits' onclick='setSeqFieldsColor()' onchange='setRangeMaxs()'></div>
		</div>
		<div id="groupsRow2Col5" class="column wfifth">
			<div class="hhalf"><br /></div>
			<div class="hhalf"><button type="button" class="buttonlike flex" onclick='if(checkAliasRange()){submitIt("range")}'>Define ALIAS Range</button></div>
		</div>
	</div>
</div>
<div class="bbox simple">
	<div class="border padd6x12">
<!--
		<input type='checkbox' name='addChildren' value='1' $check_addChildren onclick='submitIt("reload")'>
		I want to add a child to each group member.<br />
-->
		<input type='checkbox' id='splitIndep' name='splitIndep' value='1' $check_splitIndep onclick='confirmIndepSplitting()'>
		I want to split the group into independent items.
	</div>
</div>
$aliasRange
