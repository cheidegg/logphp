<div class='border padd6x12 topm6 light'>
	HINT: You can enter MTF-IDs which are not yet registered but print them nonetheless. When added,
	such IDs will be highlighted in the table below. But please pay attention: these IDs will all 
	disappear once you leave the sticker printing tab. Only existing items can be saved for later!
</div>
