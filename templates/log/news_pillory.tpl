<div id='pillory'>
	<table class='greytable smaller'>
		<thead>
			<tr><th>Production Site</th><th>Oldest Shipping</th><th>Expiry Date</th><th>No. Open Items</th><tr>
		</thead>
		<tbody>
			$rows
		</tbody>
	</table>
</div>
<!---
	<p>
		The purpose of this web page is to be an interface to the NSW Logistics Database. This DB
		holds all data related to material, tracking and logistics for the NSW series production.
	</p>
	<br />
	<p>
		Normal usage of the NSW equipment DB possible.<br />
		<br />
		<b><font color=red>Please notice our hopefully-helpful Help/FAQ section on the right of the menu bar (-).</font></b><br />   
		We spent a lot of time to put it together and it might be useful. ;-)<br />
		If you don't understand something, please ask. You can also do this via Skype (look for &quot;dbeans&quot;).<br />
		<br />
		Please be aware, that there is a <b><font color= #339900>&quot;playground&quot; database </font></b> with a development version
		interface on: <br />
		<br />
		<a href="http://hep.uni-freiburg.de/~webadmin/CERN_DB_WebView/LOG.php" style="text-decoration: none"><b><font color= #339900>
		http://hep.uni-freiburg.de/~webadmin/CERN_DB_WebView/LOG.php</font></b></a><br />
		<br />
		So, if you want to test something before entering it or if you are a developer and you want to understand the interface
		and the underlying database, please play on the playground, not on the production system. You will easily recognize the 
		playground website by its (colorful) background design:
	</p>
	<p style="text-align:center">
		<a href="http://hep.uni-freiburg.de/~webadmin/CERN_DB_WebView/LOG.php"><img src='img/playground2.png' alt='Playground picture' height='92' width='164'/></a>
	</p>
	<br />
	<p>Information about starting QAQC works and installing SQL Developer: <a href='downloads/QAQC_work_intro.pdf' download><b>DOWNLOAD</b></a></p>
	<p>Production Sites Tasks (Presentation): <a href='downloads/Prodsitetasks.pdf' download><b>DOWNLOAD</b></a><br></p>
--->
