<li> <div class="help_navlistitem"> <a class="help_link" href="#multi_edit">7 Multi Edit</a> </div></li>
<ul>
	<li> <div class="help_navlistsubitem"> <a class="help_link" href="#me_gen">7.1 Edit Multiple Items or Batches at the Same Time</a></div></li>
	<li> <div class="help_navlistsubitem"> <a class="help_link" href="#me_groups">7.2 Groups and Items from Groups</a> </div></li>
</ul>
