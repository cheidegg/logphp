<input name='prtUpdate' type='hidden' value='$prtUpdate'>
<div class='bbox simple'>
	<div class='border padd6x12'>
		Specify printer parameters:<br />
		<label>printer name<span class="mandatory">*</span>:</label>
		<input class='invisiblelike' name='prtName' type='text' maxlength='30' size='22' value='$prtName' pattern="$patternText" required>
		<label>sticker size<span class="mandatory">*</span>:</label>
		<input class='invisiblelike' name='prtSize' type='number' step='1' min='0' max='$maxSize' value='$prtSize' required>
		<label>set printer as default:</label>
		<input type='checkbox' name='prtDef' value='1' $checked_prtDef>
		<br />
		<label>margins<span class="mandatory">*</span>:</label>
		<input class='invisiblelike prtMargs' name='margTop'    type='number' step='0.1' value='$margTop' required>
		<label>top, </label>
		<input class='invisiblelike prtMargs' name='margLeft'   type='number' step='0.1' value='$margLeft' required>
		<label>left, </label>
		<input class='invisiblelike prtMargs' name='margRight'  type='number' step='0.1' value='$margRight' required>
		<label>right, </label>
		<input class='invisiblelike prtMargs' name='margBottom' type='number' step='0.1' value='$margBottom' required>
		<label>bottom</label>
		<button type="button" class="buttonlike flex" onclick="submitIt('$prtButton')">save printer parameters</button>
	</div>
</div>
