<div class="bbox simple">
	<div class="border padd6x12">
		<div class="batchesQuantRow">
			<label class="batchesQuantQuant">Quantity of required subbatches<span class="mandatory">*</span>:</label>
			<input class="invisiblelike batchesQuantInput" name="quantitySubbatches" type="number" min="2" max="$maxSubbatches" value="$quantitySubbatches" pattern="$patternInt" required>
			<button type="button" class="buttonlike flex" onclick="if(isBatchAlias('$otherId')){submitIt('require')}" $activeState>Define Quantities</button>
		</div>
	</div>
</div>
