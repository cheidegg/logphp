<input type="hidden" name="eqTypeCodeId"        value="$eqTypeCodeId" />
<input type="hidden" name="isNcf"               value="$isNcf"        />
<input type="hidden" name='hiddenLeftUnits'     value='$leftUnits'     id='batchesHiddenLeftUnits'     />
<input type="hidden" name='hiddenLeftGoodUnits' value='$leftGoodUnits' id='batchesHiddenLeftGoodUnits' />
<div class="bbox simple">
	<div class="height70">
		<div id="eqinfoRow1Col1" class="bbox column wfifth">
			<div id="eqinfoMtfId" class="hhalf">
				<input id='mtfId'        name='mtfId'        class='invisreadonly' type="text" value="$mtfId" readonly>
			</div>
			<div class="hhalf">
				<input id='eqTypeName'   name='eqTypeName'   class='invisreadonly' type="text" value="$eqTypeName" readonly>
			</div>
		</div>
		<div id="eqinfoRow1Col2" class="bbox column wfifth">
			<div class="hhalf"><label class='invisdesc'>EQ ALIAS</label></div>
			<div class="hhalf"><input id='otherId' name='otherId' class='invisiblelike full' type="text" 
							onchange="changecolor('otherId')" value="$otherId" pattern="$patternText"></div>
		</div>
		<div id="eqinfoRow1Col3" class="bbox column wfifth">
			<div class="hhalf"><label class='invisdesc'>EQ SUB BATCH ID</label></div>
			<div class="hhalf"><input id='subBatchId' name='subBatchId' class='invisreadonly' type="text" 
							value="$subBatchId" readonly></div>
		</div>
		<div id="eqinfoRow1Col4" class="bbox column wfifth">
			<div class="hhalf"><label class='invisdesc'>EQ ID</label></div>	
			<div class="hhalf"><input id='eqId' name='eqId' class='invisreadonly' type="text" 
							value="$eqId" readonly></div>			
		</div>
		<div id="eqinfoRow1Col5" class="bbox column wfifth">
			<div class="hhalf"><label class='invisdesc'>( GOOD UNITS / UNITS IN BATCH )</label></div>
			<div class="hhalf">
				<input id='goodQuantity' name='goodQuantity' class='invisiblelike half' type="text" 
							onchange="changecolor('goodQuantity')" value="$goodQuantity" $statLocDisable pattern="$patternInt"> $quantityUnit / 
				<input id='quantity' name='quantity' class='invisiblelike half' type="text" 
				            onchange="changecolor('quantity')" value="$quantity" $statLocDisable pattern="$patternInt"> $quantityUnit</div>
		</div>
	</div>
</div>
$quantreqsubbatch
$quantities
$end
