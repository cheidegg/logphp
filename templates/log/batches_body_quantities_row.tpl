<div class="batchesQuantRow">
	<label class="batchesQuantQuant">Quantity of Units to fill in #$idx<span class='mandatory'>*</span>:</label>
	<input class="invisiblelike batchesQuantInput" id="unit$idx" type="number" name="units[]" min="1" max="$quantity"
           onchange="setDivUnits('unit',$idx,$quantitySubbatches,$quantity);" value="$unit" pattern="$patternInt">
	<label class="batchesQuantUnit">$quantityUnit</label>
	<span class="slash">/</span>
	<label class="batchesQuantGood">Good Units thereof in #$idx<span class='mandatory'>*</span>:</label>
	<input class="invisiblelike batchesQuantInput" id="goodunit$idx" type="number" name="goodUnits[]" min="0" max="$goodQuantity"
           onchange="setDivGoodUnits('unit','goodunit',$idx,$quantitySubbatches,$goodQuantity);" value="$goodUnit" pattern="$patternInt">
	<label class="batchesQuantUnit">$quantityUnit</label>
</div>
