		<a name="multi_edit"></a>
		<h2 class="help_heading"> 7 Multi Edit </h2>
		<p>
		If you need to edit attributes of many items or batches, we will probably give 
		you the possibility to edit these at the same time, via the 
		<b>Edit Multiple Items</b> functionality in the equally named section in the 
		<b>Register Equipment</b> tab.<br />
		<br />
		Also, if you see this help, you already requested and got the required permissions.<br />
		<br />
		</p>
			<a name="me_gen"></a>
			<h3 class="help_heading">7.1 Edit Multiple Items or Batches at the Same Time</h3>
			<p>
			You can edit single items already in the <b>Display Equipment</b> tab, but if you want to do the same change 
			(i.e. set a new status) to many items, this is no fun at all. To perfom such an operation, you can use the Multi Edit tool.<br />
			<br />
			First, you need to tell the tool which items you need to edit by selecting ranges of <b>MTF IDs</b>, <b>Unique DB EQ IDs</b> or even 
			<b>Aliases</b> in the top section of the screen, which you can also see in the following picture:
		</p>
		<p>
			<img src='img/help/multi_edit.png' class="help_img" alt='Multi Edit Screen'/>
		</p>
		<p>
			There are currently 2 ranges selected in the list: 114 items currently being at 
			CERN and 5 items which have the status "in transit". <br />
			Below the list, you find a drop-down menue, where you can specify an attribute that you 
			want to change (i.e. status or location). <br />
			If you choose something that needs additional information (like ... which status?), another 
			field or popup will appear beside this one.<br />
			</br />
			Usually a range of items in this selection will be white. These are in this case yellow colored, 
			because there are items in the selections which are origination from a group. 
			This is in principle not a problem, except that you can only apply some of the possible changes to these items, others not.
			You can find more information about how to handle items origination from a group in  
			<a class="help_link" href="#me_groups">Multi Edit - Groups</a>.<br />
			<br />
		</p>
		<p>
			<img src='img/help/delete_range.png' class="help_img" alt='Delete a Selected Range'/>
		</p>
		<p>
			You can also delete a range that you added to the list by selecting it in the left column 
			and then pressing &quot;go&quot; beside the &quot;remove ranges&quot; menue. So once you 
			selected the attribute you want to change for all the selected ranges you should click on <b>Save changes</b>.<br />
			<br />
			Be aware, you cannot undo any of these changes easily with just one button and 
			even if you undo them manually, they will be recorded in the items history.<br />
			<br />
			Also please be aware, that you should not use this tool (anymore) to record the shipping of items, 
			since there is the newer/better tool <b>Ship Equipment</b>, which records additional information and will be confused, 
			if you use both tools on one equipment for the same shipping process.<br />
			<br />
			Selected ranges will stay in the list until you reload the tool. 
			This is to help you change multiple attributes for a given range without entering everything again.
		</p>
		<a name="me_groups"></a>
		<h3 class="help_heading">7.2 Groups and Items from Groups</h3>
		<p>
			Items originating from groups, which still have their link to their former group, can only be edited partially. 
			The attributes, which they still inherit from their mother-group cannot be set individually 
			(the concerned attributes are greyed out in the drop-down, like in the example above).
			These attributes are the ones that have to do with supplier and ordering information. 
			For these items, you can still change the supplier information by changing this information in the corresponding mother-group, 
			but this will affect all items originating from this group. If you find, that you need to set different supplier 
			information among items from one single group, you need to split the group &quot;individually&quot;. 
			You can do this by selecting the concerned items and then press <b>Save changes</b>.
		</p>
		<p>
			<img src='img/help/break_link.png' class="help_img" alt='Break a Link to a Group'/>
		</p>
		<p>
			If the group was split using the "Split into independent items" option, the items copied the supplier-related 
			information from their former group and are not anymore linked to it. They can have their own supplier-related 
			information then, but changing the groups information will not be passed down to the single items anymore.
		</p>


