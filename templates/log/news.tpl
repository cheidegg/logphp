<script type="text/javascript" src="jsNew/log/news.js"></script>
<link rel="stylesheet" href="stylesNew/log/news.css">
<div class="bbox simple">
	<div id='newLOG'>
<!--- <p><b>Production Plots have been (temporarily) taken offline due to the fact that clicking on some of them simply sends the web server into a feedback loop that takes forever and blocks the whole website for several hours in the worst case. If you urgently need to have production plots (for presentations,...) please send me (Kim) an email! (-> until we fix it...we are working on that.)</b> </p>-->
	<p>You will probably have noticed it: We are now setting the "new" LOG website as the default - you have been redirected here directly from the login page.
	</p>
	<p>Until we exchange the code on the old web adress, please use this site as the real, the one and only LOG production website!
	</p>
	<p>... and please report any bugs or weird behaviour soon to us (kim.heidegger@physik.uni-freiburg.de)!
	</p>
	<p>
	<button id="newFeat" type="button" onclick="toggleDisplayOnOff('toggleNewFeat');"><b>New Features on the new LOG page . . .  (readme!) </b></button>
	</p>
	<div id="toggleNewFeat" style="display:none;">
		<p>
         <b>- Registration:</b> The registration of Equipment does not skip MTF IDs anymore, if you cancel the it mid-process. Now it will post a warning and simply give you the next free ID if someone else grabbed "your" MTF ID while you entered information.
   </p>
   <p>
      <b>- Shipping:</b> You can now forward shipments that already exist without typing all the IDs again into the shipping box. Just pick the ID of the previous shipment.
        </p>
   <p>
      <b>- Splitting Groups:</b> After you split a group of items into individual EQs, you can enter a batch ID to attach as a child to every newly created EQ.
   </p>
   <p>
                <b>- Display Equipment:</b> Now, it is possible to "delete" unwanted statuses, documents, comments and parentings.<br> Please be aware, that these are only deactivated and not really deleted - but unless they are checking the raw data, this information will never show up again anywhere, will not be counted in graphs or similar. It is as good as deleted. Still, a comment why you deleted what could be helpful for the afterworld, but is not mandatory.<br>
Please be aware, that we still do not allow to delete shippings, since this operation affects so many different EQs and tables, that an automatic 'undo' would very often lead to completely corrupt data.<br>
If you want to undo a shipping, either just ship it back with the new forwarding function or send us an email, if it is really important that this shipping is properly undone or that one EQ is taken out of it or similar.<br>
We also repaired the Integration Tree on the left of Display Equipment. It will now properly show you all assembled "large" structures.
        </p>
        <p>
                <b>- My Site Overview:</b> Now, the tables on My Site Overview are better structured and can truly be filtered by status, location and EQtype.<br> Also, they can now be filtered for any status or location that the EQ had once in its past:<br>"Show me all EQ of type xy that have ever been at BB5 at some point and that have a status "rejected" somewhere in their history!" is now a valid search.
        </p>
        <p>
                <b>- Production Plots:</b> Now, you can download lists that show you not only the summary of the content, but each item with the date that was counted. Makes it much easier to spot missing status flags.
        </p>
        <p>
                <b>- General Improvements:</b> Tables that show you results of searches can now be displayed in multiple "pages".<br>
This removes the delay when the website is trying to load ridiculously long tables all at once. You can still decide to display it all on one page (can be useful to do searches in the table). Then, if it loads forever, you know why.
        </p>
        <p>
                The website now looks the same as before. But looking at the code, you would not say that there is any similarity at all,                        before and after.<br>
                We now have a proper coding structure which enables us to react much more quickly to new requests for features.<br>
                So if you already gave up waiting or you did not ask in the first place for anything you would really like to have on LOG, now would be a good time to try again.
        </p>
        <p>
                <b>- QAQC Website:</b> The "blue" website, which displays the QAQC measurements of the Micromegas Central QAQC DB (look at the login screen and click on the blue button, if you do not know it yet), now filters properly for almost any filter combination you can think of. Next step will be to add basic histograms for all Contexts.
        </p>
        <p>
        <b>- Production Plots:</b> Now, you can download lists that show you not only the summary of the content, but each item with the date that was counted. Makes it much easier to spot missing status flags.
        </p>
</div>
</div>


$thepillory


<p>The purpose of this web page is to be an interface to the NSW Logistics Database. 
This DB holds all data related to material, tracking and logistics for the NSW series production.
</p>
<br />
<p>Normal usage of the NSW equipment DB possible.<br />
<br />
<b><font color=red>Please notice our hopefully-helpful Help/FAQ section on the right of the menu bar (--^).</font></b><br />
We spent a lot of time to put it together and it might be useful. ;-)<br />
If you don't understand something, please ask. 
You can also do this via Skype (look for "dbeans").<br />
<br />
Please be aware, that there is a <b><font color= #339900>"playground" database</font></b> 
with a development version interface on:<br />
<br />
<a href="http://hep.uni-freiburg.de/~webadmin/CERN_DB_WebView/LOG.php" style="text-decoration: none"><b><font color= #339900>http://hep.uni-freiburg.de/~webadmin/CERN_DB_WebView/LOG.php</font></b></a><br />
<br />So, if you want to test something before entering it or if you are a 
developer and you want to understand the interface and the underlying database, 
please play on the playground, not on the production system. You will easily 
recognize the playground website by its (colorful) background design:
</p>
<p style="text-align:center"><a href="http://hep.uni-freiburg.de/~webadmin/CERN_DB_WebView/LOG.php"><img src='img/playground2.png' alt='Playground picture' height='92' width='164'/></a></p>
<p><br />Information about starting QAQC works and installing SQL Developer: <a href='downloads/QAQC_work_intro.pdf' download><b>DOWNLOAD</b></a></p>
<p>Production Sites Tasks (Presentation): <a href='downloads/Prodsitetasks.pdf' download><b>DOWNLOAD</b></a><br /></p>
</div>
