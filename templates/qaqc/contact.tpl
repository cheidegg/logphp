<div class="simple bbox">
	<p>This WebView of the <b>$title</b> has been developed by</p>
	<br />
	<p>
		Dr. Kim Temming and Ulrich Holland<br />
		<br />
		Physikalisches Institut University of Freiburg<br />
		Hermann-Herder-Strasse 3a<br />
		79104 Freiburg, Germany<br />
		<br />
		Group of Prof. Gregor Herten and Prof. Ulrich Landgraf<br />
		<br />
	</p>
	<p>If you have any questions, please contact Kim at ...</p>
	<p><font class="emph">kim.temming@physik.uni-freiburg.de</font></p>
	<p>... or by phone ...</p>
	<p><font class="emph">0049 761 203 5720</font>.</p> 
</div>
