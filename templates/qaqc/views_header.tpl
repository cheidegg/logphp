<div class="bbox simple">
	<div class="border padd6x12 wrap light">
		<div class="fleft wthird right topp6">
			Choose Site:
		</div>
		<div class="fleft wtwothirds">
			$select_site
		</div>
		<div class="clear"></div>
		<div class="fleft wthird right topp6">
			and Select Context: 
		</div>
		<div class="fleft wtwothirds">
			$select_context
		</div>
		<div class="clear"></div>
	</div>
	<div class="padd6x12 topm6 center">
		- OR - 
	</div>
	<div class="border padd6x12 wrap light">
		<div class="fleft wthird right topp6">
			Select 2D Plot: 
		</div>
		<div class="fleft wtwothirds">
			$select_twodim
		</div>
		<div class="clear"></div>
	</div>
</div>
