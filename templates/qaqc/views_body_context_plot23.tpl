<table class="invisiTable" id="$tableId">
	<thead>
		<tr>
			<th colspan="$colspan">$plotTitle ($plotDate)</th>
		</tr>
		<tr>
			$invhead
		</tr>
	</thead>
	<tbody>
		$invbody
	</tbody>
</table>
<div class="bbox simple">
	<div class="border padd6x12 center">              
		<div class="plotFrame2 divCenter center">
			<div id='$canvasId' class="divCenter plotCanvas62x50"></div>
			<div class='plotButtons wrap'>
				<div class='fleft'>
					Download Data: 
					<a id='$linkCsv' class='plotLink' href='#'>csv</a>
				</div>
				<div class='fleft plotSep'> | </div>
				<div class='fleft'>
					<a id='$linkXls' class='plotLink' href='#'>excel</a>
				</div>
				<div class='fleft plotSep'> | </div>
				<div class='fleft' id='$png'></div>
			</div>
			<div class="clear"></div>
		</div>
	</div>
</div>
$scriptData
$scriptExport
