import sys
import re

infile=open(sys.argv[1]).read()
pattern=open(sys.argv[2]).read()
outfile=open(sys.argv[3],"w")

index= infile.find('%%Creator: "barcode",')
infile=infile[:index]+"%%Orientation: Landscape\n"+infile[index:]

#outfile.write(infile.replace("%%DocumentPaperSizes: 841.889764x595.275591\n\n%%EndComments\n%%EndProlog\n\n",pattern))
infile=infile.replace("%%DocumentPaperSizes: 841.889764x595.275591\n\n%%EndComments\n%%EndProlog\n\n",pattern)

#m=re.findall("(%%Page: [0-9]+ [0-9]+).*\n",infile)

outfile.write(re.sub(r"(%%Page: [0-9]+ [0-9]+).*\n",r"\1 \n%%BeginPageSetup\n90 rotate 0 -595 translate\n%%EndPageSetup\n",infile))
