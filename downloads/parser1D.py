# Script to read json data as retrieve from MM QAQC Interface
# usage: python parser1D.py namefile
# author: edfarina
# edfarina@cern.ch
# date: 10/05/2020


import sys, getopt
from ROOT import *
import json

def main(argv):
  with open(argv[0], 'r') as file:
      data = file.read().replace('\n', '')
      # print data

  hnew = TBufferJSON.ConvertFromJSON(str(data))  
  gStyle.SetOptStat(1)
  hnew.Draw();
  input(' ')
if __name__ == "__main__":
   main(sys.argv[1:])   
